#define KOUTEST_STD_STRING_HANDLING
#include <koutest.h>

#include <pruto/error_messages.h>

#include <cstdlib>

namespace pruto {
    inline std::string koutest_debug_string(const std::vector<virtual_machine::instruction>& bytecode) { return to_string(bytecode); }
    inline std::string koutest_debug_string(virtual_machine::step_result step) { return to_string(step); }
}

using namespace pruto;
using OpCode = virtual_machine::instruction::OpCode;

static script_section_ast get_ast(const char* script, const host_identifier_table& host_entries = {})
{
    const auto ast = section_ast(script, host_entries, {});
    if( ast.error )
        FAIL(error_message(ast, script).data());
    return std::move(ast.section);
}

static void run_vm(virtual_machine& vm, virtual_machine::host_integration* host)
{
    using step = virtual_machine::step_result;
    while( true ) {
        auto s = vm.step(host);
        if( s.result == step::Stop )
            break;
        CHECK(int(s.result) < int(step::WaitingForHost));
    }
}

class test_host_integration : public code_generator::host_integration, public virtual_machine::host_integration {
public:
    enum Entries {
        VarHost,
        ProcLess,
        ProcFailOnce,
        ProcList,
        ProcIter
    };

    struct test_iter final : public virtual_machine::host_integration::for_iterator {
        bool advance(virtual_machine::data_stack_entry* element_storage) override
        {
            element_storage[0].u = count;
            ++count;
            return count <= limit;
        }
        void release() override { delete this; }
        unsigned count = 1;
        unsigned limit = 10;
    };

    static pruto::host_identifier_table host_identifier()
    {
        return {
            {"host_var", ident::host_var(VarHost)},
            {"less", ident::host_proc_ret(ProcLess, 2)},
            {"fail_once", ident::host_proc_ret(ProcFailOnce, 1)}
        };
    }

    instance proc_instance(ident id, const type_info* params) override
    {
        assert(params[0] == num_type());
        if( id == ident::host_proc_ret(ProcLess, 2) ) {
            assert(params[1] == num_type());
            return {ProcLess, type_boolean()};
        } else {
            assert(id == ident::host_proc_ret(ProcFailOnce, 1));
            return {ProcFailOnce, num_type()};
        }
    }
    instance list_operator_instance(const type_info& operand, const type_info& element_type, size_t) override
    {
        if( element_type != num_type() || operand != num_type() )
            return {};
        return {ProcList, num_type()};
    }
    instance iterator_instance(const type_info& param) override
    {
        if( param != num_type() )
            return {};
        return {ProcIter, num_type()};
    }
    type_info num_type() override { return type_host{0}; }
    bool create_num(std::string_view literal, code_generator::data_stack_entry* retval) override
    {
        retval->u = unsigned(std::atoi(literal.data()));
        return true;
    }
    type_info host_data_type(ident id) override
    {
        return id == ident::host_var(VarHost) ? num_type() : type_info();
    }
    code_generator::stack_entry_count type_size(host_type_id type_id) override
    {
        assert(type_id == 0);
        return 1;
    }

    bool call_proc(virtual_machine::host_instance_index index, const virtual_machine::data_stack_entry* args,
                   virtual_machine::data_stack_entry* retval) override
    {
        if( index == ProcLess ) {
            retval->u = (args[0].u < args[1].u);
            return true;
        } else if( index == ProcFailOnce ) {
            if( failed ) {
                retval->u = args->u + 2;
                return true;
            }
            failed = true;
            return false;
        }
        return false;
    }
    bool call_list(virtual_machine::host_instance_index index, const virtual_machine::data_stack_entry* args,
                   size_t count, virtual_machine::data_stack_entry* retval) override
    {
        assert(index == ProcList);
        unsigned s = 0;
        for( size_t i = 1; i <= count; ++i )
            s += args[i].u;
        retval->u = s * args[0].u;
        return true;
    }
    for_iterator* create_iterator(virtual_machine::host_instance_index index, const virtual_machine::data_stack_entry* arg) override
    {
        if( index != ProcIter )
            return {};
        auto i = new test_iter;
        i->limit = arg->u + 1;
        return i;
    }
    bool store_host_data(ident id, const virtual_machine::data_stack_entry* val,
                         virtual_machine::stack_entry_count offset, virtual_machine::stack_entry_count count) override
    {
        if( id != ident::host_var(VarHost) || offset != 0 || count != 1 )
            return false;
        host_var = val->u;
        return true;
    }
    bool load_host_data(ident id, virtual_machine::stack_entry_count offset, virtual_machine::stack_entry_count count,
                        virtual_machine::data_stack_entry* dst) override
    {
        if( id != ident::host_var(VarHost) || offset != 0 || count != 1 )
            return false;
        dst->u = host_var;
        return true;
    }

    unsigned host_var = 123;
    bool failed = false;
};


TEST_CASE(host_call) {
    const auto ast = get_ast("x = fail_once(5)", test_host_integration::host_identifier());

    virtual_machine vm;
    test_host_integration host;
    code_generator gen;

    CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &host)));
    CHECK(gen.push_global_init());
    {
        using step = virtual_machine::step_result;
        CHECK_EQ(vm.step(&host), (step{step::ErrorHost, 0}));
        CHECK_EQ(vm.step(&host), (step{step::Return, 0}));
        CHECK_EQ(vm.step(&host), (step{step::Stop, 0}));
        CHECK_EQ(vm.data_stack.size(), 2);
        CHECK_EQ(vm.data_stack[1].u, 7);
    }
}

TEST_CASE(host_retval) {
    const auto ast = get_ast("proc foo()\nreturn 42\nend", test_host_integration::host_identifier());

    virtual_machine vm;
    test_host_integration host;
    code_generator gen;

    CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &host)));
    CHECK_EQ(gen.prepare_call(ast_find_global(ast, "foo"), nullptr).instance, 0);

    virtual_machine::data_stack_entry retval[1];
    CHECK(gen.push_call(0, nullptr, retval));
    {
        using step = virtual_machine::step_result;
        CHECK_EQ(vm.step(&host), (step{step::Return, 0}));
        CHECK_EQ(vm.step(&host), (step{step::Stop, 0}));
        CHECK_EQ(retval[0].u, 42);
    }
}

TEST_CASE(host_var) {
    const auto ast = get_ast(R"PRU(
proc foo()
  host_var := host_var[2]
  return true
end
x = foo()
)PRU", test_host_integration::host_identifier());

    virtual_machine vm;
    test_host_integration host;
    code_generator gen;

    CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &host)));
    CHECK(gen.push_global_init());

    SUBTEST_ARGS(run_vm, vm, &host);
    CHECK_EQ(host.host_var, 246);
}

TEST_CASE(list_operator) {
    const auto ast = get_ast("x = 4 [2, 3]", test_host_integration::host_identifier());

    virtual_machine vm;
    test_host_integration host;
    code_generator gen;

    CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &host)));
    CHECK(gen.push_global_init());
    {
        using step = virtual_machine::step_result;
        CHECK_EQ(vm.step(&host), (step{step::Return, 0}));
        CHECK_EQ(vm.step(&host), (step{step::Stop, 0}));
        CHECK_EQ(vm.data_stack.size(), 4);
        CHECK_EQ(vm.data_stack[3].u, 20);
    }
}

TEST_CASE(switch_execution) {
    const auto ast = get_ast(R"PRU(
x = {a, b, c, d}
proc foo(s)
  switch s
  case :a
    return 2
  case [:b, :c]
    return 4
  else
    return 6
  end
end
a1 = foo(:x:a)
a2 = foo(:x:c)
a3 = foo(:x:d)
)PRU", test_host_integration::host_identifier());

    virtual_machine vm;
    test_host_integration host;
    code_generator gen;

    CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &host)));
    CHECK(gen.push_global_init());

    SUBTEST_ARGS(run_vm, vm, &host);
    CHECK_EQ(vm.data_stack.size(), 6);
    CHECK_EQ(vm.data_stack[3].u, 2);
    CHECK_EQ(vm.data_stack[4].u, 4);
    CHECK_EQ(vm.data_stack[5].u, 6);
}

TEST_CASE(nested_for_loop) {
    const auto ast = get_ast(R"PRU(
proc foo()
  var x = 0
  for i in 10
    y = i
    for j in 5
      x := 1 [x, 1]
      if less(5553, x)
        return x
      end
    end
    x := x [10]
  end
end
z = foo()
)PRU", test_host_integration::host_identifier());

    virtual_machine vm;
    test_host_integration host;
    code_generator gen;

    CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &host)));
    CHECK(gen.push_global_init());

    SUBTEST_ARGS(run_vm, vm, &host);
    CHECK_EQ(vm.data_stack.back().u, 5554);
    CHECK(vm.call_stack.empty());
}
