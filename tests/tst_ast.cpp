#define KOUTEST_STD_STRING_HANDLING
#include <koutest.h>

#include <pruto/error_messages.h>

namespace pruto {
    inline std::string koutest_debug_string(ident id) { return to_string(id); }
    inline std::string koutest_debug_string(ExpressionError e) { return to_string(e); }
    inline std::string koutest_debug_string(StatementError e) { return to_string(e); }
    inline std::string koutest_debug_string(AstError e) { return to_string(e); }
}

using namespace pruto;


TEST_CASE(basic_example) {

    static const char example[] = R"PRU(
; This is a comment

; global constant:
foo = 123

; global boolean variable
var bar = false; inline comment after statement

; procedure definition
proc p()
    return "Hello world"
end

; procedure without return value
proc set_bar()
    bar := true ; var reassignment with :=
end

; procedure alias
proc baz = p

; all other statements must be inside a procedure definition
proc q(i, j)

    if bar
        return i
    else if i != 10
        ; ...
    else
        set_bar() ; call
    end

    var z = i ; parameter are constants, so need explicit var copy
    while z > j
        tmp = z - 1 ; local constant scoped to the while block
        z := tmp
        if z % 2 == 0
            continue
        end
        ; ...
        break
    end

    ; similar to python, for expressions to span multiple lines, they must be nested somehow
    longexp = (
        3 + 4 * ; inline comment
        5!
        / (9 - j)
    )

    ; string
    shortstr = "as expected, with backslash escaping of quote \" newline \n and backslash \\"

    ; multiline string, equivalent to "abc\n\n def\nghi"
    longstr = "
              > abc
              >
              >  def
              > ghi
              "

    ; list expression
    l = [2, 4, 6]

    for x in l
        z := z + x - l[0] ; list expressions may function as a right-unary operator
    end

    ; struct expression
    mystruct = { a: true, b: 42, c: baz(),
        d,              ; semantically same as "d: {}" - empty structs are allowed
        e.x.y: 100      ; semantically same as "e: {x: {y: 100}}"
    }

    ; member access
    z := mystruct.b

    ; a struct implicitly defines an enum of its member names which can be used by enum expressions of the form ":structname:membername"
    myenum = :mystruct:c

    ; switch statements work exclusively on these enums
    switch myenum
    case :b
        ; ...
        ; no fallthrough, each case is a proper scope
    case [:c, :e]
        ; since myenum was assigned the 'c' member, this path is taken
        ; ...
    else
        ; run for all unmentioned cases (here 'a' and 'd') - reusing "else" instead of "default" as in C
    end

    return j
end

; sections allow for multiple more or less independent scripts inside a single file

$ newScriptSection

; ... new section code

; testing some syntactically odd but valid corner cases:
   proc   p  ( x
       , y   )
    true_x = false
    var _123 = 55.45_234
    return {x: x
    ,
    z:
        [(~y# ~ 1 |^ hostproc(1 && 2))
         \ 4 |^ x[4, 2]]
    }
  end
)PRU";

    static const char example_ast_str[] =
R"AST(global_const(0) foo = 123
global_var(1) bar = false
proc global_proc_ret(2, 0) p()
return "Hello world"
end
proc global_proc(3, 0) set_bar()
global_var(1) := true
end
proc global_proc_ret(4, 2) q(i, j)
if global_var(1)
return local_const(0)
else if call:host_proc_ret(6, 2)(local_const(0), 10)
else
call:global_proc(3, 0)()
end
local_var(2) z = local_const(0)
while call:host_proc_ret(7, 2)(local_var(2), local_const(1))
local_const(3) tmp = call:host_proc_ret(4, 2)(local_var(2), 1)
local_var(2) := local_const(3)
if call:host_proc_ret(5, 2)(call:host_proc_ret(2, 2)(local_var(2), 2), 0)
continue
end
break
end
local_const(4) longexp = call:host_proc_ret(3, 2)(3, call:host_proc_ret(1, 2)(call:host_proc_ret(0, 2)(4, call:host_proc_ret(8, 1)(5)), call:host_proc_ret(4, 2)(9, local_const(1))))
local_const(5) shortstr = "as expected, with backslash escaping of quote \" newline \n and backslash \\"
local_const(6) longstr = "abc\n\n def\nghi"
local_const(7) l = [2, 4, 6]
for local_const(8) x in local_const(7)
local_var(2) := call:host_proc_ret(4, 2)(call:host_proc_ret(3, 2)(local_var(2), local_const(8)), local_const(7)[0])
end
local_const(9) mystruct = {a: true, b: 42, c: call:global_proc_ret(2, 0)(), d: {}, e: {x: {y: 100}}}
local_var(2) := local_const(9).b
local_const(10) myenum = :local_const(9):c
switch local_const(10)
case [:b]
case [:c, :e]
else
end
return local_const(1)
end
)AST";

    unsigned i = 0;
    const auto ast = section_ast(example, {}, {
                                     {SymOp::Asterisk, binary_operator(i++, 0)},
                                     {SymOp::Slash, binary_operator(i++, 0)},
                                     {SymOp::Percent, binary_operator(i++, 0)},
                                     {SymOp::Plus, binary_operator(i++, 1)},
                                     {SymOp::Minus, binary_operator(i++, 1)},
                                     {SymOp::Equal, binary_operator(i++, 2)},
                                     {SymOp::NotEqual, binary_operator(i++, 2)},
                                     {SymOp::Greater, binary_operator(i++, 2)},
                                     {SymOp::Exclamation, right_unary_operator(i++)},
                                 });

    if( ast.error )
        FAIL(error_message(ast, example).data());
    CHECK_EQ(ast.section.globals.size(), 5);
    CHECK_EQ(to_string(ast.section), example_ast_str);
    {
        stm_data_def g;
        g.line = 4;
        g.name = "foo";
        g.id = ident::global_const(0);
        exp_num_lit e1;
        e1.line = 4;
        e1.pos = 6;
        e1.val = "123";
        g.e.push_back(e1);
        CHECK((ast.section.globals.at(0) == std::variant<stm_proc_def, stm_data_def>(std::move(g))));
    }
    {
        stm_data_def g;
        g.line = 7;
        g.name = "bar";
        g.id = ident::global_var(1);
        exp_bool_lit e1;
        e1.line = 7;
        e1.pos = 10;
        e1.val = false;
        g.e.push_back(e1);
        CHECK((ast.section.globals.at(1) == std::variant<stm_proc_def, stm_data_def>(std::move(g))));
    }
    {
        stm_proc_def g;
        g.line = 10;
        g.name = "p";
        g.id = ident::global_proc_ret(2, 0);

        stm_return_value s1;
        s1.line = 11;

        exp_string_lit e1;
        e1.line = 11;
        e1.pos = 11;
        e1.val = "Hello world";
        s1.e.push_back(std::move(e1));

        g.statements.push_back(std::move(s1));
        g.statements.emplace_back(stm_end{12});
        CHECK((ast.section.globals.at(2) == std::variant<stm_proc_def, stm_data_def>(std::move(g))));
    }
    {
        stm_proc_def g;
        g.line = 15;
        g.name = "set_bar";
        g.id = ident::global_proc(3, 0);

        stm_reassign s1;
        s1.line = 16;
        s1.id = ident::global_var(1);

        exp_bool_lit e1;
        e1.line = 16;
        e1.pos = 11;
        e1.val = true;
        s1.e.push_back(std::move(e1));

        g.statements.push_back(std::move(s1));
        g.statements.emplace_back(stm_end{17});
        CHECK((ast.section.globals.at(3) == std::variant<stm_proc_def, stm_data_def>(std::move(g))));
    }
    {
        CHECK(std::holds_alternative<stm_proc_def>(ast.section.globals.at(4)));
        const auto& g = std::get<stm_proc_def>(ast.section.globals.at(4));

        CHECK_EQ(g.line, 23);
        CHECK_EQ(g.name, "q");
        CHECK_EQ(g.id, ident::global_proc_ret(4, 2));
        CHECK(g.parameters == std::vector<std::string>({"i", "j"}));

        CHECK_EQ(g.statements.size(), 32);

        {
            CHECK(std::holds_alternative<stm_data_def>(g.statements.at(6)));
            const auto& s = std::get<stm_data_def>(g.statements.at(6));
            CHECK_EQ(s.line, 33);
            CHECK_EQ(s.name, "z");
            CHECK_EQ(s.id, ident::local_var(2));
        }
        {
            CHECK(std::holds_alternative<stm_data_def>(g.statements.at(8)));
            const auto& s = std::get<stm_data_def>(g.statements.at(8));
            CHECK_EQ(s.line, 35);
            CHECK_EQ(s.name, "tmp");
            CHECK_EQ(s.id, ident::local_const(3));
        }
        {
            CHECK(std::holds_alternative<stm_data_def>(g.statements.at(15)));
            const auto& s = std::get<stm_data_def>(g.statements.at(15));
            CHECK_EQ(s.line, 45);
            CHECK_EQ(s.name, "longexp");
            CHECK_EQ(s.id, ident::local_const(4));
        }
    }
    {
        CHECK_EQ(ast.section.end_line, 98);
        i = 1;
        const auto ast2 = section_ast(example,
                                      {{std::string("hostproc"), ident::host_proc_ret(0, 1)}}, {
                                          {SymOp::Backslash, binary_operator(i++, 0)},
                                          {SymOp::BarCaret, binary_operator(i++, 1)},
                                          {SymOp::Tilde, op_binary_and_left_unary{
                                               binary_operator(i++, 0), left_unary_operator(i++)
                                           }},
                                          {SymOp::AndAnd, binary_operator(i++, 0)},
                                          {SymOp::Hash, right_unary_operator(i++)},
                                      }, ast.section);
        if( ast2.error )
            FAIL(error_message(ast2, example).data());
        CHECK_EQ(ast2.section.name, "newScriptSection");
        CHECK_EQ(ast2.section.end_line, 115);
        CHECK_EQ(ast2.section.end_line_byte_offset, sizeof(example));

        CHECK(std::holds_alternative<stm_proc_def>(ast2.section.globals.at(0)));
        const auto& g = std::get<stm_proc_def>(ast2.section.globals.at(0));
        CHECK_EQ(g.line, 103);
    }

    const uint32_t findable_statement_lines[] = {
        4, 7, 10, 11, 12, 15, 16, 17, 23, 25, 26, 27, 29, 30, 31, 33, 34, 35, 36, 37, 38, 39, 41, 42, 45,
        52, 55, 63, 65, 66, 67, 70, 76, 79, 82, 83, 86, 89, 91, 93, 94
    };
    for( uint32_t i = 0; i < 100; ++i ) {
        const auto loc = ast_find_statement(ast.section, i);
        if( !loc.proc_id && loc.statement == ast.section.globals.size() ) {
            for( size_t j = 0; j < sizeof(findable_statement_lines) / sizeof(uint32_t); ++j )
                CHECK_NOT_EQ(i, findable_statement_lines[j]);
            continue;
        }
        const auto line = ast_find_statement_script_line(ast.section, loc);
        CHECK_EQ(line, i);
    }
}


template<typename Err>
static void check_error(const ast_result& ast, Err e, int pos, int size)
{
    if( !ast.error )
        FAIL("Expected error missing");
    if( !std::holds_alternative<Err>(ast.error->val) )
        FAIL(to_string(*ast.error).data());
    CHECK_EQ(std::get<Err>(ast.error->val), e);
    CHECK_EQ(ast.error->pos, pos);
    CHECK_EQ(ast.error->size, size);
}

template<typename Err>
static void check_error(const char* script, Err e, int pos, int size)
{
    ast_result ast = section_ast(script, {}, {});
    check_error(ast, e, pos, size);
}

static void exp_unexpected_char_at(const char* script, int pos)
{
    check_error(script, ExpressionError::UnexpectedCharacter, pos, 1);
}

TEST_CASE(exp_error) {
    SUBTEST_ARGS(exp_unexpected_char_at, "a=[1", 4);
    SUBTEST_ARGS(exp_unexpected_char_at, "a={?", 3);
    SUBTEST_ARGS(exp_unexpected_char_at, "a={x", 4);
    SUBTEST_ARGS(exp_unexpected_char_at, "a=(1", 4);
    SUBTEST_ARGS(exp_unexpected_char_at, "a=:b?", 4);
    SUBTEST_ARGS(check_error, "a=if", ExpressionError::UnexpectedKeyword, 2, 2);
    SUBTEST_ARGS(check_error, "a=:true", ExpressionError::UnexpectedKeyword, 3, 4);
    SUBTEST_ARGS(check_error, "a=:?", ExpressionError::UnexpectedNonEnumColon, 2, 1);
    SUBTEST_ARGS(check_error, "a==", ExpressionError::UnexpectedAssignSymbol, 2, 1);
    SUBTEST_ARGS(check_error, "a=", ExpressionError::MissingOperand, 2, 0);
    SUBTEST_ARGS(check_error, "a=1 2", ExpressionError::MissingOperatorBetweenOperands, 4, 1);
    SUBTEST_ARGS(check_error, "a=1 :b.c:d", ExpressionError::MissingOperatorBetweenOperands, 4, 6);
    SUBTEST_ARGS(check_error, "a= {b}\nc=1a.b", ExpressionError::MissingOperatorBetweenOperands, 3, 3);
    SUBTEST_ARGS(check_error, "a= {b}\nc=a.b()", ExpressionError::MissingOperatorBetweenOperands, 5, 1);
    SUBTEST_ARGS(check_error, "a=(((((((((((", ExpressionError::NestingLimitExceeded, 12, 1);
    {
        ast_result ast = section_ast("a=([{)", {}, {});
        SUBTEST_ARGS(check_error, ast, ExpressionError::UnexpectedCharacter, 5, 1);
        CHECK_EQ(ast.error->exp_nesting.size(), 3);
        CHECK(ast.error->exp_nesting.at(0).type == ExpressionNesting::Group);
        CHECK(ast.error->exp_nesting.at(1).type == ExpressionNesting::List);
        CHECK(ast.error->exp_nesting.at(2).type == ExpressionNesting::Struct);
        CHECK_EQ(ast.error->exp_nesting.at(2).line, 0);
        CHECK_EQ(ast.error->exp_nesting.at(2).pos, 4);
    }
}

TEST_CASE(stm_error) {
    SUBTEST_ARGS(check_error, "?", StatementError::UnexpectedCharacter, 0, 1);
    SUBTEST_ARGS(check_error, "in", StatementError::UnexpectedKeyword, 0, 2);
    SUBTEST_ARGS(check_error, "return", StatementError::KeywordOutsideProc, 0, 6);
    SUBTEST_ARGS(check_error, "a(", StatementError::CallOutsideProc, 0, 1);
    SUBTEST_ARGS(check_error, "a:=", StatementError::ReassignOutsideProc, 1, 2);
    SUBTEST_ARGS(check_error, "proc a()\nproc b(", StatementError::NestedProcDefinition, 0, 6);
    SUBTEST_ARGS(check_error, "proc a()\nb.c=", StatementError::UnexpectedCharacter, 3, 1);
    SUBTEST_ARGS(check_error, "proc a()\nelse", StatementError::ElseOutsideConditional, 0, 4);
    SUBTEST_ARGS(check_error, "proc a()\nif 1\nelse a", StatementError::UnexpectedWordAfterElse, 5, 1);
    SUBTEST_ARGS(check_error, "proc a()\nbreak", StatementError::BreakOutsideLoop, 0, 5);
    SUBTEST_ARGS(check_error, "proc a()\ncontinue", StatementError::ContinueOutsideLoop, 0, 8);
    SUBTEST_ARGS(check_error, "proc a()\ncase", StatementError::CaseOutsideSwitch, 0, 4);
    SUBTEST_ARGS(check_error, "proc a()\nswitch 1\nx", StatementError::NeedCaseAfterSwitch, 0, 1);
    SUBTEST_ARGS(check_error, "proc a()\nfor b x", StatementError::NeedInAfterForIdent, 6, 1);
    SUBTEST_ARGS(check_error, "proc a()\nif(", StatementError::MissingSpaceAfterIf, 2, 1);
    SUBTEST_ARGS(check_error, "proc a()\nswitch(", StatementError::MissingSpaceAfterSwitch, 6, 1);
    SUBTEST_ARGS(check_error, "proc a()\nswitch 1\ncase:", StatementError::MissingSpaceAfterCase, 4, 1);
    SUBTEST_ARGS(check_error, "proc a()\nwhile(", StatementError::MissingSpaceAfterWhile, 5, 1);
    SUBTEST_ARGS(check_error, "proc a()\nfor b in(", StatementError::MissingSpaceAfterForIn, 8, 1);
    SUBTEST_ARGS(check_error, "proc a()\nreturn(", StatementError::MissingSpaceAfterReturn, 6, 1);
    SUBTEST_ARGS(check_error, "proc a()\n$", StatementError::SectionInsideProc, 0, 1);
    SUBTEST_ARGS(check_error, " $", StatementError::WhitespaceBeforeSection, 0, 1);
}

TEST_CASE(ast_error) {
    SUBTEST_ARGS(check_error, "x=y", AstError::UnknownName, 2, 1);
    SUBTEST_ARGS(check_error, "x=1\n x=2", AstError::InvalidRedefine, 1, 1);
    SUBTEST_ARGS(check_error, "proc a()\n x=1\n x:=2", AstError::ReassignNotVar, 1, 1);
    SUBTEST_ARGS(check_error, "x=!1", AstError::UndefinedOperator, 2, 1);
    {
        const auto ast = section_ast("x=1+", {}, {{SymOp::Plus, left_unary_operator(0)}});
        SUBTEST_ARGS(check_error, ast, AstError::LeftUnaryAfterOperand, 3, 1);
    }
    {
        const auto ast = section_ast("x=+1", {}, {{SymOp::Plus, right_unary_operator(0)}});
        SUBTEST_ARGS(check_error, ast, AstError::RightUnaryBeforeOperand, 2, 1);
    }
    {
        const auto ast = section_ast("x=1+1", {}, {{SymOp::Plus, right_unary_operator(0)}});
        SUBTEST_ARGS(check_error, ast, AstError::MissingBinaryOperatorBetweenOperands, 4, 1);
    }
    {
        const auto ast = section_ast("x=+1", {}, {{SymOp::Plus, binary_operator(42, 1)}});
        SUBTEST_ARGS(check_error, ast, AstError::BinaryOperatorMissingLeftOperand, 2, 1);
        CHECK(ast.error->call.id.is_host());
        CHECK(ast.error->call.id.is_proc());
        CHECK_EQ(ast.error->call.id.index(), 42);
        CHECK_EQ(ast.error->call.id.proc_parameter_count(), 2);
        CHECK_EQ(ast.error->call.line, 0);
        CHECK_EQ(ast.error->call.pos, 2);
    }
    {
        const auto ast = section_ast("x=1+", {}, {{SymOp::Plus, binary_operator(42, 1)}});
        SUBTEST_ARGS(check_error, ast, AstError::BinaryOperatorMissingRightOperand, 4, 1);
        CHECK(ast.error->call.id.is_host());
        CHECK(ast.error->call.id.is_proc());
        CHECK_EQ(ast.error->call.id.index(), 42);
        CHECK_EQ(ast.error->call.id.proc_parameter_count(), 2);
        CHECK_EQ(ast.error->call.line, 0);
        CHECK_EQ(ast.error->call.pos, 3);
    }
    SUBTEST_ARGS(check_error, "x=1\n y=x()", AstError::NameIsNotProc, 3, 1);
    {
        const auto ast = section_ast("proc p()\n end\n x=p()", {}, {});
        SUBTEST_ARGS(check_error, ast, AstError::ProcHasNoValue, 5, 1);
        CHECK(ast.error->call.id.is_global());
        CHECK(ast.error->call.id.is_proc());
        CHECK(!ast.error->call.id.proc_has_retval());
        CHECK_EQ(ast.error->call.id.index(), 0);
        CHECK_EQ(ast.error->call.id.proc_parameter_count(), 0);
        CHECK_EQ(ast.error->call.line, 2);
        CHECK_EQ(ast.error->call.pos, 3);
    }
    {
        const auto ast = section_ast("proc p()\nreturn 1\nend\nproc q()\np()", {}, {});
        SUBTEST_ARGS(check_error, ast, AstError::ProcHasValue, 2, 1);
        CHECK(ast.error->call.id.is_global());
        CHECK(ast.error->call.id.is_proc());
        CHECK(ast.error->call.id.proc_has_retval());
        CHECK_EQ(ast.error->call.id.index(), 0);
        CHECK_EQ(ast.error->call.id.proc_parameter_count(), 0);
        CHECK_EQ(ast.error->call.line, 4);
        CHECK_EQ(ast.error->call.pos, 0);
    }
    SUBTEST_ARGS(check_error, "proc p(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16",
                 AstError::ProcTooManyParameter, 73, 3);
    {
        const auto ast = section_ast("proc p()\n return 1\n end\n x=p(2)", {}, {});
        SUBTEST_ARGS(check_error, ast, AstError::ProcArgParameterCountMismatch, 6, 1);
        CHECK(ast.error->call.id.is_global());
        CHECK(ast.error->call.id.is_proc());
        CHECK_EQ(ast.error->call.id.index(), 0);
        CHECK_EQ(ast.error->call.id.proc_parameter_count(), 0);
        CHECK_EQ(ast.error->call.line, 3);
        CHECK_EQ(ast.error->call.pos, 3);
        CHECK_EQ(ast.error->call_args, 1);
    }
    SUBTEST_ARGS(check_error, "proc p()\n if 1\n return 1\n else\n return\n", AstError::ProcReturnValueMismatch, 1, 6);
    SUBTEST_ARGS(check_error, "x = {a: 1, a: 2}\n", AstError::RepeatedStructMemberName, 11, 1);
    {
        static const char repeated_case[] = R"PRU(
proc p()
    switch 1
    case :a
    case :b
        switch 2
            case :b
        end
    case [:c, :d, :a]
    end
end
)PRU";
        SUBTEST_ARGS(check_error, repeated_case, AstError::RepeatedCaseLabel, 19, 1);
    }
    SUBTEST_ARGS(check_error, "proc p()\n if 1\n if 2\n if 3\n if 4\n if 5\n if 6\n if 7\n if 8\n if 9\n if 10\n",
                 AstError::BlockNestingLimitExceeded, 1, 2);
}

TEST_CASE(operators) {
    const auto ast = section_ast("x = 1 + (2 * 3 + 4) * (-5! - -6)", {}, {
                                     {SymOp::Asterisk, binary_operator(0, 1)},
                                     {SymOp::Slash, binary_operator(1, 1)},
                                     {SymOp::Plus, binary_operator(2, 2)},
                                     {SymOp::Minus, op_binary_and_left_unary{ binary_operator(3, 2), left_unary_operator(4) }},
                                     {SymOp::Exclamation, right_unary_operator(5)},
                                 });

    if( ast.error )
        FAIL(to_string(*ast.error).data());
    CHECK_EQ(ast.section.globals.size(), 1);
    CHECK(std::holds_alternative<stm_data_def>(ast.section.globals.front()));
    const auto& stm = std::get<stm_data_def>(ast.section.globals.front());
    CHECK_EQ(stm.name, "x");
    const auto& e = stm.e;
    CHECK_EQ(e.size(), 14);

    CHECK(std::holds_alternative<exp_num_lit>(e.at(0)));
    CHECK_EQ(std::get<exp_num_lit>(e.at(0)).val, "1");

    CHECK(std::holds_alternative<exp_num_lit>(e.at(1)));
    CHECK_EQ(std::get<exp_num_lit>(e.at(1)).val, "2");
    CHECK(std::holds_alternative<exp_num_lit>(e.at(2)));
    CHECK_EQ(std::get<exp_num_lit>(e.at(2)).val, "3");
    CHECK(std::holds_alternative<exp_call>(e.at(3)));
    CHECK_EQ(std::get<exp_call>(e.at(3)).id, ident::host_proc_ret(0, 2));
    CHECK(std::holds_alternative<exp_num_lit>(e.at(4)));
    CHECK_EQ(std::get<exp_num_lit>(e.at(4)).val, "4");
    CHECK(std::holds_alternative<exp_call>(e.at(5)));
    CHECK_EQ(std::get<exp_call>(e.at(5)).id, ident::host_proc_ret(2, 2));

    CHECK(std::holds_alternative<exp_num_lit>(e.at(6)));
    CHECK_EQ(std::get<exp_num_lit>(e.at(6)).val, "5");
    CHECK(std::holds_alternative<exp_call>(e.at(7)));
    CHECK_EQ(std::get<exp_call>(e.at(7)).id, ident::host_proc_ret(4, 1));
    CHECK(std::holds_alternative<exp_call>(e.at(8)));
    CHECK_EQ(std::get<exp_call>(e.at(8)).id, ident::host_proc_ret(5, 1));
    CHECK(std::holds_alternative<exp_num_lit>(e.at(9)));
    CHECK_EQ(std::get<exp_num_lit>(e.at(9)).val, "6");
    CHECK(std::holds_alternative<exp_call>(e.at(10)));
    CHECK_EQ(std::get<exp_call>(e.at(10)).id, ident::host_proc_ret(4, 1));
    CHECK(std::holds_alternative<exp_call>(e.at(11)));
    CHECK_EQ(std::get<exp_call>(e.at(11)).id, ident::host_proc_ret(3, 2));

    CHECK(std::holds_alternative<exp_call>(e.at(12)));
    CHECK_EQ(std::get<exp_call>(e.at(12)).id, ident::host_proc_ret(0, 2));

    CHECK(std::holds_alternative<exp_call>(e.at(13)));
    CHECK_EQ(std::get<exp_call>(e.at(13)).id, ident::host_proc_ret(2, 2));

    for( int i = 0; i <= static_cast<int>(SymOp::Equal); ++i ) {
        const auto s = to_string(static_cast<SymOp>(i));
        CHECK_EQ(s.size(), sym_op_size(s.data()));
        CHECK_EQ(i, static_cast<int>(sym_op(s.data(), s.size())));
    }
}

TEST_CASE(ast_error_unbalanced_exp) {
    SUBTEST_ARGS(check_error, "proc a()\nx=(1\nif x", ExpressionError::UnexpectedKeyword, 0, 2);
    SUBTEST_ARGS(check_error, "proc a()\nx=(1\ny=1", ExpressionError::MissingOperatorBetweenOperands, 0, 1);
    SUBTEST_ARGS(check_error, "proc a()\nx=(\ny=1", AstError::UnknownName, 0, 1);
    SUBTEST_ARGS(check_error, "proc a()\nvar x=1\ny=(\nx:=2", ExpressionError::UnexpectedNonEnumColon, 1, 1);
    SUBTEST_ARGS(check_error, "proc a()\nvar x=1\ny=(\nx=2", ExpressionError::UnexpectedAssignSymbol, 1, 1);
    {
        const auto ast = section_ast("proc a()\nx=(1+\ny=1", {}, {{SymOp::Plus, right_unary_operator(0)}});
        SUBTEST_ARGS(check_error, ast, AstError::MissingBinaryOperatorBetweenOperands, 0, 1);
    }
    SUBTEST_ARGS(check_error, "proc a()\nend\nproc b()\nx=(\na()", AstError::ProcHasNoValue, 2, 1);
}
