#define KOUTEST_STD_STRING_HANDLING
#include <koutest.h>

#include <pruto/error_messages.h>

#include <cstdlib>

namespace pruto {
    inline std::string koutest_debug_string(ident id) { return to_string(id); }
    inline std::string koutest_debug_string(TypeCheckError e) { return to_string(e); }
    inline std::string koutest_debug_string(const type_info& t) { type_stringifier s; return s(t); }
    inline std::string koutest_debug_string(const std::vector<virtual_machine::instruction>& bytecode) { return to_string(bytecode); }
    inline std::string koutest_debug_string(virtual_machine::step_result step) { return to_string(step); }
}

using namespace pruto;
using OpCode = virtual_machine::instruction::OpCode;

static script_section_ast get_ast(const char* script, const host_identifier_table& host_entries = {})
{
    const auto ast = section_ast(script, host_entries, {});
    if( ast.error )
        FAIL(error_message(ast, script).data());
    return std::move(ast.section);
}

static code_generator::reset_result get_reset_result(const char* script)
{
    const auto ast = get_ast(script);
    virtual_machine vm;
    code_generator gen;
    code_generator::host_integration default_host;
    return gen.reset(&ast, &vm, &default_host);
}

TEST_CASE(unsupported_literal) {
    {
        const auto res = get_reset_result("x = true\ny = 123");
        CHECK(std::holds_alternative<code_generator::literal_error>(res));
        const auto& e = std::get<code_generator::literal_error>(res);
        CHECK(e.val == LiteralError::HostDisabledNumLiterals);
        CHECK_EQ(e.proc_id, ident());
        CHECK_EQ(e.statement, 1);
        CHECK_EQ(e.expression_element, 0);
    }

    {
        const auto res = get_reset_result(R"PRU(
a = false
proc foo(x, y, z)
    return z
end
proc bar(x1, x2, x3, x4, x5)
    return foo(true, {}, "baz")
end
)PRU");
        CHECK(std::holds_alternative<code_generator::literal_error>(res));
        const auto& e = std::get<code_generator::literal_error>(res);
        CHECK(e.val == LiteralError::HostDisabledStrLiterals);
        CHECK_EQ(e.proc_id, ident::global_proc_ret(2, 5));
        CHECK_EQ(e.statement, 0);
        CHECK_EQ(e.expression_element, 2);
    }
}

TEST_CASE(type_errors) {
    {
        const auto res = get_reset_result("x = {a, b, c}\ny = x.d");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::InvalidMember);
        CHECK_EQ(e.sub_index, 1);
        CHECK_EQ(e.location.size(), 1);
        CHECK_EQ(e.location.front().proc_id, ident());
        CHECK_EQ(e.location.front().statement, 1);
        CHECK_EQ(e.location.front().expression_element, 0);
    }
    {
        const auto res = get_reset_result("x = {a, b, c}\ny = :x:d");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::InvalidMember);
        CHECK_EQ(e.sub_index, 1);
        CHECK_EQ(e.location.size(), 1);
        CHECK_EQ(e.location.front().proc_id, ident());
        CHECK_EQ(e.location.front().statement, 1);
        CHECK_EQ(e.location.front().expression_element, 0);
    }
    {
        const auto res = get_reset_result("x = {a: {b: {c}}}\ny = x.a.b.c.d");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::InvalidMember);
        CHECK_EQ(e.sub_index, 4);
    }
    {
        const auto res = get_reset_result("x = {a: {b: {c}}}\ny = :x.a.b.c:d");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::InvalidMember);
        CHECK_EQ(e.sub_index, 4);
    }
    {
        const auto res = get_reset_result(R"PRU(
proc foo()
    x = {a, b, c}
    switch :x:b
    case [:a, :b, :d]
    end
    return true
end
z = foo()
)PRU");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::InvalidMember);
        CHECK_EQ(e.sub_index, 2);
        CHECK_EQ(e.location.size(), 2);
        CHECK_EQ(e.location.front().proc_id, ident::global_proc_ret(0, 0));
        CHECK_EQ(e.location.front().statement, 2);
        CHECK_EQ(e.location.front().expression_element, -1);
    }
    {
        const auto res = get_reset_result("x = {a: true}\ny = x.a.b");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::RefNotAStruct);
        CHECK_EQ(e.sub_index, 1);
        CHECK_EQ(e.location.size(), 1);
        CHECK_EQ(e.location.front().proc_id, ident());
        CHECK_EQ(e.location.front().statement, 1);
        CHECK_EQ(e.location.front().expression_element, 0);
        CHECK_EQ(e.mismatch_actual, type_info{type_boolean{}});
    }
    {
        const auto res = get_reset_result("x = [true, false, {}]");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::ListElementTypeMismatch);
        CHECK_EQ(e.sub_index, 2);
        CHECK_EQ(e.location.front().statement, 0);
        CHECK_EQ(e.location.front().expression_element, 3);
        CHECK_EQ(e.mismatch_expected, type_info{type_boolean{}});
        CHECK_EQ(e.mismatch_actual, type_info{type_struct{}});
    }
    {
        const auto res = get_reset_result(R"PRU(
proc foo()
    while {x: true}
        return false
    end
end
z = foo()
)PRU");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::ConditionalNotBoolean);
        type_struct s;
        s.members.push_back({"x", type_boolean{}});
        CHECK_EQ(e.mismatch_actual, type_info{std::move(s)});
    }
    {
        const auto res = get_reset_result(R"PRU(
proc foo()
end
proc bar()
    switch foo
    case :x
        return false
    end
end
z = bar()
)PRU");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::SwitchNotEnum);
        CHECK_EQ(e.mismatch_actual, type_info{type_proc{ident::global_proc(0, 0)}});
    }
    {
        const auto res = get_reset_result(R"PRU(
proc bar()
    x = {a, b, c, d, e}
    switch :x:a
    case :a
        return false
    case [:c, :e]
        return true
    end
end
z = bar()
)PRU");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::MissingCaseOrElse);
        CHECK(e.missing_cases == std::vector<std::string>({"b", "d"}));
    }
    {
        const auto res = get_reset_result(R"PRU(
proc foo()
    var a = {}
    a := true
    return a
end
z = foo()
)PRU");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::ReassignTypeMismatch);
        CHECK_EQ(e.mismatch_expected, type_info{type_struct{}});
        CHECK_EQ(e.mismatch_actual, type_info{type_boolean{}});
    }
    {
        const auto res = get_reset_result(R"PRU(
proc foo()
    return {}
    return false
end
z = foo()
)PRU");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::ReturnTypeMismatch);
        CHECK_EQ(e.mismatch_expected, type_info{type_struct{}});
        CHECK_EQ(e.mismatch_actual, type_info{type_boolean{}});
    }
    {
        const auto res = get_reset_result("x = []");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::HostRejetedList);
    }
    {
        const auto res = get_reset_result("x = {}[]");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::HostRejetedListOp);
    }
    {
        const auto res = get_reset_result(R"PRU(
proc foo()
    for i in {}
    end
    return false
end
z = foo()
)PRU");
        CHECK(std::holds_alternative<code_generator::prepare_result_error_ptr>(res));
        const auto& e = *std::get<code_generator::prepare_result_error_ptr>(res);
        CHECK(std::holds_alternative<TypeCheckError>(e.val));
        CHECK_EQ(std::get<TypeCheckError>(e.val), TypeCheckError::HostRejetedIter);
    }
}

TEST_CASE(reassignment) {
    const auto ast = get_ast(R"PRU(
proc foo()
    var x = {a: true, b: false, c: false}
    y = false
    var z = true
    x.c := true
    x.b := y
    z := x.a
    x := {a: z, b: x.c, c: y}
    return x
end
z = foo()
)PRU");

    virtual_machine vm;
    code_generator gen;
    {
        code_generator::host_integration default_host;
        CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &default_host)));
    }

    const std::vector<virtual_machine::instruction> expected({
         {OpCode::Invalid, 0},
         {OpCode::Invalid, 0},
         {OpCode::StartProc, 0},
         {OpCode::SizeArg, 0},
         {OpCode::UpdateAstPosition, 0},
         {OpCode::PushIndex, 1},
         {OpCode::PushIndex, 0},
         {OpCode::PushIndex, 0},
         {OpCode::UpdateAstPosition, 1},
         {OpCode::PushIndex, 0},
         {OpCode::UpdateAstPosition, 2},
         {OpCode::PushIndex, 1},
         {OpCode::UpdateAstPosition, 3},
         {OpCode::PushIndex, 1},
         {OpCode::PopMove, 2},
         {OpCode::SizeArg, 1},
         {OpCode::UpdateAstPosition, 4},
         {OpCode::PushCopy, 3},
         {OpCode::SizeArg, 1},
         {OpCode::PopMove, 1},
         {OpCode::SizeArg, 1},
         {OpCode::UpdateAstPosition, 5},
         {OpCode::PushCopy, 0},
         {OpCode::SizeArg, 1},
         {OpCode::PopMove, 4},
         {OpCode::SizeArg, 1},
         {OpCode::UpdateAstPosition, 6},
         {OpCode::PushCopy, 4},
         {OpCode::SizeArg, 1},
         {OpCode::PushCopy, 2},
         {OpCode::SizeArg, 1},
         {OpCode::PushCopy, 3},
         {OpCode::SizeArg, 1},
         {OpCode::PopMove, 0},
         {OpCode::SizeArg, 3},
         {OpCode::UpdateAstPosition, 7},
         {OpCode::PushCopy, 0},
         {OpCode::SizeArg, 3},
         {OpCode::Return, 3},
         {OpCode::UpdateAstPosition, 8},
         {OpCode::MissingReturnError, 0},
         {OpCode::StartProc, 16777215},
         {OpCode::SizeArg, 0},
         {OpCode::UpdateAstPosition, 1},
         {OpCode::CallProc, 2},
         {OpCode::PopMoveGlobal, 0},
         {OpCode::SizeArg, 3},
         {OpCode::IncrementGlobalDataInitCounter, 0},
         {OpCode::Return, 0}
    });
    CHECK_EQ(vm.byte_code, expected);
}

TEST_CASE(call_statement) {
    const auto ast = get_ast(R"PRU(
proc p1(x, y)
    return
end
proc p2()
    p1(true, false)
end
proc foo()
    p1(false, p2)
    p2()
    return {}
end
z = foo()
)PRU");

    virtual_machine vm;
    code_generator gen;
    {
        code_generator::host_integration default_host;
        CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &default_host)));
    }

    const std::vector<virtual_machine::instruction> expected({
         {OpCode::Invalid, 0},
         {OpCode::Invalid, 0},
         {OpCode::StartProc, 0},
         {OpCode::SizeArg, 1},
         {OpCode::UpdateAstPosition, 0},
         {OpCode::Return, 0},
         {OpCode::UpdateAstPosition, 1},
         {OpCode::Return, 0},
         {OpCode::StartProc, 1},
         {OpCode::SizeArg, 2},
         {OpCode::UpdateAstPosition, 0},
         {OpCode::Return, 0},
         {OpCode::UpdateAstPosition, 1},
         {OpCode::Return, 0},
         {OpCode::StartProc, 2},
         {OpCode::SizeArg, 0},
         {OpCode::UpdateAstPosition, 0},
         {OpCode::PushIndex, 1},
         {OpCode::PushIndex, 0},
         {OpCode::CallProc, 8},
         {OpCode::UpdateAstPosition, 1},
         {OpCode::Return, 0},
         {OpCode::StartProc, 3},
         {OpCode::SizeArg, 0},
         {OpCode::UpdateAstPosition, 0},
         {OpCode::PushIndex, 0},
         {OpCode::CallProc, 2},
         {OpCode::UpdateAstPosition, 1},
         {OpCode::CallProc, 14},
         {OpCode::UpdateAstPosition, 2},
         {OpCode::Return, 0},
         {OpCode::UpdateAstPosition, 3},
         {OpCode::MissingReturnError, 0},
         {OpCode::StartProc, 16777215},
         {OpCode::SizeArg, 0},
         {OpCode::UpdateAstPosition, 3},
         {OpCode::CallProc, 22},
         {OpCode::IncrementGlobalDataInitCounter, 0},
         {OpCode::Return, 0}
    });
    CHECK_EQ(vm.byte_code, expected);
}

TEST_CASE(if_statement) {
    const auto ast = get_ast(R"PRU(
proc p1()
end
proc p2(x)
end
proc p3()
    return true
end
proc foo()
    x = true
    if false
        p1()
    else if x
        y = x
        p2(y)
    else if p3()
    else
        p1()
    end
    return {}
end
z = foo()
)PRU");

    virtual_machine vm;
    code_generator gen;
    {
        code_generator::host_integration default_host;
        CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &default_host)));
    }

    const std::vector<virtual_machine::instruction> expected({
        {OpCode::Invalid, 0},
        {OpCode::Invalid, 0},
        {OpCode::StartProc, 0},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::Return, 0},
        {OpCode::StartProc, 1},
        {OpCode::SizeArg, 1},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::Return, 0},
        {OpCode::StartProc, 2},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::PushIndex, 1},
        {OpCode::Return, 1},
        {OpCode::UpdateAstPosition, 1},
        {OpCode::MissingReturnError, 0},
        {OpCode::StartProc, 3},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::PushIndex, 1},
        {OpCode::UpdateAstPosition, 1},
        {OpCode::PushIndex, 0},
        {OpCode::JumpIfZeroPop, 27},
        {OpCode::UpdateAstPosition, 2},
        {OpCode::CallProc, 2},
        {OpCode::Jump, 46},
        {OpCode::UpdateAstPosition, 3},
        {OpCode::PushCopy, 0},
        {OpCode::SizeArg, 1},
        {OpCode::JumpIfZeroPop, 40},
        {OpCode::UpdateAstPosition, 4},
        {OpCode::PushCopy, 0},
        {OpCode::SizeArg, 1},
        {OpCode::UpdateAstPosition, 5},
        {OpCode::PushCopy, 1},
        {OpCode::SizeArg, 1},
        {OpCode::CallProc, 6},
        {OpCode::Pop, 1},
        {OpCode::Jump, 46},
        {OpCode::UpdateAstPosition, 6},
        {OpCode::CallProc, 10},
        {OpCode::JumpIfZeroPop, 44},
        {OpCode::Jump, 46},
        {OpCode::UpdateAstPosition, 8},
        {OpCode::CallProc, 2},
        {OpCode::UpdateAstPosition, 10},
        {OpCode::Return, 0},
        {OpCode::UpdateAstPosition, 11},
        {OpCode::MissingReturnError, 0},
        {OpCode::StartProc, 16777215},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 4},
        {OpCode::CallProc, 17},
        {OpCode::IncrementGlobalDataInitCounter, 0},
        {OpCode::Return, 0}
    });
    CHECK_EQ(vm.byte_code, expected);
}

TEST_CASE(switch_statement) {
    const auto ast = get_ast(R"PRU(
proc p1()
end
proc p2(x)
end
proc foo()
    x = {a, b, c, d}
    switch :x:c
    case :a
        p1()
    case :b
        y = x
        p2(y)
    case [:c, :d]
        p1()
    end
    switch :x:b
    case :b
        p1()
    else
        p2(x)
    end
    return {}
end
z = foo()
)PRU");

    virtual_machine vm;
    code_generator gen;
    {
        code_generator::host_integration default_host;
        CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &default_host)));
    }

    const std::vector<virtual_machine::instruction> expected({
        {OpCode::Invalid, 0},
        {OpCode::Invalid, 0},
        {OpCode::StartProc, 0},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::Return, 0},
        {OpCode::StartProc, 1},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::Return, 0},
        {OpCode::StartProc, 2},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::UpdateAstPosition, 1},
        {OpCode::PushIndex, 2},
        {OpCode::JumpTable, 4},
        {OpCode::JumpTableEntry, 20},
        {OpCode::JumpTableEntry, 23},
        {OpCode::JumpTableEntry, 27},
        {OpCode::JumpTableEntry, 27},
        {OpCode::UpdateAstPosition, 3},
        {OpCode::CallProc, 2},
        {OpCode::Jump, 29},
        {OpCode::UpdateAstPosition, 5},
        {OpCode::UpdateAstPosition, 6},
        {OpCode::CallProc, 6},
        {OpCode::Jump, 29},
        {OpCode::UpdateAstPosition, 8},
        {OpCode::CallProc, 2},
        {OpCode::UpdateAstPosition, 10},
        {OpCode::PushIndex, 1},
        {OpCode::JumpTable, 4},
        {OpCode::JumpTableEntry, 39},
        {OpCode::JumpTableEntry, 36},
        {OpCode::JumpTableEntry, 39},
        {OpCode::JumpTableEntry, 39},
        {OpCode::UpdateAstPosition, 12},
        {OpCode::CallProc, 2},
        {OpCode::Jump, 41},
        {OpCode::UpdateAstPosition, 14},
        {OpCode::CallProc, 6},
        {OpCode::UpdateAstPosition, 16},
        {OpCode::Return, 0},
        {OpCode::UpdateAstPosition, 17},
        {OpCode::MissingReturnError, 0},
        {OpCode::StartProc, 16777215},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 3},
        {OpCode::CallProc, 10},
        {OpCode::IncrementGlobalDataInitCounter, 0},
        {OpCode::Return, 0}
    });
    CHECK_EQ(vm.byte_code, expected);
}

TEST_CASE(while_statement) {
    const auto ast = get_ast(R"PRU(
proc p1(x, y)
end
proc p2()
end
proc foo()
    var x = true
    while x
        y = {a: true, b: false}
        if y.b
            continue
        else if y.a
            z = false
            p2()
            break
        end
        z = false
        p1(x, z)
        x := false
    end
    return {}
end
z = foo()
)PRU");

    virtual_machine vm;
    code_generator gen;
    {
        code_generator::host_integration default_host;
        CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &default_host)));
    }

    const std::vector<virtual_machine::instruction> expected({
        {OpCode::Invalid, 0},
        {OpCode::Invalid, 0},
        {OpCode::StartProc, 0},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::Return, 0},
        {OpCode::StartProc, 1},
        {OpCode::SizeArg, 2},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::Return, 0},
        {OpCode::StartProc, 2},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::PushIndex, 1},
        {OpCode::UpdateAstPosition, 1},
        {OpCode::PushCopy, 0},
        {OpCode::SizeArg, 1},
        {OpCode::JumpIfZeroPop, 55},
        {OpCode::UpdateAstPosition, 2},
        {OpCode::PushIndex, 1},
        {OpCode::PushIndex, 0},
        {OpCode::UpdateAstPosition, 3},
        {OpCode::PushCopy, 2},
        {OpCode::SizeArg, 1},
        {OpCode::JumpIfZeroPop, 29},
        {OpCode::UpdateAstPosition, 4},
        {OpCode::Pop, 2},
        {OpCode::Jump, 14},
        {OpCode::Jump, 41},
        {OpCode::UpdateAstPosition, 5},
        {OpCode::PushCopy, 1},
        {OpCode::SizeArg, 1},
        {OpCode::JumpIfZeroPop, 41},
        {OpCode::UpdateAstPosition, 6},
        {OpCode::PushIndex, 0},
        {OpCode::UpdateAstPosition, 7},
        {OpCode::CallProc, 2},
        {OpCode::UpdateAstPosition, 8},
        {OpCode::Pop, 3},
        {OpCode::Jump, 55},
        {OpCode::Pop, 1},
        {OpCode::UpdateAstPosition, 10},
        {OpCode::PushIndex, 0},
        {OpCode::UpdateAstPosition, 11},
        {OpCode::PushCopy, 0},
        {OpCode::SizeArg, 1},
        {OpCode::PushCopy, 3},
        {OpCode::SizeArg, 1},
        {OpCode::CallProc, 6},
        {OpCode::UpdateAstPosition, 12},
        {OpCode::PushIndex, 0},
        {OpCode::PopMove, 0},
        {OpCode::SizeArg, 1},
        {OpCode::Pop, 3},
        {OpCode::Jump, 14},
        {OpCode::UpdateAstPosition, 14},
        {OpCode::Return, 0},
        {OpCode::UpdateAstPosition, 15},
        {OpCode::MissingReturnError, 0},
        {OpCode::StartProc, 16777215},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 3},
        {OpCode::CallProc, 10},
        {OpCode::IncrementGlobalDataInitCounter, 0},
        {OpCode::Return, 0}
    });
    CHECK_EQ(vm.byte_code, expected);
}

class test_host_integration : public code_generator::host_integration {
public:
    instance iterator_instance(const type_info& param) override
    {
        if( param != num_type() )
            return {};
        return {0, num_type()};
    }
    type_info num_type() override { return type_host{0}; }
    bool create_num(std::string_view literal, code_generator::data_stack_entry* retval) override
    {
        retval[0].u = 111;
        retval[1].u = unsigned(std::atoi(literal.data()));
        return true;
    }
    type_info str_type() override { return type_host{1}; }
    bool create_str(std::string_view literal, code_generator::data_stack_entry* retval) override
    {
        retval[0].u = 222;
        retval[1].u = literal.size();
        retval[2].u = 444;
        return true;
    }
    code_generator::stack_entry_count type_size(host_type_id type_id) override
    {
        switch( type_id ) {
        case 0: return 2;
        case 1: return 3;
        default:
            FAIL(std::to_string(type_id).data());
            return 9999;
        }
    }
};

TEST_CASE(for_statement) {
    const auto ast = get_ast(R"PRU(
proc foo()
    var x = 10
    for i in 5
        var y = false
        for j in 3
            if y
                return j
            else
                x := 100
                continue
            end
            z = "hello"
        end
        if y
            break
        end
    end
    return x
end
z = foo()
)PRU");

    virtual_machine vm;
    code_generator gen;
    {
        test_host_integration host;
        CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &host)));
    }

    const std::vector<virtual_machine::instruction> expected({
        {OpCode::Invalid, 0},
        {OpCode::Invalid, 0},
        {OpCode::StartProc, 0},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 0},
        {OpCode::PushCopyGlobal, 0},
        {OpCode::SizeArg, 2},
        {OpCode::UpdateAstPosition, 1},
        {OpCode::PushCopyGlobal, 2},
        {OpCode::SizeArg, 2},
        {OpCode::PushForLoop, 0},
        {OpCode::AdvanceForLoopOrJump, 55},
        {OpCode::UpdateAstPosition, 2},
        {OpCode::PushIndex, 0},
        {OpCode::UpdateAstPosition, 3},
        {OpCode::PushCopyGlobal, 4},
        {OpCode::SizeArg, 2},
        {OpCode::PushForLoop, 0},
        {OpCode::AdvanceForLoopOrJump, 44},
        {OpCode::UpdateAstPosition, 4},
        {OpCode::PushCopy, 4},
        {OpCode::SizeArg, 1},
        {OpCode::JumpIfZeroPop, 29},
        {OpCode::UpdateAstPosition, 5},
        {OpCode::PushCopy, 5},
        {OpCode::SizeArg, 2},
        {OpCode::PopForLoop, 2},
        {OpCode::Return, 2},
        {OpCode::Jump, 38},
        {OpCode::UpdateAstPosition, 7},
        {OpCode::PushCopyGlobal, 6},
        {OpCode::SizeArg, 2},
        {OpCode::PopMove, 0},
        {OpCode::SizeArg, 2},
        {OpCode::UpdateAstPosition, 8},
        {OpCode::Pop, 2},
        {OpCode::UpdateAstPosition, 3},
        {OpCode::Jump, 18},
        {OpCode::UpdateAstPosition, 10},
        {OpCode::PushCopyGlobal, 8},
        {OpCode::SizeArg, 3},
        {OpCode::Pop, 5},
        {OpCode::UpdateAstPosition, 3},
        {OpCode::Jump, 18},
        {OpCode::PopForLoop, 1},
        {OpCode::UpdateAstPosition, 12},
        {OpCode::PushCopy, 4},
        {OpCode::SizeArg, 1},
        {OpCode::JumpIfZeroPop, 52},
        {OpCode::UpdateAstPosition, 13},
        {OpCode::Pop, 3},
        {OpCode::Jump, 55},
        {OpCode::Pop, 3},
        {OpCode::UpdateAstPosition, 1},
        {OpCode::Jump, 11},
        {OpCode::PopForLoop, 1},
        {OpCode::UpdateAstPosition, 16},
        {OpCode::PushCopy, 0},
        {OpCode::SizeArg, 2},
        {OpCode::Return, 2},
        {OpCode::UpdateAstPosition, 17},
        {OpCode::MissingReturnError, 0},
        {OpCode::StartProc, 16777215},
        {OpCode::SizeArg, 0},
        {OpCode::UpdateAstPosition, 1},
        {OpCode::CallProc, 2},
        {OpCode::PopMoveGlobal, 11},
        {OpCode::SizeArg, 2},
        {OpCode::IncrementGlobalDataInitCounter, 0},
        {OpCode::Return, 0}
    });
    CHECK_EQ(vm.byte_code, expected);
}

TEST_CASE(global_data) {
    const auto ast = get_ast(R"PRU(
x = true
y = 123
proc foo()
  if x
    return "bar"
  end
  return "bazzz"
end
proc moo(s)
  return s
end
z = moo(foo())
abc = {ax: x, ay: y, az: z}
)PRU");

    virtual_machine vm;
    code_generator gen;

    {
        test_host_integration host;
        CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &host)));
    }

    {
        const std::vector<virtual_machine::instruction> expected({
             {OpCode::Invalid, 0},
             {OpCode::Invalid, 0},
             {OpCode::StartProc, 0},
             {OpCode::SizeArg, 0},
             {OpCode::UpdateAstPosition, 0},
             {OpCode::PushCopyGlobal, 0},
             {OpCode::SizeArg, 1},
             {OpCode::JumpIfZeroPop, 12},
             {OpCode::UpdateAstPosition, 1},
             {OpCode::PushCopyGlobal, 5},
             {OpCode::SizeArg, 3},
             {OpCode::Return, 3},
             {OpCode::UpdateAstPosition, 3},
             {OpCode::PushCopyGlobal, 8},
             {OpCode::SizeArg, 3},
             {OpCode::Return, 3},
             {OpCode::UpdateAstPosition, 4},
             {OpCode::MissingReturnError, 0},
             {OpCode::StartProc, 1},
             {OpCode::SizeArg, 3},
             {OpCode::UpdateAstPosition, 0},
             {OpCode::PushCopy, 0},
             {OpCode::SizeArg, 3},
             {OpCode::Return, 3},
             {OpCode::UpdateAstPosition, 1},
             {OpCode::MissingReturnError, 0},
             {OpCode::StartProc, 16777215},
             {OpCode::SizeArg, 0},
             {OpCode::UpdateAstPosition, 0},
             {OpCode::PushIndex, 1},
             {OpCode::PopMoveGlobal, 0},
             {OpCode::SizeArg, 1},
             {OpCode::IncrementGlobalDataInitCounter, 0},
             {OpCode::UpdateAstPosition, 1},
             {OpCode::PushCopyGlobal, 1},
             {OpCode::SizeArg, 2},
             {OpCode::PopMoveGlobal, 3},
             {OpCode::SizeArg, 2},
             {OpCode::IncrementGlobalDataInitCounter, 0},
             {OpCode::UpdateAstPosition, 4},
             {OpCode::CallProc, 2},
             {OpCode::CallProc, 18},
             {OpCode::PopMoveGlobal, 11},
             {OpCode::SizeArg, 3},
             {OpCode::IncrementGlobalDataInitCounter, 0},
             {OpCode::UpdateAstPosition, 5},
             {OpCode::PushCopyGlobal, 0},
             {OpCode::SizeArg, 1},
             {OpCode::PushCopyGlobal, 3},
             {OpCode::SizeArg, 2},
             {OpCode::PushCopyGlobal, 11},
             {OpCode::SizeArg, 3},
             {OpCode::PopMoveGlobal, 14},
             {OpCode::SizeArg, 6},
             {OpCode::IncrementGlobalDataInitCounter, 0},
             {OpCode::Return, 0}
        });
        CHECK_EQ(vm.byte_code, expected);
    }

    {
        CHECK_EQ(to_string(vm.byte_code), R"PRU({
{OpCode::Invalid, 0},
{OpCode::Invalid, 0},
{OpCode::StartProc, 0},
{OpCode::SizeArg, 0},
{OpCode::UpdateAstPosition, 0},
{OpCode::PushCopyGlobal, 0},
{OpCode::SizeArg, 1},
{OpCode::JumpIfZeroPop, 12},
{OpCode::UpdateAstPosition, 1},
{OpCode::PushCopyGlobal, 5},
{OpCode::SizeArg, 3},
{OpCode::Return, 3},
{OpCode::UpdateAstPosition, 3},
{OpCode::PushCopyGlobal, 8},
{OpCode::SizeArg, 3},
{OpCode::Return, 3},
{OpCode::UpdateAstPosition, 4},
{OpCode::MissingReturnError, 0},
{OpCode::StartProc, 1},
{OpCode::SizeArg, 3},
{OpCode::UpdateAstPosition, 0},
{OpCode::PushCopy, 0},
{OpCode::SizeArg, 3},
{OpCode::Return, 3},
{OpCode::UpdateAstPosition, 1},
{OpCode::MissingReturnError, 0},
{OpCode::StartProc, 16777215},
{OpCode::SizeArg, 0},
{OpCode::UpdateAstPosition, 0},
{OpCode::PushIndex, 1},
{OpCode::PopMoveGlobal, 0},
{OpCode::SizeArg, 1},
{OpCode::IncrementGlobalDataInitCounter, 0},
{OpCode::UpdateAstPosition, 1},
{OpCode::PushCopyGlobal, 1},
{OpCode::SizeArg, 2},
{OpCode::PopMoveGlobal, 3},
{OpCode::SizeArg, 2},
{OpCode::IncrementGlobalDataInitCounter, 0},
{OpCode::UpdateAstPosition, 4},
{OpCode::CallProc, 2},
{OpCode::CallProc, 18},
{OpCode::PopMoveGlobal, 11},
{OpCode::SizeArg, 3},
{OpCode::IncrementGlobalDataInitCounter, 0},
{OpCode::UpdateAstPosition, 5},
{OpCode::PushCopyGlobal, 0},
{OpCode::SizeArg, 1},
{OpCode::PushCopyGlobal, 3},
{OpCode::SizeArg, 2},
{OpCode::PushCopyGlobal, 11},
{OpCode::SizeArg, 3},
{OpCode::PopMoveGlobal, 14},
{OpCode::SizeArg, 6},
{OpCode::IncrementGlobalDataInitCounter, 0},
{OpCode::Return, 0}
})PRU");
    }

    std::vector<virtual_machine::data_stack_entry> expected_data;
    expected_data.resize(20);
    {
        expected_data[1].u = 111;
        expected_data[2].u = 123;
        expected_data[5].u = 222;
        expected_data[6].u = 3;
        expected_data[7].u = 444;
        expected_data[8].u = 222;
        expected_data[9].u = 5;
        expected_data[10].u = 444;
        CHECK(vm.data_stack == expected_data);
    }

    CHECK_EQ(vm.call_stack.size(), 0);
    CHECK(gen.push_global_init());
    CHECK_EQ(vm.call_stack.size(), 1);
    {
        using step = virtual_machine::step_result;
        CHECK_EQ(vm.step(nullptr), (step{step::Step, 1}));
        CHECK_EQ(vm.step(nullptr), (step{step::Step, 4}));
        CHECK_EQ(vm.step(nullptr), (step{step::Call, 0}));
        CHECK_EQ(vm.step(nullptr), (step{step::Step, 1}));
        CHECK_EQ(vm.step(nullptr), (step{step::Return, 0}));
        CHECK_EQ(vm.step(nullptr), (step{step::Call, 1}));
        CHECK_EQ(vm.step(nullptr), (step{step::Return, 0}));
        CHECK_EQ(vm.step(nullptr), (step{step::Step, 5}));
        CHECK_EQ(vm.step(nullptr), (step{step::Return, 0}));
        CHECK_EQ(vm.step(nullptr), (step{step::Stop, 0}));
        CHECK_EQ(vm.step(nullptr), (step{step::Stop, 0}));

        expected_data[0].u = 1;
        expected_data[3] = expected_data[1];
        expected_data[4] = expected_data[2];
        expected_data[11] = expected_data[5];
        expected_data[12] = expected_data[6];
        expected_data[13] = expected_data[7];
        expected_data[14] = expected_data[0];
        expected_data[15] = expected_data[1];
        expected_data[16] = expected_data[2];
        expected_data[17] = expected_data[11];
        expected_data[18] = expected_data[12];
        expected_data[19] = expected_data[13];
        CHECK(vm.data_stack == expected_data);
    }
}
