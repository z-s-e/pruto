#define KOUTEST_STD_STRING_HANDLING
#include <koutest.h>

#include <pruto/error_messages.h>

//#include <iostream>

using namespace pruto;

static void check_ast_msg(const char* script, const std::string& msg, const symop_table& operator_table = {})
{
    ast_result ast = section_ast(script, {}, operator_table);
    const auto s = error_message(ast, script);
    if( msg != s ) {
        //std::cout << strlit_encode_singleline(s.data()) << std::endl;
        CHECK_EQ(msg, s);
    }
}

static script_section_ast get_ast(const char* script, const host_identifier_table& host_entries = {})
{
    const auto ast = section_ast(script, host_entries, {});
    if( ast.error )
        FAIL(error_message(ast, script).data());
    return std::move(ast.section);
}

static void check_codegen_msg(const char* script, const std::string& msg)
{
    const auto ast = get_ast(script);
    virtual_machine vm;
    code_generator::host_integration default_host;
    code_generator gen;
    const auto s = error_message(gen.reset(&ast, &vm, &default_host), ast, script);
    if( msg != s ) {
        //std::cout << strlit_encode_singleline(s.data()) << std::endl;
        CHECK_EQ(msg, s);
    }
}

static void run_vm(virtual_machine& vm, virtual_machine::host_integration* host)
{
    using step = virtual_machine::step_result;
    while( true ) {
        auto s = vm.step(host);
        if( s.result == step::Stop )
            break;
        CHECK(int(s.result) < int(step::WaitingForHost));
    }
}


TEST_CASE(ast_error_message) {
    SUBTEST_ARGS(check_ast_msg, "a=[1", "Unexpected end of script\n  at line 1, column 5\n> a=[1 <<");
    SUBTEST_ARGS(check_ast_msg, "a={?", "Unexpected character '?'\n  at line 1, column 4\n> a={? <<");
    SUBTEST_ARGS(check_ast_msg, "a=if", "Unexpected keyword inside expression\n  at line 1, column 3\n> a=if <<");
    SUBTEST_ARGS(check_ast_msg, "a=:?", "Unexpected colon not belonging to an enum inside expression\n  at line 1, column 3\n> a=: <<");
    SUBTEST_ARGS(check_ast_msg, "a==", "Unexpected assignment symbol inside expression\n  at line 1, column 3\n> a== <<");
    SUBTEST_ARGS(check_ast_msg, "a=", "Expression is missing an operand\n  at line 1, column 3\n> a= <<");
    SUBTEST_ARGS(check_ast_msg, "a=1 2", "Expression is missing an operator between the current and previous operands"
                 "\n  at line 1, column 5"
                 "\n> a=1 2 <<");
    SUBTEST_ARGS(check_ast_msg, "a=(((((((((((", "Expression nesting limit exceeded\n  at line 1, column 13\n> a=((((((((((( <<");

    SUBTEST_ARGS(check_ast_msg, "in", "Unexpected keyword\n  at line 1, column 1\n> in <<");
    SUBTEST_ARGS(check_ast_msg, "return", "Unexpected keyword outside procedure definition\n  at line 1, column 1\n> return <<");
    SUBTEST_ARGS(check_ast_msg, "a (", "Unexpected call outside procedure definition\n  at line 1, column 1\n> a ( <<");
    SUBTEST_ARGS(check_ast_msg, "a  :=", "Unexpected reassignment outside procedure definition\n  at line 1, column 4\n> a  := <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nfor \n", "Unexpected line break\n  at line 2, column 5\n> for  <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nproc foo1   (",
                 "Procedure definition inside another procedure\n  at line 2, column 1\n> proc foo1   ( <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nelse if",
                 "'else' statement without corresponding conditional\n  at line 2, column 1\n> else if <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\n  break",
                 "'break' statement without corresponding loop\n  at line 2, column 3\n>   break <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\n  case",
                 "'case' statement without corresponding 'switch'\n  at line 2, column 3\n>   case <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nswitch 1\nx", "Missing 'case' statement after 'switch'\n  at line 3, column 1\n> x <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nfor b xxx", "Missing 'in' in 'for' statement\n  at line 2, column 7\n> for b xxx <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nif(", "Missing space after keyword\n  at line 2, column 3\n> if( <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\n$ foo", "Section separator inside procedure definition\n  at line 2, column 1\n> $ foo <<");
    SUBTEST_ARGS(check_ast_msg, " $ bar; baz", "Space before section separator character\n  at line 1, column 1\n>  $ bar; baz <<");

    SUBTEST_ARGS(check_ast_msg, "x=foo; bar", "Unknown name 'foo'\n  at line 1, column 3\n> x=foo <<");
    SUBTEST_ARGS(check_ast_msg, "aa=1\n aa=2", "Redefinition of previously defined name 'aa'\n  at line 2, column 2\n>  aa= <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\n x=1\n x:=2",
                 "Left side of reassignment is not a variable\n  at line 3, column 2\n>  x:= <<");
    SUBTEST_ARGS(check_ast_msg, "x=!1", "Undefined operator '!'\n  at line 1, column 3\n> x=! <<");

    SUBTEST_ARGS(check_ast_msg, "x=1+", "Left unary operator '+' after operand\n  at line 1, column 4\n> x=1+ <<",
                 {{SymOp::Plus, left_unary_operator(0)}});
    SUBTEST_ARGS(check_ast_msg, "x=+1", "Right unary operator '+' before operand\n  at line 1, column 3\n> x=+ <<",
                 {{SymOp::Plus, right_unary_operator(0)}});
    SUBTEST_ARGS(check_ast_msg, "x=1+1",
                 "Expression is missing a binary operator between the current and previous operands\n  at line 1, column 5\n> x=1+1 <<",
                 {{SymOp::Plus, right_unary_operator(0)}});
    SUBTEST_ARGS(check_ast_msg, "x=+1", "Binary operator '+' is missing a left operand\n  at line 1, column 3\n> x=+ <<",
                 {{SymOp::Plus, binary_operator(42, 1)}});
    SUBTEST_ARGS(check_ast_msg, "x=(1|~;\n)", "Binary operator '|~' is missing a right operand\n  at line 1, column 5\n> x=(1|~ <<",
                 {{SymOp::BarTilde, binary_operator(42, 1)}});

    SUBTEST_ARGS(check_ast_msg, "x=1\n y=x()", "The name 'x' is not a procedure\n  at line 2, column 4\n>  y=x( <<");
    SUBTEST_ARGS(check_ast_msg, "proc p()\n end\n x=p()",
                 "Procedure without a return value 'p' called inside expression\n  at line 3, column 4\n>  x=p( <<");
    SUBTEST_ARGS(check_ast_msg, "proc p()\nreturn 1\nend\nproc q()\np()",
                 "Procedure call statement with procedure 'p' that has a return value\n  at line 5, column 1\n> p( <<");

    SUBTEST_ARGS(check_ast_msg, "proc p(a1, a2, a3, a4, a5, a6, a7, a8,\n a9, a10, a11, a12, a13, a14, a15, a16",
                 "Too many procedure parameter\n  at line 2, column 36\n>  a9, a10, a11, a12, a13, a14, a15, a16 <<");
    SUBTEST_ARGS(check_ast_msg, "proc p()\n return 1\n end\n x=p(2)",
                 "Procedure 'p' called with wrong argument count (needs 0, got 1)\n  at line 4, column 4\n>  x=p( <<");

    SUBTEST_ARGS(check_ast_msg, "proc p()\n if 1\n return\n else\n return 1; baz\n",
                 "Procedure has both return statements with and without value\n  at line 5, column 2\n>  return 1; baz <<");
    SUBTEST_ARGS(check_ast_msg, "x = {a: 1, a: 2}\n", "Repeated struct member name 'a'\n  at line 1, column 12\n> x = {a: 1, a <<");
    {
        static const char repeated_case[] = R"PRU(
proc p()
    switch 1
    case :a
    case :b
        switch 2
            case :b
        end
    case [:c, :d
         , :a]
    end
end
)PRU";
        SUBTEST_ARGS(check_ast_msg, repeated_case, "Repeated case label 'a'\n  at line 10, column 13\n>          , :a <<");
    }
    SUBTEST_ARGS(check_ast_msg, "proc p()\n if 1\n if 2\n if 3\n if 4\n if 5\n if 6\n if 7\n if 8\n if 9\n if 10\n",
                 "Block nesting limit exceeded\n  at line 11, column 2\n>  if 10 <<");

    SUBTEST_ARGS(check_ast_msg, "proc a()\nx=(1\nif x",
                 "Unexpected keyword inside expression (starting at line 2)\n  at line 3, column 1\n> if <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nx=(1\ny=1",
                 "Expression is missing an operator between the current and previous operands\n  at line 3, column 1\n> y <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nx=(\ny=1",
                 "Unknown name 'y' (referenced in expression starting at line 2)\n  at line 3, column 1\n> y <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nvar x=1\ny=(\nx:=2",
                 "Unexpected colon not belonging to an enum inside expression (starting at line 3)\n  at line 4, column 2\n> x: <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nvar x=1\ny=(\nx=2",
                 "Unexpected assignment symbol inside expression (starting at line 3)\n  at line 4, column 2\n> x= <<");
    SUBTEST_ARGS(check_ast_msg, "proc a()\nx=(1+\ny=1",
                 "Expression is missing a binary operator between the current and previous operands\n  at line 3, column 1\n> y <<",
                 {{SymOp::Plus, right_unary_operator(0)}});
    SUBTEST_ARGS(check_ast_msg, "proc a()\nend\nproc b()\nx=(\na()",
                 "Procedure without a return value 'a' called inside expression (starting at line 4)\n  at line 5, column 1\n> a( <<");
}

struct size_limit_test_host_integration : public code_generator::host_integration
{
    instance iterator_instance(const type_info&) override
    {
        type_struct s;
        s.members.push_back({"a", type_host{0}});
        s.members.push_back({"b", type_boolean{}});
        return {0, s};
    }

    type_info num_type() override { return type_host{0}; }
    bool create_num(std::string_view, code_generator::data_stack_entry* ) override { return true; }

    code_generator::stack_entry_count type_size(host_type_id) override { return code_generator::MaximumTypeSize; }
};

TEST_CASE(codegen_error_message) {
    SUBTEST_ARGS(check_codegen_msg, "x = {a, b, c}\ny = x.d",
                 "Invalid data member\n  members: a, b, c\n  at line 2, column 7\n> y = x.d <<");

    SUBTEST_ARGS(check_codegen_msg, R"PRU(
proc p()
  var x = {a, b: {d, e}, c}
  x.b.foo := true
  return x
end
y = p()
)PRU", "Invalid data member\n  members: d, e\n  at line 4, column 7; proc p <>\n>   x.b.foo <<"
       "\n    called from line 7, column 5");

    SUBTEST_ARGS(check_codegen_msg, R"PRU(
proc p(y)
  var x = {a, b, c}
  switch :x:b
  case :a
  case [:c,
        :d]
  end
  return x
end
y = p(true)
)PRU", "Invalid data member\n  members: a, b, c\n  at line 7, column 10; proc p <boolean>"
       "\n>         :d <<\n    called from line 11, column 5");

    SUBTEST_ARGS(check_codegen_msg, "x = {a, b, c}\ny = x.b.z",
                 "Invalid data member\n  members: - (empty struct)\n  at line 2, column 9\n> y = x.b.z <<");

    SUBTEST_ARGS(check_codegen_msg, "x = {a: true}\ny = x.a.z",
                 "Name is not a struct\n  type: boolean\n  at line 2, column 7\n> y = x.a. <<");

    SUBTEST_ARGS(check_codegen_msg, R"PRU(
proc p()
  x = {a: {b: false}}
  y = :x.a.b:c
  return x
end
y = p()
)PRU", "Name is not a struct\n  type: boolean\n  at line 4, column 12; proc p <>"
       "\n>   y = :x.a.b: <<\n    called from line 7, column 5");

    SUBTEST_ARGS(check_codegen_msg, "x = true\ny = x.a.z",
                 "Name is not a struct\n  type: boolean\n  at line 2, column 5\n> y = x. <<");

    SUBTEST_ARGS(check_codegen_msg, "x = true\ny = x.a.z",
                 "Name is not a struct\n  type: boolean\n  at line 2, column 5\n> y = x. <<");

    SUBTEST_ARGS(check_codegen_msg, "x = [true, false, {}, true]",
                 "List element 3 has a different type than previous ones"
                 "\n  previous type: boolean"
                 "\n           type: struct()\n  at line 1, column 5\n> x = [ <<");

    SUBTEST_ARGS(check_codegen_msg, R"PRU(
proc p(x)
  if x
      return true
  end
  return false
end
y = p({a})
)PRU", "Conditional expression does not have a boolean type\n  type: struct(a: struct())"
       "\n  at line 3, column 3; proc p <struct(a: struct())>\n>   if x <<\n    called from line 8, column 5");

    SUBTEST_ARGS(check_codegen_msg, R"PRU(
proc p()
  x = {a, b, c}
  switch :x:b
  case :b
  end
  return false
end
y = p()
)PRU", "Switch block has unhandled cases (or missing an 'else' path)\n  missing cases: a, c"
       "\n  at line 6, column 3; proc p <>\n>   end <<\n    called from line 9, column 5");

    SUBTEST_ARGS(check_codegen_msg, R"PRU(
proc p()
  var x = true
  x := {z}
  return false
end
y = p()
)PRU", "Var is reassigned a value of different type\n         var type: boolean\n  reassigned type: struct(z: struct())"
       "\n  at line 4, column 5; proc p <>\n>   x := <<\n    called from line 7, column 5");

    SUBTEST_ARGS(check_codegen_msg, R"PRU(
proc p()
  if true
    return false
  end
  return {}
end
proc q(a, b, c)
  return p()
end
x = {a, b}
y = q(p, q, :x:b)
)PRU", "Return expression is of different type than previous ones\n  previous  type: boolean"
       "\n            type: struct()\n  at line 6, column 3; proc p <>"
       "\n>   return {} <<\n    called from line 9, column 10; proc q <global_proc_ret(0, 0), global_proc_ret(1, 3), enum(a, b)>"
       "\n    called from line 12, column 5");

    SUBTEST_ARGS(check_codegen_msg, "x = true[]",
                 "Host rejected list operator expression\n  at line 1, column 9\n> x = true[ <<");

    SUBTEST_ARGS(check_codegen_msg, R"PRU(
proc p()
  for x in (
            true)
  end
  return false
end
y = p()
)PRU", "Host rejected iterator expression\n  at line 3, column 3; proc p <>\n>   for x in ( <<"
       "\n    called from line 8, column 5");

    {
        const char txt[] = "x = 1 &< 2";
        const auto ast = section_ast(txt, {}, {{SymOp::AndLess, binary_operator(42, 1)}});
        if( ast.error )
            FAIL(error_message(ast, txt).data());
        virtual_machine vm;
        size_limit_test_host_integration host;
        code_generator gen;
        const auto s = error_message(gen.reset(&ast.section, &vm, &host), ast.section, txt);
        CHECK_EQ("Host rejected operator call instance '&<'\n  at line 1, column 7\n> x = 1 &< <<", s);
    }

    SUBTEST_ARGS(check_codegen_msg, "x = \"foo\\\"bar\"",
                 "Host does not accept any string literals\n  at line 1, column 5\n> x = \"foo\\\"bar\" <<");
    SUBTEST_ARGS(check_codegen_msg, "x = \"\n> foo\n\"",
                 "Host does not accept any string literals\n  at line 1, column 5\n> x = \" <<");
    SUBTEST_ARGS(check_codegen_msg, "x = 123.45",
                 "Host does not accept any numeric literals\n  at line 1, column 5\n> x = 123.45 <<");

    {
        const char txt[] = "x = \"baz\"";
        const auto ast = get_ast(txt);
        virtual_machine vm;
        code_generator::host_integration default_host;
        code_generator gen;
        const auto s = error_message(gen.reset(&ast, &vm, &default_host), ast, txt, "foobar");
        CHECK_EQ("foobar\n  at line 1, column 5\n> x = \"baz\" <<", s);
    }

    {
        const char txt[] = "x = {a: 123, b: true}";
        const auto ast = get_ast(txt);
        virtual_machine vm;
        size_limit_test_host_integration host;
        code_generator gen;
        const auto s = error_message(gen.reset(&ast, &vm, &host), ast, txt);
        CHECK_EQ("Type exceeds the memory size limit\n  type: struct(a: host(0), b: boolean)"
                 "\n  at line 1, column 5\n> x = { <<", s);
    }

    {
        const char txt[] = R"PRU(
proc p()
  for x in true
  end
  return false
end
y = p()
)PRU";
        const auto ast = get_ast(txt);
        virtual_machine vm;
        size_limit_test_host_integration host;
        code_generator gen;
        const auto s = error_message(gen.reset(&ast, &vm, &host), ast, txt);
        CHECK_EQ("Type exceeds the memory size limit\n  type: struct(a: host(0), b: boolean)"
                 "\n  at line 3, column 7; proc p <>\n>   for x <<\n    called from line 7, column 5", s);
    }
}

class backtrace_test_host_integration : public code_generator::host_integration, public virtual_machine::host_integration {
public:

    static pruto::host_identifier_table host_identifier()
    {
        return { {"host_proc", ident::host_proc(0, 1)} };
    }

    instance proc_instance(ident id, const type_info* params) override
    {
        assert(params[0] == type_info{type_boolean()});
        assert(id.index() == 0);
        return {0, {}};
    }

    bool call_proc(virtual_machine::host_instance_index index, const virtual_machine::data_stack_entry* args,
                   virtual_machine::data_stack_entry*) override
    {
        assert(index == 0);
        if( args->u ) {
            bt = current_backtrace_string(*gen);
        } else {
            gen->push_call(1, nullptr, nullptr);

            using step = virtual_machine::step_result;
            while( true ) {
                auto s = gen->current_vm()->step(this);
                CHECK(s.result != step::Stop);
                if( s.result == step::WaitingForHost )
                    break;
                CHECK(int(s.result) < int(step::WaitingForHost));
            }
        }
        return true;
    }

    code_generator* gen = nullptr;
    std::string bt;
};

TEST_CASE(backtrace) {
    {
        const auto ast = get_ast(R"PRU(proc q()
        host_proc(true)
end
proc p(x)
  y = x.foo
  host_proc(false)
  return y
end
z = p({foo: false, bar})
)PRU", backtrace_test_host_integration::host_identifier());
        virtual_machine vm;
        backtrace_test_host_integration host;
        code_generator gen;
        host.gen = &gen;

        CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &host)));
        CHECK_EQ(gen.prepare_call(ast_find_global(ast, "q"), nullptr).instance, 1);
        CHECK(gen.push_global_init());

        SUBTEST_ARGS(run_vm, vm, &host);
        CHECK_EQ(host.bt, R"PRU(  at host call
    called from line 2, column 9; proc q <>
    called from inside host call
    called from line 6, column 3; proc p <struct(foo: boolean, bar: struct())>
    called from line 9, column 5)PRU");
    }

    {
        const auto ast = get_ast(R"PRU(
proc foobar(x)
  if x
    return true
  end
end
proc p(x, y, z)
  return {}
end
proc q()
  x = foobar(true)
  return foobar(false)
end
baz = p(false, p({}, q(), {}), true)
)PRU");
        virtual_machine vm;
        code_generator::host_integration default_host;
        code_generator gen;

        CHECK(std::holds_alternative<std::monostate>(gen.reset(&ast, &vm, &default_host)));
        CHECK(gen.push_global_init());

        using step = virtual_machine::step_result;
        while( true ) {
            auto s = vm.step(nullptr);
            CHECK(s.result != step::Stop);
            if( s.result == step::ErrorMissingReturn )
                break;
            CHECK(int(s.result) < int(step::WaitingForHost));
        }
        CHECK_EQ(current_backtrace_string(gen), R"PRU(  at statement line 6; proc foobar <boolean>
    called from line 12, column 10; proc q <>
    called from line 14, column 22)PRU");
    }
}
