/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pruto/virtual_machine.h"

namespace pruto {

static_assert(sizeof(virtual_machine::data_stack_entry) == 4, "data_stack_entry may not be padded");

static void push_copy(std::vector<virtual_machine::data_stack_entry>& v, size_t index, size_t count)
{
    if( count == 0 )
        return;
    const size_t old_size = v.size();
    v.resize(old_size + count);
    std::memcpy(v.data() + old_size, v.data() + index, count * sizeof(virtual_machine::data_stack_entry));
}

static void pop_move(std::vector<virtual_machine::data_stack_entry>& v, size_t index, size_t count)
{
    if( count == 0 )
        return;
    const size_t old_size = v.size();
    std::memcpy(v.data() + index, v.data() + old_size - count, count * sizeof(virtual_machine::data_stack_entry));
    v.resize(old_size - count);
}

virtual_machine::step_result virtual_machine::step(host_integration* host)
{
    while( true ) {
        const auto current_ip = ip;
        const auto old_size = data_stack.size();
        const auto inst = byte_code[current_ip];

        switch( inst.code ) {
        case instruction::OpCode::Invalid:
            if( current_ip == StopIP ) {
                assert(call_stack.empty());
                return {step_result::Stop, 0};
            }
            if( current_ip == HostCallIP )
                return {step_result::WaitingForHost, 0};
            assert(false); break;
        case instruction::OpCode::StartProc:
            assert(false); break;
        case instruction::OpCode::CallProc:
            assert(byte_code[inst.val].code == instruction::OpCode::StartProc);
            assert(byte_code[inst.val + 1].code == instruction::OpCode::SizeArg);
            assert(byte_code[inst.val + 2].code == instruction::OpCode::UpdateAstPosition);

            call_stack.push_back(call_stack_entry_call_data{ current_ip, frame_offset, nullptr });
            frame_offset = old_size - byte_code[inst.val + 1].val;
            ip = inst.val + 3;
            return {step_result::Call, byte_code[inst.val].val};
        case instruction::OpCode::CallHost:
            {
                const auto& call_info = host_instances[inst.val];
                data_stack.resize(old_size + call_info.retval_size);
                auto* retval = data_stack.data() + old_size;

                call_stack.push_back(call_stack_entry_call_data{ current_ip, frame_offset, nullptr });
                ip = HostCallIP;
                bool ok = host->call_proc(call_info.index, retval - call_info.params_size, retval);
                assert(ip == HostCallIP);
                ip = current_ip;
                call_stack.pop_back();

                if( !ok ) {
                    data_stack.resize(old_size);
                    return {step_result::ErrorHost, 0};
                }

                if( call_info.retval_size > 0 )
                    std::memmove(retval - call_info.params_size, retval, call_info.retval_size * sizeof(data_stack_entry));
                data_stack.resize(old_size - call_info.params_size + call_info.retval_size);
            }
            break;
        case instruction::OpCode::PushIndex:
            data_stack.push_back({uint32_t(inst.val)});
            break;
        case instruction::OpCode::Pop:
            data_stack.resize(old_size - inst.val);
            break;
        case instruction::OpCode::Jump:
            ip = inst.val;
            continue;
        case instruction::OpCode::JumpIfZeroPop:
            {
                const auto tmp = data_stack.back().u;
                data_stack.pop_back();
                if( tmp == 0 ) {
                    ip = inst.val;
                    continue;
                }
            }
            break;
        case instruction::OpCode::Return:
            {
                const auto& call = std::get<call_stack_entry_call_data>(call_stack.back());
                const auto count = inst.val;

                if( count > 0 ) {
                    auto* retval = data_stack.data() + (old_size - count);
                    if( call.retval_storage != nullptr ) {
                        std::memcpy(call.retval_storage, retval, count * sizeof(data_stack_entry));
                        data_stack.resize(frame_offset);
                    } else {
                        std::memmove(data_stack.data() + frame_offset, retval, count * sizeof(data_stack_entry));
                        data_stack.resize(frame_offset + count);
                    }
                } else {
                    data_stack.resize(frame_offset);
                }

                frame_offset = call.frame_offset;
                ip = call.return_ip;
                if( ip > HostCallIP )
                    ++ip;
                call_stack.pop_back();
            }
            return {step_result::Return, 0};
        case instruction::OpCode::PushCopy:
            assert(byte_code[current_ip + 1].code == instruction::OpCode::SizeArg);
            push_copy(data_stack, frame_offset + inst.val, byte_code[current_ip + 1].val);
            ++ip;
            break;
        case instruction::OpCode::PushCopyGlobal:
            assert(byte_code[current_ip + 1].code == instruction::OpCode::SizeArg);
            push_copy(data_stack, inst.val, byte_code[current_ip + 1].val);
            ++ip;
            break;
        case instruction::OpCode::PushCopyHost:
            assert(byte_code[current_ip + 1].code == instruction::OpCode::SizeArg);
            {
                const auto& data_info = host_data[inst.val];
                const auto count = byte_code[current_ip + 1].val;
                data_stack.resize(old_size + count);

                ip = HostCallIP;
                bool ok = host->load_host_data(data_info.id, data_info.offset, count, data_stack.data() + old_size);
                assert(ip == HostCallIP);
                ip = current_ip;

                if( !ok ) {
                    data_stack.resize(old_size);
                    return {step_result::ErrorHost, 0};
                }
            }
            ++ip;
            break;
        case instruction::OpCode::PopMove:
            assert(byte_code[current_ip + 1].code == instruction::OpCode::SizeArg);
            pop_move(data_stack, frame_offset + inst.val, byte_code[current_ip + 1].val);
            ++ip;
            break;
        case instruction::OpCode::PopMoveGlobal:
            assert(byte_code[current_ip + 1].code == instruction::OpCode::SizeArg);
            pop_move(data_stack, inst.val, byte_code[current_ip + 1].val);
            ++ip;
            break;
        case instruction::OpCode::PopMoveHost:
            assert(byte_code[current_ip + 1].code == instruction::OpCode::SizeArg);
            {
                const auto& data_info = host_data[inst.val];
                const auto count = byte_code[current_ip + 1].val;

                ip = HostCallIP;
                bool ok = host->store_host_data(data_info.id, data_stack.data() + (old_size - count), data_info.offset, count);
                assert(ip == HostCallIP);
                ip = current_ip;

                if( !ok )
                    return {step_result::ErrorHost, 0};

                data_stack.resize(old_size - count);
            }
            ++ip;
            break;
        case instruction::OpCode::CallList:
            assert(byte_code[current_ip + 1].code == instruction::OpCode::SizeArg);
            {
                const auto& call_info = host_instances[inst.val];
                data_stack.resize(old_size + call_info.retval_size);
                auto* retval = data_stack.data() + old_size;

                ip = HostCallIP;
                bool ok = host->call_list(call_info.index, retval - call_info.params_size, byte_code[current_ip + 1].val, retval);
                assert(ip == HostCallIP);
                ip = current_ip;

                if( !ok ) {
                    data_stack.resize(old_size);
                    return {step_result::ErrorHost, 0};
                }

                if( call_info.retval_size > 0 )
                    std::memmove(retval - call_info.params_size, retval, call_info.retval_size * sizeof(data_stack_entry));
                data_stack.resize(old_size - call_info.params_size + call_info.retval_size);
            }
            ++ip;
            break;
        case instruction::OpCode::SizeArg:
            assert(false); break;
        case instruction::OpCode::PushForLoop:
            {
                const auto& call_info = host_instances[inst.val];

                ip = HostCallIP;
                auto it = host->create_iterator(call_info.index, data_stack.data() + (old_size - call_info.params_size));
                assert(ip == HostCallIP);
                ip = current_ip;

                if( it == nullptr )
                    return {step_result::ErrorHost, 0};

                data_stack.resize(old_size - call_info.params_size);
                call_stack.push_back(call_stack_entry_for_loop_data{ call_info.retval_size, it });
            }
            break;
        case instruction::OpCode::AdvanceForLoopOrJump:
            {
                const auto& loop = std::get<call_stack_entry_for_loop_data>(call_stack.back());
                data_stack.resize(old_size + loop.element_size);

                ip = HostCallIP;
                if( !loop.iterator->advance(data_stack.data() + old_size) ) {
                    data_stack.resize(old_size);
                    ip = inst.val;
                    continue;
                }
                assert(ip == HostCallIP);
                ip = current_ip;
            }
            break;
        case instruction::OpCode::PopForLoop:
            assert(inst.val > 0);
            ip = HostCallIP;
            for( size_t i = 0; i < inst.val; ++i ) {
                std::get<call_stack_entry_for_loop_data>(call_stack.back()).iterator->release();
                call_stack.pop_back();
            }
            assert(ip == HostCallIP);
            ip = current_ip;
            break;
        case instruction::OpCode::JumpTable:
            {
                const auto tmp = data_stack.back().u;
                data_stack.pop_back();
                ip = byte_code[current_ip + 1 + tmp].val;
            }
            continue;
        case instruction::OpCode::JumpTableEntry:
            assert(false); break;
        case instruction::OpCode::IncrementGlobalDataInitCounter:
            assert(inst.val == 0);
            ++next_global_data_init;
            break;
        case instruction::OpCode::UpdateAstPosition:
            ++ip;
            return {step_result::Step, inst.val};
        case instruction::OpCode::MissingReturnError:
            assert(inst.val == 0);
            return {step_result::ErrorMissingReturn, 0};
        }
        ++ip;
    }
}

void virtual_machine::push_call(instruction_offset loc, const data_stack_entry* args, data_stack_entry* retval_storage)
{
    assert(byte_code[loc].code == instruction::OpCode::StartProc);
    assert(byte_code[loc + 1].code == instruction::OpCode::SizeArg);
    assert(byte_code[loc + 2].code == instruction::OpCode::UpdateAstPosition);

    const size_t old_size = data_stack.size();
    const size_t args_size = byte_code[loc + 1].val;
    data_stack.insert(data_stack.end(), args, args + args_size);

    call_stack.push_back(call_stack_entry_call_data{ ip, frame_offset, retval_storage });
    frame_offset = old_size;
    ip = loc + 3;
}

bool operator ==(const virtual_machine::host_instance_info& l, const virtual_machine::host_instance_info& r)
{
    return l.index == r.index && l.params_size == r.params_size && l.retval_size == r.retval_size;
}

bool operator ==(const virtual_machine::host_data_info& l, const virtual_machine::host_data_info& r)
{
    return l.id == r.id && l.offset == r.offset;
}

bool virtual_machine::host_integration::call_proc(host_instance_index, const data_stack_entry*, data_stack_entry*)
{
    return false;
}
bool virtual_machine::host_integration::call_list(host_instance_index, const data_stack_entry*, size_t, data_stack_entry*)
{
    return false;
}
virtual_machine::host_integration::for_iterator* virtual_machine::host_integration::create_iterator(host_instance_index, const data_stack_entry*)
{
    return nullptr;
}
bool virtual_machine::host_integration::store_host_data(ident, const data_stack_entry*, stack_entry_count, stack_entry_count)
{
    return false;
}
bool virtual_machine::host_integration::load_host_data(ident, stack_entry_count, stack_entry_count, data_stack_entry*)
{
    return false;
}

} // namespace
