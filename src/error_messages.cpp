/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pruto/error_messages.h"

namespace pruto {

static const char InternalError[] = "Internal error (bug)";
static constexpr size_t BugIndicatingValue = 9999999;

static type_stringifier default_stringifier;

static std::string line_number(uint32_t line_offset) { return std::to_string(line_offset + 1); }
static std::string line_column_info(uint32_t line_offset, uint32_t pos)
{
    return "line " + line_number(line_offset) + ", column " + std::to_string(pos + 1);
}
static std::string at_line_column_info_line(uint32_t line_offset, uint32_t pos)
{
    return "\n  at " + line_column_info(line_offset, pos);
}

static std::string quoted_line(const char* line_start, size_t count)
{
    return "\n> " + std::string(line_start, count) + " <<";
}

static uint32_t find_line_offset(const char* text, uint32_t line) { return uint32_t(find_line(text, line) - text); }

static std::string enum_member_list(const type_info& type)
{
    type_enum e;
    if( std::holds_alternative<type_struct>(type) )
        e = get_enum_and_index(std::get<type_struct>(type), {}).e;
    else
        e = std::get<type_enum>(type);
    if( e.members.empty() )
        return "- (empty struct)";
    return join_strings(e.members);
}

static std::string proc_instance_suffix(const code_generator::instance_location& loc,
                                        const script_section_ast& ast,
                                        type_stringifier& type_string)
{
    if( !loc.proc_id )
        return {};
    return "; proc " + ast_get_global_proc(ast, loc.proc_id).name
            + " <" + type_string.list_to_string(loc.param_types.data(), loc.param_types.size()) + ">";
}

static std::string called_from_string_line(const code_generator::instance_location& loc,
                                           const script_section_ast& ast,
                                           type_stringifier& type_string)
{
    const auto exp = exp_element_script_location(ast_find_expression_element(ast, loc));
    const auto s = "\n    called from " + line_column_info(exp.line, exp.pos);
    return s + proc_instance_suffix(loc, ast, type_string);
}


std::string error_message(const ast_result& r, const char* script)
{
    struct ast_error_message_visitor {
        std::string unexpected_char()
        {
            const char c = script[loc.bytes_before_line + error.pos];
            if( c == 0 ) {
                --output_line_size;
                return "Unexpected end of script";
            } else if( c == '\n' ) {
                --output_line_size;
                return "Unexpected line break";
            } else if( c == '\t' ) {
                --output_line_size;
                return "Unexpected tab character";
            } else if( (c > 0 && c < 32) || c == 127 ) {
                --output_line_size;
                return "Unexpected ASCII control character";
            } else if( c < 0 || c > 127 ) {
                --output_line_size;
                return "Unexpected non-ASCII character";
            }
            std::string s("Unexpected character '");
            s.push_back(c);
            return s + "'";
        }

        std::string error_string() { return std::string(script + loc.bytes_before_line + error.pos, error.size); }

        void set_output_to_call()
        {
            assert(error.call.id);
            loc.line = error.call.line;
            loc.bytes_before_line = find_line_offset(script, loc.line);
            loc.pos = error.call.pos;
            output_line_size = loc.pos;
        }

        void expand_output_to_next(char c)
        {
            while( script[loc.bytes_before_line + output_line_size - 1] != c )
                ++output_line_size;
        }

        void expand_output_to_full_line()
        {
            while( true ) {
                const char next = script[loc.bytes_before_line + output_line_size];
                if( next == '\n' || next == 0 )
                    break;
                ++output_line_size;
            }
        }

        std::string maybe_expression_start_hint(std::string beginning = {})
        {
            if( !error.exp_nesting.empty() ) {
                const auto exp_start = error.exp_nesting.front().line;
                if( exp_start < loc.line ) {
                    if( beginning.empty() )
                        beginning = " (starting at line ";
                    return beginning + line_number(exp_start) + ")";
                }
            }
            return {};
        }

        std::string call_proc_name()
        {
            const char* name = script + loc.bytes_before_line + loc.pos;
            size_t i = 1;
            while( is_word_continuation(name[i]) )
                ++i;
            return std::string(name, i);
        }

        std::string operator()(ExpressionError val)
        {
            switch( val ) {
            case ExpressionError::UnexpectedCharacter:
                return unexpected_char();
            case ExpressionError::UnexpectedKeyword:
                return "Unexpected keyword inside expression" + maybe_expression_start_hint();
            case ExpressionError::UnexpectedNonEnumColon:
                return "Unexpected colon not belonging to an enum inside expression" + maybe_expression_start_hint();
            case ExpressionError::UnexpectedAssignSymbol:
                return "Unexpected assignment symbol inside expression" + maybe_expression_start_hint();
            case ExpressionError::MissingOperand:
                return "Expression is missing an operand";
            case ExpressionError::MissingOperatorBetweenOperands:
                return "Expression is missing an operator between the current and previous operands";
            case ExpressionError::NestingLimitExceeded:
                return "Expression nesting limit exceeded";
            }
            assert(false);
            return {};
        }
        std::string operator()(StatementError val)
        {
            switch( val ) {
            case StatementError::UnexpectedCharacter:
                return unexpected_char();
            case StatementError::UnexpectedKeyword:
                return "Unexpected keyword";
            case StatementError::KeywordOutsideProc:
                return "Unexpected keyword outside procedure definition";
            case StatementError::CallOutsideProc:
                expand_output_to_next('(');
                return "Unexpected call outside procedure definition";
            case StatementError::ReassignOutsideProc:
                return "Unexpected reassignment outside procedure definition";
            case StatementError::NestedProcDefinition:
                expand_output_to_next('(');
                return "Procedure definition inside another procedure";
            case StatementError::ElseOutsideConditional:
                expand_output_to_full_line();
                return "'else' statement without corresponding conditional";
            case StatementError::UnexpectedWordAfterElse:
                return "Unexpected word after 'else'";
            case StatementError::BreakOutsideLoop:
                expand_output_to_full_line();
                return "'break' statement without corresponding loop";
            case StatementError::ContinueOutsideLoop:
                expand_output_to_full_line();
                return "'continue' statement without corresponding loop";
            case StatementError::CaseOutsideSwitch:
                expand_output_to_full_line();
                return "'case' statement without corresponding 'switch'";
            case StatementError::NeedCaseAfterSwitch:
                return "Missing 'case' statement after 'switch'";
            case StatementError::NeedInAfterForIdent:
                return "Missing 'in' in 'for' statement";
            case StatementError::MissingSpaceAfterIf:
            case StatementError::MissingSpaceAfterSwitch:
            case StatementError::MissingSpaceAfterCase:
            case StatementError::MissingSpaceAfterWhile:
            case StatementError::MissingSpaceAfterForIn:
            case StatementError::MissingSpaceAfterReturn:
                return "Missing space after keyword";
            case StatementError::SectionInsideProc:
                expand_output_to_full_line();
                return "Section separator inside procedure definition";
            case StatementError::WhitespaceBeforeSection:
                expand_output_to_full_line();
                return "Space before section separator character";
            }
            assert(false);
            return {};
        }
        std::string operator()(AstError val)
        {
            switch( val ) {
            case AstError::UnknownName:
                return "Unknown name '" + error_string() + "'"
                        + maybe_expression_start_hint(" (referenced in expression starting at line ");
            case AstError::RecursiveRef:
                return "Procedure '" + error_string() + "' is referenced within itself";
            case AstError::InvalidRedefine:
                expand_output_to_next('=');
                return "Redefinition of previously defined name '" + error_string() + "'";
            case AstError::ReassignNotVar:
                expand_output_to_next('=');
                return "Left side of reassignment is not a variable";
            case AstError::UndefinedOperator:
                return "Undefined operator '" + error_string() + "'";
            case AstError::LeftUnaryAfterOperand:
                return "Left unary operator '" + error_string() + "' after operand";
            case AstError::RightUnaryBeforeOperand:
                return "Right unary operator '" + error_string() + "' before operand";
            case AstError::MissingBinaryOperatorBetweenOperands:
                return "Expression is missing a binary operator between the current and previous operands";
            case AstError::BinaryOperatorMissingLeftOperand:
                return "Binary operator '" + error_string() + "' is missing a left operand";
            case AstError::BinaryOperatorMissingRightOperand:
                {
                    set_output_to_call();
                    const char* op = script + loc.bytes_before_line + loc.pos;
                    const size_t op_size = sym_op_size(op);
                    output_line_size += op_size;
                    return "Binary operator '" + std::string(op, op_size) + "' is missing a right operand";
                }
            case AstError::NameIsNotProc:
                expand_output_to_next('(');
                return "The name '" + error_string() + "' is not a procedure";
            case AstError::ProcInEnumRef:
                return "Procedure '" + error_string() + "' is referenced inside an enum expression";
            case AstError::ProcHasNoValue:
                set_output_to_call();
                expand_output_to_next('(');
                return "Procedure without a return value '" + call_proc_name() + "' called inside expression"
                        + maybe_expression_start_hint();
            case AstError::ProcHasValue:
                set_output_to_call();
                expand_output_to_next('(');
                return "Procedure call statement with procedure '" + call_proc_name() + "' that has a return value";
            case AstError::ProcTooManyParameter:
                return "Too many procedure parameter";
            case AstError::ProcArgParameterCountMismatch:
                set_output_to_call();
                expand_output_to_next('(');
                return "Procedure '" + call_proc_name() + "' called with wrong argument count (needs "
                        + std::to_string(error.call.id.proc_parameter_count()) + ", got "
                        + std::to_string(error.call_args) + ")";
            case AstError::ProcReturnValueMismatch:
                expand_output_to_full_line();
                return "Procedure has both return statements with and without value";
            case AstError::RepeatedStructMemberName:
                return "Repeated struct member name '" + error_string() + "'";
            case AstError::RepeatedCaseLabel:
                return "Repeated case label '" + error_string() + "'";
            case AstError::BlockNestingLimitExceeded:
                expand_output_to_full_line();
                return "Block nesting limit exceeded";
            }
            assert(false);
            return {};
        }

        const char* script;
        const ast_result_error& error;
        error_script_location loc;
        size_t output_line_size;
    };

    if( !r.error )
        return InternalError;
    ast_error_message_visitor v{script, *r.error, {r.error->line, r.error->line_byte_offset, r.error->pos},
                                r.error->pos + r.error->size};
    auto s = std::visit(v, r.error->val);
    s += at_line_column_info_line(v.loc.line, v.loc.pos);
    s += quoted_line(script + v.loc.bytes_before_line, v.output_line_size);
    return s;
}

static std::string error_message(const code_generator::prepare_result_error& error, const script_section_ast& ast,
                                 const char* script, const char* host_reject_message, type_stringifier* type_string)
{
    struct prepare_error_message_visitor {
        void expand_output_to_next(char c)
        {
            while( script[loc.bytes_before_line + output_line_size - 1] != c )
                ++output_line_size;
        }

        void expand_output_to_full_line()
        {
            while( true ) {
                const char next = script[loc.bytes_before_line + output_line_size];
                if( next == '\n' || next == 0 )
                    break;
                ++output_line_size;
            }
        }

        void expand_output_to_current_word_end()
        {
            while( is_word_continuation(script[loc.bytes_before_line + output_line_size]) )
                ++output_line_size;
        }

        const char* rejected_message(const char* fallback) { return host_reject_message ? host_reject_message : fallback; }

        std::string operator()(TypeCheckError val)
        {
            loc = error_find_script_location(val, error.sub_index, error.location.front(), ast, script);
            output_line_size = loc.pos + 1;

            switch( val ) {
            case pruto::TypeCheckError::InvalidMember:
                expand_output_to_current_word_end();
                return "Invalid data member\n  members: " + enum_member_list(error.mismatch_expected);
            case pruto::TypeCheckError::RefNotAStruct:
                expand_output_to_current_word_end();
                ++output_line_size;
                return "Name is not a struct\n  type: " + type_string(error.mismatch_actual);
            case pruto::TypeCheckError::ListElementTypeMismatch:
                return "List element " + std::to_string(error.sub_index + 1) + " has a different type than previous ones"
                        "\n  previous type: " + type_string(error.mismatch_expected) +
                        "\n           type: " + type_string(error.mismatch_actual);
            case pruto::TypeCheckError::ConditionalNotBoolean:
                expand_output_to_full_line();
                return "Conditional expression does not have a boolean type"
                        "\n  type: " + type_string(error.mismatch_actual);
            case pruto::TypeCheckError::SwitchNotEnum:
                expand_output_to_full_line();
                return "Switch expression does not have an enum type"
                        "\n  type: " + type_string(error.mismatch_actual);
            case pruto::TypeCheckError::MissingCaseOrElse:
                expand_output_to_full_line();
                assert(!error.missing_cases.empty());
                return "Switch block has unhandled cases (or missing an 'else' path)"
                        "\n  missing cases: " + join_strings(error.missing_cases);
            case pruto::TypeCheckError::ReassignTypeMismatch:
                ++output_line_size;
                return "Var is reassigned a value of different type"
                        "\n         var type: " + type_string(error.mismatch_expected) +
                        "\n  reassigned type: " + type_string(error.mismatch_actual);
            case pruto::TypeCheckError::ReturnTypeMismatch:
                expand_output_to_full_line();
                return "Return expression is of different type than previous ones"
                        "\n  previous  type: " + type_string(error.mismatch_expected) +
                        "\n            type: " + type_string(error.mismatch_actual);
            case pruto::TypeCheckError::HostRejetedProc:
                if( is_word_start(script[loc.bytes_before_line + loc.pos]) ) {
                    expand_output_to_current_word_end();
                    std::string proc(script + loc.bytes_before_line + loc.pos, output_line_size - loc.pos);
                    expand_output_to_next('(');
                    std::string msg = "Host rejected procedure call instance '" + proc + "'";
                    return rejected_message(msg.data());
                } else {
                    output_line_size += sym_op_size(script + loc.bytes_before_line + loc.pos) - 1;
                    std::string op(script + loc.bytes_before_line + loc.pos, output_line_size - loc.pos);
                    std::string msg = "Host rejected operator call instance '" + op + "'";
                    return rejected_message(msg.data());
                }
            case pruto::TypeCheckError::HostRejetedList:
                return rejected_message("Host rejected list expression");
            case pruto::TypeCheckError::HostRejetedListOp:
                return rejected_message("Host rejected list operator expression");
            case pruto::TypeCheckError::HostRejetedIter:
                expand_output_to_full_line();
                return rejected_message("Host rejected iterator expression");
            }
            assert(false);
            return {};
        }
        std::string operator()(code_generator::LimitError val)
        {
            loc = error_find_script_location(val, error.location.front(), ast, script);
            output_line_size = loc.pos + 1;

            const auto& ast_loc = error.location.front();

            switch( val ) {
            case code_generator::LimitError::TypeExceedsSizeLimit:
                if( ast_loc.expression_element < 0 ) {
                    expand_output_to_current_word_end();
                } else {
                    // possible expressions: exp_call (regular or operator), exp_ref, exp_list, exp_struct
                    // expand the output_line_size for exp_call and exp_ref
                    const auto& exp = ast_find_expression_element(ast, ast_loc);
                    if( std::holds_alternative<exp_call>(exp) ) {
                        if( is_word_start(script[loc.bytes_before_line + loc.pos]) )
                            expand_output_to_next('(');
                        else
                            output_line_size += sym_op_size(script + loc.bytes_before_line + loc.pos) - 1;
                    } else if( std::holds_alternative<exp_ref>(exp) ) {
                        while( true ) {
                            expand_output_to_current_word_end();
                            if( script[loc.bytes_before_line + output_line_size] == '.' ) {
                                output_line_size += 2;
                                continue;
                            }
                            break;
                        }
                    }
                }
                return "Type exceeds the memory size limit\n  type: " + type_string(error.mismatch_actual);
            }
            assert(false);
            return {};
        }

        const code_generator::prepare_result_error& error;
        const script_section_ast& ast;
        const char* script;
        const char* host_reject_message;
        type_stringifier& type_string;

        error_script_location loc = {BugIndicatingValue, BugIndicatingValue, BugIndicatingValue};
        size_t output_line_size = 0;
    } v{error, ast, script, host_reject_message, type_string == nullptr ? default_stringifier : *type_string};

    auto s = std::visit(v, error.val);
    s += at_line_column_info_line(v.loc.line, v.loc.pos) + proc_instance_suffix(error.location.front(), ast, v.type_string);
    s += quoted_line(script + v.loc.bytes_before_line, v.output_line_size);
    for( size_t i = 1; i < error.location.size(); ++i )
        s += called_from_string_line(error.location[i], ast, v.type_string);
    return s;
}

std::string error_message(const code_generator::prepare_result& r, const script_section_ast& ast,
                          const char* script, const char* host_reject_message, type_stringifier* type_string)
{
    return r.error ? error_message(*r.error, ast, script, host_reject_message, type_string) : std::string(InternalError);
}

std::string error_message(const code_generator::reset_result& r, const script_section_ast& ast,
                          const char* script, const char* host_reject_message, type_stringifier* type_string)
{
    struct reset_error_message_visitor {
        std::string operator()(std::monostate) { return InternalError; }
        std::string operator()(const code_generator::literal_error& e)
        {
            std::string s;
            if( host_reject_message != nullptr ) {
                s = host_reject_message;
            } else {
                switch( e.val ) {
                case LiteralError::HostDisabledNumLiterals:
                    s = "Host does not accept any numeric literals";
                    break;
                case LiteralError::HostDisabledStrLiterals:
                    s = "Host does not accept any string literals";
                    break;
                case LiteralError::HostRejectedNumLiteral:
                    s = "Host rejected specific numeric literal";
                    break;
                case LiteralError::HostRejectedStrLiteral:
                    s = "Host rejected specific string literal";
                    break;
                }
            }
            const auto loc = error_find_script_location(e, ast, script);
            s += at_line_column_info_line(loc.line, loc.pos);
            {
                const char* line = script + loc.bytes_before_line;
                size_t size = loc.pos + 1;
                if( line[size - 1] == '"' ) {
                    while( true ) {
                        if( line[size] == '\n' )
                            break;
                        ++size;
                        if( line[size - 1] == '"' )
                            break;
                        if( line[size - 1] == '\\' )
                            ++size;
                    }
                } else {
                    assert(is_digit(line[size - 1]));
                    while( is_numlit_continuation(line[size]) )
                        ++size;
                }
                s += quoted_line(line, size);
            }
            return s;
        }
        std::string operator()(const code_generator::prepare_result_error_ptr& e)
        {
            return error_message(*e, ast, script, host_reject_message, type_string);
        }

        const script_section_ast& ast;
        const char* script;
        const char* host_reject_message;
        type_stringifier* type_string;
    };
    return std::visit(reset_error_message_visitor{ast, script, host_reject_message, type_string}, r);
}


std::string current_backtrace_string(const code_generator& gen, type_stringifier* type_string)
{
    const auto bt = current_backtrace(gen);
    if( bt.empty() )
        return "  (empty backtrace)";
    const auto& ast = *gen.current_ast();
    std::string s("  at ");
    {
        const auto& loc = bt.front();
        if( !loc.proc_id && loc.statement == ast.globals.size() ) {
            s += "host call";
        } else if( loc.expression_element < 0 ) {
            s += "statement line " + line_number(ast_find_statement_script_line(ast, loc));
        } else {
            const auto exp = exp_element_script_location(ast_find_expression_element(ast, loc));
            s += line_column_info(exp.line, exp.pos);
        }
        s += proc_instance_suffix(loc, ast, type_string == nullptr ? default_stringifier : *type_string);
    }
    for( auto it = bt.begin() + 1, end = bt.end(); it != end; ++it ) {
        if( !it->proc_id && it->statement == ast.globals.size() ) {
            s += "\n    called from inside host call";
        } else if( it->expression_element < 0 ) {
            // can only happen when some host callbacks that are not executed inside expressions do
            // cause (either directly or indirectly) a backtrace generation
            s += "\n    called in statement line " + line_number(ast_find_statement_script_line(ast, *it))
                    + proc_instance_suffix(*it, ast, type_string == nullptr ? default_stringifier : *type_string);
        } else {
            s += called_from_string_line(*it, ast, type_string == nullptr ? default_stringifier : *type_string);
        }
    }
    return s;
}


static error_script_location error_find_script_location_base(const code_generator::ast_location& loc,
                                                             const script_section_ast& ast, const char* script)
{
    if( loc.expression_element >= 0 ) {
        const auto exp = exp_element_script_location(ast_find_expression_element(ast, loc));
        return {exp.line, find_line_offset(script, exp.line), exp.pos};
    }
    assert(loc.proc_id && loc.proc_id.index() < ast.globals.size());
    const auto& proc_def = ast_get_global_proc(ast, loc.proc_id);
    assert(loc.statement < proc_def.statements.size());
    const auto line = proc_statement_script_line(proc_def.statements[loc.statement]);
    const auto bytes_before_line = find_line_offset(script, line);
    uint32_t pos = 0;
    while( is_ws(script[bytes_before_line + pos]) )
        ++pos;
    return {line, bytes_before_line, pos};
}

error_script_location error_find_script_location(TypeCheckError e, size_t sub_index,
                                                 const code_generator::ast_location& loc,
                                                 const script_section_ast& ast, const char* script)
{
    auto l = error_find_script_location_base(loc, ast, script);

    switch( e ) {
    case pruto::TypeCheckError::InvalidMember:
        // move error pos to the member causing the error
        if( loc.expression_element < 0 ) {
            // can only happen in a ref from a reassignment statement or in a case list
            assert(loc.proc_id && loc.proc_id.index() < ast.globals.size());

            const auto& proc_def = ast_get_global_proc(ast, loc.proc_id);
            if( std::holds_alternative<stm_case>(proc_def.statements[loc.statement]) )
                return error_find_case_label(l, sub_index, script);
        }
        assert(is_word_start(script[l.bytes_before_line + l.pos]));
        for( size_t i = 0; i < sub_index; ++i ) {
            while( true ) {
                ++l.pos;
                const char c = script[l.bytes_before_line + l.pos];
                if( c == '.' || c == ':' )
                    break;
            }
            ++l.pos;
        }
        break;
    case pruto::TypeCheckError::RefNotAStruct:
        // move error pos to the member causing the error
        assert(is_word_start(script[l.bytes_before_line + l.pos]));
        for( size_t i = 0; i < sub_index; ++i ) {
            while( script[l.bytes_before_line + l.pos] != '.' )
                ++l.pos;
            ++l.pos;
        }
        break;
    case pruto::TypeCheckError::ReassignTypeMismatch:
        // move error pos to ":" from the ":=" reassignment
        while( script[l.bytes_before_line + l.pos] != ':' )
            ++l.pos;
        break;
    default: ;
    }

    return l;
}

error_script_location error_find_script_location(const code_generator::literal_error& e,
                                                 const script_section_ast& ast, const char* script)
{
    const auto exp = exp_element_script_location(ast_find_expression_element(ast, e));
    return {exp.line, find_line_offset(script, exp.line), exp.pos};
}

error_script_location error_find_script_location(code_generator::LimitError,
                                                 const code_generator::ast_location& loc,
                                                 const script_section_ast& ast, const char* script)
{
    auto l = error_find_script_location_base(loc, ast, script);
    if( loc.expression_element < 0 ) {
        // can only happen when the element value of a host for iterator is too big
        // (otherwise LimitError only happen inside expressions)
        // position the error to the identifier after 'for'
        assert(script[l.bytes_before_line + l.pos] == 'f'
                && script[l.bytes_before_line + l.pos + 1] == 'o'
                && script[l.bytes_before_line + l.pos + 2] == 'r');
        l.pos += 3;
        while( is_ws(script[l.bytes_before_line + l.pos]) )
            ++l.pos;
    }
    return l;
}

error_script_location error_find_case_label(error_script_location case_statement_loc, size_t index, const char* script)
{
    struct error_case_label_callbacks : public statement_callbacks {
        const char* inner_newline(const char* line, size_t pos) override
        {
            ++loc.line;
            loc.bytes_before_line += pos + 1;
            return line + pos + 1;
        }
        bool case_stm(const char*, size_t) override { return true; }
        bool case_stm_label(const char*, size_t label_start, size_t) override
        {
            if( index == current )
                loc.pos = label_start;
            ++current;
            return current <= index;
        }
        bool param_label_separator(const char*, size_t) override { return true; }

        error_script_location loc;
        size_t index;
        size_t current = 0;
    } cb;
    cb.loc = case_statement_loc;
    cb.index = index;
    parse_statement(script + case_statement_loc.bytes_before_line, BlockNesting::Switch, InsideLoop::No, &cb);
    return cb.loc;
}

}  // namespace
