/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pruto/type_system.h"

#include <unordered_set>

namespace pruto {

static inline bool operator ==(const type_struct::member& l, const type_struct::member& r)
{
    return l.name == r.name && l.type == r.type;
}

bool operator ==(const type_struct& l, const type_struct& r)
{
    return l.members == r.members;
}

bool type_is_valid(const type_info& t)
{
    struct valid_visitor {
        bool operator()(const type_host& t) { return t.id != InvalidHostType; }
        bool operator()(const type_boolean&) { return true; }
        bool operator()(const type_struct& t)
        {
            std::unordered_set<std::string_view> s;
            for( const auto& m : t.members ) {
                if( !is_identifier(m.name.data()) )
                    return false;
                if( s.find(m.name) != s.end() )
                    return false;
                s.insert(m.name);
                if( !type_is_valid(m.type) )
                    return false;
            }
            return true;
        }
        bool operator()(const type_enum& t)
        {
            std::unordered_set<std::string_view> s;
            if( t.members.empty() )
                return false;
            for( const auto& m : t.members ) {
                if( !is_identifier(m.data()) )
                    return false;
                if( s.find(m) != s.end() )
                    return false;
                s.insert(m);
            }
            return true;
        }
        bool operator()(const type_proc& t) { return t.id.is_proc(); }
    };
    return std::visit(valid_visitor(), t);
}

enum_and_index get_enum_and_index(const type_struct& s, const std::string& member)
{
    enum_and_index r;

    const size_t N = s.members.size();
    r.e.members.reserve(N);
    r.index = N;

    bool found = false;
    for( size_t i = 0; i < N; ++i ) {
        const auto& name = s.members[i].name;
        if( !found && (name == member) ) {
            r.index = i;
            found = true;
        }
        r.e.members.push_back(name);
    }
    return r;
}

} // namespace
