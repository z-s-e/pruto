/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pruto/parser.h"

#include <cassert>

namespace pruto {

constexpr bool is_symop_continuation(char c)
{
    switch( c ) {
    case '&':
    case '|':
    case '+':
    case '-':
    case '*':
    case '/':
    case '\\':
    case '~':
    case '%':
    case '#':
    case '^':
    case '\'':
    case '<':
    case '>':
    case '!':
        return true;
    default:
        return false;
    }
}

enum class WordStatus {
    Invalid,
    B, Br, Bre, Brea, Break,
    C, Ca, Cas, Case, Co, Con, Cont, Conti, Contin, Continu, Continue,
    E, El, Els, Else, En, End,
    F, Fa, Fal, Fals, False, Fo, For,
    I, If, In,
    P, Pr, Pro, Proc,
    R, Re, Ret, Retu, Retur, Return,
    S, Sw, Swi, Swit, Switc, Switch,
    T, Tr, Tru, True,
    V, Va, Var,
    W, Wh, Whi, Whil, While,
    Other
};

struct word_result {
    WordStatus word = WordStatus::Invalid;
    bool keyword = true;
    size_t end = 0;
};

static word_result read_word(const char* line, size_t pos)
{
    assert(is_word_start(line[pos]));
    word_result r;

    switch( line[pos] ) {
    case 'b': r.word = WordStatus::B; break;
    case 'c': r.word = WordStatus::C; break;
    case 'e': r.word = WordStatus::E; break;
    case 'f': r.word = WordStatus::F; break;
    case 'i': r.word = WordStatus::I; break;
    case 'p': r.word = WordStatus::P; break;
    case 'r': r.word = WordStatus::R; break;
    case 's': r.word = WordStatus::S; break;
    case 't': r.word = WordStatus::T; break;
    case 'v': r.word = WordStatus::V; break;
    case 'w': r.word = WordStatus::W; break;
    default:
        ++pos;
        goto word_other;
    }
    ++pos;

    while( true ) {
        const char c = line[pos];
        switch( r.word ) {
        case WordStatus::B:
            if( c != 'r' ) goto word_other;
            r.word = WordStatus::Br; break;
        case WordStatus::Br:
            if( c != 'e' ) goto word_other;
            r.word = WordStatus::Bre; break;
        case WordStatus::Bre:
            if( c != 'a' ) goto word_other;
            r.word = WordStatus::Brea; break;
        case WordStatus::Brea:
            if( c != 'k' ) goto word_other;
            r.word = WordStatus::Break; goto after_keyword;
        case WordStatus::C:
            if( c == 'o' )      r.word = WordStatus::Co;
            else if( c == 'a' ) r.word = WordStatus::Ca;
            else                goto word_other;
            break;
        case WordStatus::Ca:
            if( c != 's' ) goto word_other;
            r.word = WordStatus::Cas; break;
        case WordStatus::Cas:
            if( c != 'e' ) goto word_other;
            r.word = WordStatus::Case; goto after_keyword;
        case WordStatus::Co:
            if( c != 'n' ) goto word_other;
            r.word = WordStatus::Con; break;
        case WordStatus::Con:
            if( c != 't' ) goto word_other;
            r.word = WordStatus::Cont; break;
        case WordStatus::Cont:
            if( c != 'i' ) goto word_other;
            r.word = WordStatus::Conti; break;
        case WordStatus::Conti:
            if( c != 'n' ) goto word_other;
            r.word = WordStatus::Contin; break;
        case WordStatus::Contin:
            if( c != 'u' ) goto word_other;
            r.word = WordStatus::Continu; break;
        case WordStatus::Continu:
            if( c != 'e' ) goto word_other;
            r.word = WordStatus::Continue; goto after_keyword;
        case WordStatus::E:
            if( c == 'n' )      r.word = WordStatus::En;
            else if( c == 'l' ) r.word = WordStatus::El;
            else goto word_other;
            break;
        case WordStatus::El:
            if( c != 's' ) goto word_other;
            r.word = WordStatus::Els; break;
        case WordStatus::Els:
            if( c != 'e' ) goto word_other;
            r.word = WordStatus::Else; goto after_keyword;
        case WordStatus::En:
            if( c != 'd' ) goto word_other;
            r.word = WordStatus::End; goto after_keyword;
        case WordStatus::F:
            if( c == 'a' )      r.word = WordStatus::Fa;
            else if( c == 'o' ) r.word = WordStatus::Fo;
            else goto word_other;
            break;
        case WordStatus::Fa:
            if( c != 'l' ) goto word_other;
            r.word = WordStatus::Fal; break;
        case WordStatus::Fal:
            if( c != 's' ) goto word_other;
            r.word = WordStatus::Fals; break;
        case WordStatus::Fals:
            if( c != 'e' ) goto word_other;
            r.word = WordStatus::False; goto after_keyword;
        case WordStatus::Fo:
            if( c != 'r' ) goto word_other;
            r.word = WordStatus::For; goto after_keyword;
        case WordStatus::I:
            if( c == 'f' )      { r.word = WordStatus::If; goto after_keyword; }
            else if( c == 'n' ) { r.word = WordStatus::In; goto after_keyword; }
            goto word_other;
        case WordStatus::P:
            if( c != 'r' ) goto word_other;
            r.word = WordStatus::Pr; break;
        case WordStatus::Pr:
            if( c != 'o' ) goto word_other;
            r.word = WordStatus::Pro; break;
        case WordStatus::Pro:
            if( c != 'c' ) goto word_other;
            r.word = WordStatus::Proc; goto after_keyword;
        case WordStatus::R:
            if( c != 'e' ) goto word_other;
            r.word = WordStatus::Re; break;
        case WordStatus::Re:
            if( c != 't' ) goto word_other;
            r.word = WordStatus::Ret; break;
        case WordStatus::Ret:
            if( c != 'u' ) goto word_other;
            r.word = WordStatus::Retu; break;
        case WordStatus::Retu:
            if( c != 'r' ) goto word_other;
            r.word = WordStatus::Retur; break;
        case WordStatus::Retur:
            if( c != 'n' ) goto word_other;
            r.word = WordStatus::Return; goto after_keyword;
        case WordStatus::S:
            if( c != 'w' ) goto word_other;
            r.word = WordStatus::Sw; break;
        case WordStatus::Sw:
            if( c != 'i' ) goto word_other;
            r.word = WordStatus::Swi; break;
        case WordStatus::Swi:
            if( c != 't' ) goto word_other;
            r.word = WordStatus::Swit; break;
        case WordStatus::Swit:
            if( c != 'c' ) goto word_other;
            r.word = WordStatus::Switc; break;
        case WordStatus::Switc:
            if( c != 'h' ) goto word_other;
            r.word = WordStatus::Switch; goto after_keyword;
        case WordStatus::T:
            if( c != 'r' ) goto word_other;
            r.word = WordStatus::Tr; break;
        case WordStatus::Tr:
            if( c != 'u' ) goto word_other;
            r.word = WordStatus::Tru; break;
        case WordStatus::Tru:
            if( c != 'e' ) goto word_other;
            r.word = WordStatus::True; goto after_keyword;
        case WordStatus::V:
            if( c != 'a' ) goto word_other;
            r.word = WordStatus::Va; break;
        case WordStatus::Va:
            if( c != 'r' ) goto word_other;
            r.word = WordStatus::Var; goto after_keyword;
        case WordStatus::W:
            if( c != 'h' ) goto word_other;
            r.word = WordStatus::Wh; break;
        case WordStatus::Wh:
            if( c != 'i' ) goto word_other;
            r.word = WordStatus::Whi; break;
        case WordStatus::Whi:
            if( c != 'l' ) goto word_other;
            r.word = WordStatus::Whil; break;
        case WordStatus::Whil:
            if( c != 'e' ) goto word_other;
            r.word = WordStatus::While; goto after_keyword;
        default: assert(false);
        }
        ++pos;
    }

after_keyword:
    ++pos;
    if( is_word_continuation(line[pos]) ) {
        ++pos;
word_other:
        r.keyword = false;
        r.word = WordStatus::Other;
        while( is_word_continuation(line[pos]) )
            ++pos;
    }

    assert(r.word != WordStatus::Invalid);
    r.end = pos;
    return r;
}

static size_t skip_ws(const char* line, size_t pos)
{
    while( is_ws(line[pos]) )
        ++pos;
    return pos;
}

static size_t skip_comment(const char* line, size_t pos)
{
    assert(line[pos] == ';');
    ++pos;
    while( line[pos] != '\n' && line[pos] != 0 )
        ++pos;
    return pos;
}

template<typename Callback, typename Error>
static bool err(const char* line, Error e, size_t pos, size_t size, Callback* cb)
{
    cb->error(line, e, pos, size);
    return false;
}

static bool err_char_exp(const char* line, size_t pos, expression_callbacks* cb)
{
    return err(line, ExpressionError::UnexpectedCharacter, pos, 1, cb);
}
static bool err_char_stm(const char* line, size_t pos, statement_callbacks* cb)
{
    return err(line, StatementError::UnexpectedCharacter, pos, 1, cb);
}

static bool read_ident_exp(const char* line, size_t* posref, expression_callbacks* cb)
{
    const size_t pos = *posref;
    if( !is_word_start(line[pos]) )
        return err_char_exp(line, pos, cb);
    auto w = read_word(line, pos);
    if( w.keyword )
        return err(line, ExpressionError::UnexpectedKeyword, pos, w.end - pos, cb);
    *posref = w.end;
    return true;
}

static bool read_ident_stm(const char* line, size_t* posref, statement_callbacks* cb)
{
    const size_t pos = *posref;
    if( !is_word_start(line[pos]) )
        return err_char_stm(line, pos, cb);
    auto w = read_word(line, pos);
    if( w.keyword )
        return err(line, StatementError::UnexpectedKeyword, pos, w.end - pos, cb);
    *posref = w.end;
    return true;
}

static size_t read_inner_space(const char** lineref, size_t pos, expression_callbacks* cb)
{
    const char* line = *lineref;
    while( true ) {
        pos = skip_ws(line, pos);
        if( line[pos] == ';' ) {
            cb->comment_start(line, pos);
            pos = skip_comment(line, pos);
        }
        if( line[pos] == '\n' ) {
            line = cb->inner_newline(line, pos);
            pos = 0;
        } else {
            *lineref = line;
            return pos;
        }
    }
}

static bool read_strlit(const char** lineref, size_t* posref, expression_callbacks* cb)
{
    const char* line = *lineref;
    size_t pos = *posref;

    assert(line[pos] == '"');
    if( !cb->str_lit_start(line, pos) )
        return false;
    pos = skip_ws(line, pos + 1);
    if( line[pos] == '\n' ) {
        while( true ) {
            assert(line[pos] == '\n');
            line = cb->inner_newline(line, pos);
            pos = skip_ws(line, 0);
            if( line[pos] == '>' ) {
                ++pos;
                if( is_ws(line[pos]) ) {
                    ++pos;
                    while( line[pos] != '\n' ) {
                        if( line[pos] == 0 )
                            return err_char_exp(line, pos, cb);
                        ++pos;
                    }
                } else if( line[pos] != '\n' ) {
                    return err_char_exp(line, pos, cb);
                }
            } else if( line[pos] == '"' ) {
                break;
            } else {
                return err_char_exp(line, pos, cb);
            }
        }
    } else {
        while( line[pos] != '"' ) {
            if( line[pos] == '\\' ) {
                ++pos;
                if( (line[pos] != '"') && (line[pos] != '\\') && (line[pos] != 'n') )
                    return err_char_exp(line, pos, cb);
            } else if( (line[pos] == '\n') || (line[pos] == 0) ) {
                return err_char_exp(line, pos, cb);
            }
            ++pos;
        }
    }

    assert(line[pos] == '"');
    *lineref = line;
    *posref = pos + 1;
    return cb->str_lit_end(line, pos);
}

static size_t read_ref(const char* line, size_t* posref, expression_callbacks* cb)
{
    size_t pos = *posref;
    const size_t ref_start = pos;
    if( !read_ident_exp(line, &pos, cb) )
        return 0;
    const size_t ident_size = pos - ref_start;
    while( line[pos] == '.' ) {
        ++pos;
        if( !read_ident_exp(line, &pos, cb) )
            return 0;
    };
    *posref = pos;
    return ident_size;
}

static bool read_enum_exp(const char* line, size_t* posref, bool need_operator, expression_callbacks* cb)
{
    size_t pos = *posref;

    assert(line[pos] == ':');
    if( !is_word_start(line[pos + 1]) )
        return err(line, ExpressionError::UnexpectedNonEnumColon, pos, 1, cb);
    ++pos;

    const size_t ref_start = pos;
    const size_t ident_size = read_ref(line, &pos, cb);
    if( ident_size == 0 )
        return false;
    const size_t ref_size = pos - ref_start;
    if( line[pos] != ':' )
        return err_char_exp(line, pos, cb);
    ++pos;
    const size_t mem_start = pos;
    if( !read_ident_exp(line, &pos, cb) )
        return false;

    *posref = pos;
    if( need_operator )
        return err(line, ExpressionError::MissingOperatorBetweenOperands, ref_start - 1, pos - ref_start + 1, cb);
    return cb->enum_ref(line, ref_start, ident_size, ref_size, pos - mem_start);
}

// these use the following read_exp recursively, so forward-declare:
template<char Term>
static bool read_exp_list(const char** lineref, size_t* posref, int nesting, expression_callbacks* cb);
static bool read_struct(const char** lineref, size_t* posref, int nesting, expression_callbacks* cb);

static bool read_exp(const char** lineref, size_t* posref, int nesting, expression_callbacks* cb)
{
    assert(nesting <= ExpressionNestingLimit);

    const char* line = *lineref;
    size_t pos = *posref;

    bool has_operand = false;
    bool need_operator = false;

    while( true ) {
        const char c = line[pos];

        if( is_ws(c) ) {
            ++pos;
            continue;
        } else if( is_word_start(c) ) {
            const auto ident_start = pos;
            auto w = read_word(line, pos);
            const size_t ident_size = w.end - ident_start;

            if( w.keyword && w.word != WordStatus::True && w.word != WordStatus::False )
                return err(line, ExpressionError::UnexpectedKeyword, ident_start, ident_size, cb);
            if( need_operator && (w.word == WordStatus::True || w.word == WordStatus::False) )
                return err(line, ExpressionError::MissingOperatorBetweenOperands, ident_start, ident_size, cb);

            if( w.word == WordStatus::True ) {
                if( !cb->true_lit(line, pos) )
                    return false;
                pos = w.end;
            } else if( w.word == WordStatus::False ) {
                if( !cb->false_lit(line, pos) )
                    return false;
                pos = w.end;
            } else {
                pos = w.end;
                while( line[pos] == '.' ) {
                    ++pos;
                    if( !read_ident_exp(line, &pos, cb) )
                        return false;
                };
                const auto ref_size = pos - ident_start;
                if( need_operator )
                    return err(line, ExpressionError::MissingOperatorBetweenOperands, ident_start, ref_size, cb);
                pos = skip_ws(line, pos);
                if( ident_size == ref_size && line[pos] == '(' ) {
                    if( nesting == ExpressionNestingLimit )
                        return err(line, ExpressionError::NestingLimitExceeded, pos, 1, cb);
                    if( !cb->call_start(line, ident_start, ident_size, pos) )
                        return false;
                    if( !read_exp_list<')'>(&line, &pos, nesting + 1, cb) )
                        return false;
                    if( !cb->call_end(line, pos) )
                        return false;
                    ++pos;
                } else if( !cb->ref(line, ident_start, ident_size, ref_size) ) {
                    return false;
                }
            }
            has_operand = true;
            need_operator = true;
            continue;
        } else if( is_digit(c) ) {
            const auto num_start = pos;
            ++pos;
            while( is_numlit_continuation(line[pos]) )
                ++pos;

            if( need_operator )
                return err(line, ExpressionError::MissingOperatorBetweenOperands, num_start, pos - num_start, cb);
            if( !cb->num_lit(line, num_start, pos - num_start) )
                return false;
            has_operand = true;
            need_operator = true;
            continue;
        }

        switch( c ) {
        case '(':
        case '[':
        case '{':
        case '"':
            if( need_operator && c != '[' )
                return err(line, ExpressionError::MissingOperatorBetweenOperands, pos, 1, cb);

            if( c == '"' ) {
                if( !read_strlit(&line, &pos, cb) )
                    return false;
            } else if( nesting == ExpressionNestingLimit ) {
                return err(line, ExpressionError::NestingLimitExceeded, pos, 1, cb);
            } else if( c == '(' ) {
                if( !cb->group_start(line, pos) )
                    return false;
                ++pos;
                if( !read_exp(&line, &pos, nesting + 1, cb) )
                    return false;
                if( line[pos] != ')' )
                    return err_char_exp(line, pos, cb);
                if( !cb->group_end(line, pos) )
                    return false;
                ++pos;
            } else if( c == '[' ) {
                if( !cb->list_start(line, pos) )
                    return false;
                if( !read_exp_list<']'>(&line, &pos, nesting + 1, cb) )
                    return false;
                if( !cb->list_end(line, pos) )
                    return false;
                ++pos;
            } else if( !read_struct(&line, &pos, nesting + 1, cb) ) {
                return false;
            }

            has_operand = true;
            need_operator = true;
            break;
        case ':':
            if( !read_enum_exp(line, &pos, need_operator, cb) )
                return false;
            has_operand = true;
            need_operator = true;
            break;
        case '+':
        case '-':
        case '*':
        case '/':
        case '\\':
        case '~':
        case '%':
        case '#':
        case '^':
        case '\'':
            if( !cb->symop(line, pos, 1) )
                return false;
            ++pos;
            need_operator = false;
            break;
        case '&':
        case '|':
            if( is_symop_continuation(line[pos + 1]) ) {
                if( !cb->symop(line, pos, 2) )
                    return false;
                pos += 2;
            } else {
                if( !cb->symop(line, pos, 1) )
                    return false;
                ++pos;
            }
            need_operator = false;
            break;
        case '<':
        case '>':
        case '!':
        case '=':
            if( line[pos + 1] == '=' ) {
                if( !cb->symop(line, pos, 2) )
                    return false;
                pos += 2;
            } else if( c != '=' ) {
                if( !cb->symop(line, pos, 1) )
                    return false;
                ++pos;
            } else {
                return err(line, ExpressionError::UnexpectedAssignSymbol, pos, 1, cb);
            }
            need_operator = false;
            break;
        case ';':
        case '\n':
            if( nesting > 0 ) {
                pos = read_inner_space(&line, pos, cb);
                continue;
            }
            [[fallthrough]];
        default:
            if( !has_operand )
                return err(line, ExpressionError::MissingOperand, pos, 0, cb);
            *lineref = line;
            *posref = pos;
            return true;
        }
    }
}

template<char Term>
static bool read_exp_list(const char** lineref, size_t* posref, int nesting, expression_callbacks* cb)
{
    const char* line = *lineref;
    size_t pos = *posref;
    assert( ((line[pos] == '(') && (Term == ')')) || ((line[pos] == '[') && (Term == ']')) );

    pos = read_inner_space(&line, pos + 1, cb);
    if( line[pos] == Term )
        goto exp_list_end;

    while( true ) {
        if( !read_exp(&line, &pos, nesting, cb) )
            return false;
        if( line[pos] != ',' )
            break;
        if( !cb->exp_mem_separator(line, pos) )
            return false;
        ++pos;
    }
    if( line[pos] != Term )
        return err_char_exp(line, pos, cb);

exp_list_end:
    *lineref = line;
    *posref = pos;
    return true;
}

static bool read_struct(const char** lineref, size_t* posref, int nesting, expression_callbacks* cb)
{
    const char* line = *lineref;
    size_t pos = *posref;

    assert(line[pos] == '{');
    if( !cb->struct_start(line, pos) )
        return false;
    ++pos;

    pos = read_inner_space(&line, pos, cb);
    if( line[pos] == '}' )
        goto struct_end;

    while( true ) {
        const size_t ref_start = pos;
        const size_t ident_size = read_ref(line, &pos, cb);
        if( ident_size == 0 )
            return false;
        const size_t ref_size = pos - ref_start;

        if( line[pos] == ':' ) {
            if( !cb->struct_mem_value(line, ref_start, ident_size, ref_size) )
                return false;
            ++pos;
            if( !read_exp(&line, &pos, nesting, cb) )
                return false;
        } else {
            if( !cb->struct_mem(line, ref_start, ident_size, ref_size) )
                return false;
            pos = read_inner_space(&line, pos, cb);
        }

        if( line[pos] != ',' )
            break;
        if( !cb->exp_mem_separator(line, pos) )
            return false;
        pos = read_inner_space(&line, pos + 1, cb);
    }
    if( line[pos] != '}' )
        return err_char_exp(line, pos, cb);

struct_end:
    assert(line[pos] == '}');
    if( !cb->struct_end(line, pos) )
        return false;

    *lineref = line;
    *posref = pos + 1;
    return true;
}

expression_parse_result parse_expression(const char* line, size_t start, expression_callbacks* cb)
{
    return read_exp(&line, &start, 0, cb) ? expression_parse_result{line, start} : expression_parse_result{};
}


enum class EOS { No, Yes };

static bool read_line_suffix(const char* line, size_t pos, EOS allow_eos, statement_callbacks* cb)
{
    pos = skip_ws(line, pos);
    if( line[pos] == ';' ) {
        cb->comment_start(line, pos);
        pos = skip_comment(line, pos);
    }

    if( line[pos] == '\n' )
        cb->newline(line, pos);
    else if( line[pos] == 0 && (allow_eos == EOS::Yes) )
        cb->end_of_script(line, pos);
    else
        return err_char_stm(line, pos, cb);
    return true;
}

static bool read_section(const char* line, size_t pos, BlockNesting nesting, statement_callbacks* cb)
{
    assert(line[pos] == '$');
    if( pos != 0 )
        return err(line, StatementError::WhitespaceBeforeSection, 0, pos, cb);
    if( nesting != BlockNesting::Global )
        return err(line, StatementError::SectionInsideProc, pos, 1, cb);

    pos = skip_ws(line, pos + 1);
    const size_t ident_start = pos;
    if( !read_ident_stm(line, &pos, cb) )
        return false;

    if( !cb->section(line, ident_start, pos - ident_start) )
        return false;
    return read_line_suffix(line, pos, EOS::Yes, cb);
}

static bool read_exp_line(const char* line, size_t pos, EOS allow_eos, statement_callbacks* cb)
{
    if( !read_exp(&line, &pos, 0, cb) )
        return false;
    return read_line_suffix(line, pos, allow_eos, cb);
}

static bool read_ws_exp_line(const char* line, size_t pos,
                             StatementError missing_ws, statement_callbacks* cb)
{
    if( !is_ws(line[pos]) )
        return err(line, missing_ws, pos, 1, cb);
    return read_exp_line(line, pos + 1, EOS::No, cb);
}

static bool read_proc(const char* line, size_t pos, BlockNesting nesting, statement_callbacks* cb)
{
    const size_t proc_pos = pos;
    pos = skip_ws(line, pos + 4);

    const size_t ident_start = pos;
    if( !read_ident_stm(line, &pos, cb) )
        return false;
    const size_t ident_size = pos - ident_start;

    pos = skip_ws(line, pos);

    if( line[pos] == '=' ) {
        const size_t sym_pos = pos;
        pos = skip_ws(line, pos + 1);

        const size_t alias_pos = pos;
        if( !read_ident_stm(line, &pos, cb) )
            return false;

        if( !cb->proc_alias(line, proc_pos, ident_start, ident_size, sym_pos, alias_pos, pos - alias_pos) )
            return false;
        return read_line_suffix(line, pos, nesting == BlockNesting::Global ? EOS::Yes : EOS::No, cb);
    } else if( line[pos] != '(' ) {
        return err_char_stm(line, pos, cb);
    }

    if( nesting != BlockNesting::Global )
        return err(line, StatementError::NestedProcDefinition, proc_pos, pos - proc_pos, cb);
    if( !cb->proc_declaration(line, proc_pos, ident_start, ident_size, pos) )
        return false;

    pos = read_inner_space(&line, pos + 1, cb);
    if( line[pos] == ')' )
        goto declaration_end;

    while( true ) {
        const size_t param_start = pos;
        if( !read_ident_stm(line, &pos, cb) )
            return false;
        if( !cb->proc_declaration_param(line, param_start, pos - param_start) )
            return false;
        pos = read_inner_space(&line, pos, cb);
        if( line[pos] != ',' )
            break;
        if( !cb->param_label_separator(line, pos) )
            return false;
        pos = read_inner_space(&line, pos + 1, cb);
    }
    if( line[pos] != ')' )
        return err_char_stm(line, pos, cb);

declaration_end:
    if( !cb->proc_declaration_end(line, pos) )
        return false;
    return read_line_suffix(line, pos + 1, EOS::No, cb);
}

static bool read_return(const char* line, size_t pos, statement_callbacks* cb)
{
    const size_t ret_pos = pos;
    pos += 6;
    switch( line[pos] ) {
    case '\n':
    case ';':
        break;
    case ' ':
    case '\t':
        pos = skip_ws(line, pos + 1);
        break;
    default:
        return err(line, StatementError::MissingSpaceAfterReturn, pos, 1, cb);
    }

    if( line[pos] == ';' || line[pos] == '\n' ) {
        if( !cb->return_stm(line, ret_pos) )
            return false;
        return read_line_suffix(line, pos, EOS::No, cb);
    } else {
        if( !cb->return_value_stm(line, ret_pos) )
            return false;
        return read_exp_line(line, pos, EOS::No, cb);
    }
}

static bool read_var(const char* line, size_t pos, BlockNesting nesting, statement_callbacks* cb)
{
    const size_t var_pos = pos;
    pos = skip_ws(line, pos + 3);

    const size_t ident_start = pos;
    if( !read_ident_stm(line, &pos, cb) )
        return false;
    const size_t ident_size = pos - ident_start;

    pos = skip_ws(line, pos);
    if( line[pos] != '=' )
        return err_char_stm(line, pos, cb);
    if( !cb->var_def(line, var_pos, ident_start, ident_size, pos) )
        return false;
    return read_exp_line(line, pos + 1, nesting == BlockNesting::Global ? EOS::Yes : EOS::No, cb);
}

static bool read_else(const char* line, size_t pos, BlockNesting nesting, statement_callbacks* cb)
{
    if( (nesting != BlockNesting::If) && (nesting != BlockNesting::SwitchCase) )
        return err(line, StatementError::ElseOutsideConditional, pos, 4, cb);

    const size_t else_pos = pos;
    pos = skip_ws(line, pos + 4);

    if( (line[pos] == '\n') || (line[pos] == ';') ) {
        if( !cb->else_stm(line, else_pos) )
            return false;
        return read_line_suffix(line, pos, EOS::No, cb);
    }

    if( !is_word_start(line[pos]) )
        return err_char_stm(line, pos, cb);
    const auto w = read_word(line, pos);
    if( w.word != WordStatus::If )
        return err(line, StatementError::UnexpectedWordAfterElse, pos, w.end - pos, cb);
    if( !cb->else_if_stm(line, else_pos, pos) )
        return false;

    return read_ws_exp_line(line, pos + 2, StatementError::MissingSpaceAfterIf, cb);
}

static bool read_case(const char* line, size_t pos, BlockNesting nesting, statement_callbacks* cb)
{
    if( (nesting != BlockNesting::Switch) && (nesting != BlockNesting::SwitchCase) )
        return err(line, StatementError::CaseOutsideSwitch, pos, 4, cb);

    if( !is_ws(line[pos + 4]) )
        return err(line, StatementError::MissingSpaceAfterCase, pos + 4, 1, cb);

    if( !cb->case_stm(line, pos) )
        return false;
    pos = skip_ws(line, pos + 5);

    if( line[pos] == ':' ) {
        ++pos;
        const size_t label_start = pos;
        if( !read_ident_stm(line, &pos, cb) )
            return false;
        if( !cb->case_stm_label(line, label_start, pos - label_start) )
            return false;
        return read_line_suffix(line, pos, EOS::No, cb);
    } else if( line[pos] == '[' ) {
        if( !cb->param_label_separator(line, pos) )
            return false;

        pos = read_inner_space(&line, pos + 1, cb);
        while( true ) {
            if( line[pos] != ':' )
                return err_char_stm(line, pos, cb);
            ++pos;

            const size_t label_start = pos;
            if( !read_ident_stm(line, &pos, cb) )
                return false;
            if( !cb->case_stm_label(line, label_start, pos - label_start) )
                return false;

            pos = read_inner_space(&line, pos, cb);
            if( line[pos] != ',' )
                break;
            if( !cb->param_label_separator(line, pos) )
                return false;
            pos = read_inner_space(&line, pos + 1, cb);
        }
        if( line[pos] != ']' )
            return err_char_stm(line, pos, cb);
        if( !cb->param_label_separator(line, pos) )
            return false;
        return read_line_suffix(line, pos + 1, EOS::No, cb);
    } else {
        return err_char_stm(line, pos, cb);
    }
}

static bool read_for(const char* line, size_t pos, BlockNesting nesting, statement_callbacks* cb)
{
    if( nesting == BlockNesting::Global )
        return err(line, StatementError::KeywordOutsideProc, pos, 3, cb);

    const size_t for_pos = pos;
    pos = skip_ws(line, pos + 3);

    const size_t ident_start = pos;
    if( !read_ident_stm(line, &pos, cb) )
        return false;
    const size_t ident_size = pos - ident_start;

    pos = skip_ws(line, pos);
    if( !is_word_start(line[pos]) )
        return err_char_stm(line, pos, cb);
    const auto in = read_word(line, pos);
    if( in.word != WordStatus::In )
        return err(line, StatementError::NeedInAfterForIdent, pos, in.end - pos, cb);
    if( !cb->for_stm(line, for_pos, ident_start, ident_size, pos) )
        return false;

    return read_ws_exp_line(line, pos + 2, StatementError::MissingSpaceAfterForIn, cb);
}

static bool read_first_ident(const char* line, size_t pos, size_t size, BlockNesting nesting, statement_callbacks* cb)
{
    const size_t ident_start = pos;
    pos += size;
    while( line[pos] == '.' ) {
        ++pos;
        if( !read_ident_stm(line, &pos, cb) )
            return false;
    }
    const size_t ref_size = pos - ident_start;

    pos = skip_ws(line, pos);
    switch( line[pos] ) {
    case ':':
        if( line[pos + 1] != '=' )
            return err_char_stm(line, pos + 1, cb);
        if( nesting == BlockNesting::Global )
            return err(line, StatementError::ReassignOutsideProc, pos, 2, cb);
        if( !cb->reassign(line, ident_start, size, ref_size, pos) )
            return false;
        return read_exp_line(line, pos + 2, EOS::No, cb);
    case '=':
        if( ref_size != size )
            return err_char_stm(line, pos, cb);
        if( !cb->const_def(line, ident_start, size, pos) )
            return false;
        return read_exp_line(line, pos + 1, nesting == BlockNesting::Global ? EOS::Yes : EOS::No, cb);
    case '(':
        if( ref_size != size )
            return err_char_stm(line, pos, cb);
        if( nesting == BlockNesting::Global )
            return err(line, StatementError::CallOutsideProc, ident_start, pos - ident_start, cb);
        if( !cb->call_stm_start(line, ident_start, size, pos) )
            return false;
        if( !read_exp_list<')'>(&line, &pos, 1, cb) )
            return false;
        if( !cb->call_stm_end(line, pos) )
            return false;
        return read_line_suffix(line, pos + 1, EOS::No, cb);
    default:
        return err_char_stm(line, pos, cb);
    }
}

static bool read_statement_first_word(const char* line, size_t pos, BlockNesting nesting,
                                      InsideLoop loop, statement_callbacks* cb)
{
    if( !is_word_start(line[pos]) )
        return err_char_stm(line, pos, cb);
    const auto w = read_word(line, pos);
    const auto w_size = w.end - pos;

    if( (nesting == BlockNesting::Switch) && (w.word != WordStatus::Case) )
        return err(line, StatementError::NeedCaseAfterSwitch, pos, w_size, cb);

    switch( w.word ) {
    case WordStatus::Proc:
        return read_proc(line, pos, nesting, cb);
    case WordStatus::Return:
        if( nesting == BlockNesting::Global )
            return err(line, StatementError::KeywordOutsideProc, pos, w_size, cb);
        return read_return(line, pos, cb);
    case WordStatus::Var:
        return read_var(line, pos, nesting, cb);
    case WordStatus::If:
        if( nesting == BlockNesting::Global )
            return err(line, StatementError::KeywordOutsideProc, pos, w_size, cb);
        if( !cb->if_stm(line, pos) )
            return false;
        return read_ws_exp_line(line, pos + 2, StatementError::MissingSpaceAfterIf, cb);
    case WordStatus::Else:
        return read_else(line, pos, nesting, cb);
    case WordStatus::End:
        if( nesting == BlockNesting::Global )
            return err(line, StatementError::KeywordOutsideProc, pos, w_size, cb);
        if( !cb->end_stm(line, pos) )
            return false;
        return read_line_suffix(line, pos + 3, nesting == BlockNesting::Proc ? EOS::Yes : EOS::No, cb);
    case WordStatus::Switch:
        if( nesting == BlockNesting::Global )
            return err(line, StatementError::KeywordOutsideProc, pos, w_size, cb);
        if( !cb->switch_stm(line, pos) )
            return false;
        return read_ws_exp_line(line, pos + 6, StatementError::MissingSpaceAfterSwitch, cb);
    case WordStatus::Case:
        return read_case(line, pos, nesting, cb);
    case WordStatus::While:
        if( nesting == BlockNesting::Global )
            return err(line, StatementError::KeywordOutsideProc, pos, w_size, cb);
        if( !cb->while_stm(line, pos) )
            return false;
        return read_ws_exp_line(line, pos + 5, StatementError::MissingSpaceAfterWhile, cb);
    case WordStatus::For:
        return read_for(line, pos, nesting, cb);
    case WordStatus::Break:
        if( loop == InsideLoop::No )
            return err(line, StatementError::BreakOutsideLoop, pos, w_size, cb);
        if( !cb->break_stm(line, pos) )
            return false;
        return read_line_suffix(line, pos + 5, EOS::No, cb);
    case WordStatus::Continue:
        if( loop == InsideLoop::No )
            return err(line, StatementError::ContinueOutsideLoop, pos, w_size, cb);
        if( !cb->continue_stm(line, pos) )
            return false;
        return read_line_suffix(line, pos + 8, EOS::No, cb);
    case WordStatus::False:
    case WordStatus::True:
    case WordStatus::In:
        return err(line, StatementError::UnexpectedKeyword, pos, w_size, cb);
    default:
        return read_first_ident(line, pos, w_size, nesting, cb);
    }
}

void parse_statement(const char* line, BlockNesting current_nesting, InsideLoop loop, statement_callbacks* cb)
{
    assert(loop == InsideLoop::Yes || current_nesting != BlockNesting::Loop);
    assert(loop == InsideLoop::No || ((current_nesting != BlockNesting::Global) && (current_nesting != BlockNesting::Proc)));
    size_t pos = skip_ws(line, 0);

    switch( line[pos] ) {
    case ';':
    case 0:
        read_line_suffix(line, pos, current_nesting == BlockNesting::Global ? EOS::Yes : EOS::No, cb);
        break;
    case '\n':
        cb->newline(line, pos);
        break;
    case '$':
        read_section(line, pos, current_nesting, cb);
        break;
    default:
        read_statement_first_word(line, pos, current_nesting, loop, cb);
    }
}


SymOp sym_op(const char* start, size_t size)
{
    assert(size == 1 || size == 2);
    char c = start[0];
    int offset = 0;
    if( size == 2 ) {
        switch( c ) {
        case '&':
            offset = 15;
            break;
        case '|':
            offset = 30;
            break;
        case '<':
            assert(start[1] == '=');
            return SymOp::LessEqual;
        case '>':
            assert(start[1] == '=');
            return SymOp::GreaterEqual;
        case '!':
            assert(start[1] == '=');
            return SymOp::NotEqual;
        case '=':
            assert(start[1] == '=');
            return SymOp::Equal;
        default:
            assert(false);
        }
        c = start[1];
    }
    SymOp tmp = SymOp::Exclamation;
    switch( c ) {
    case '&': tmp = SymOp::And; break;
    case '|': tmp = SymOp::Bar; break;
    case '+': tmp = SymOp::Plus; break;
    case '-': tmp = SymOp::Minus; break;
    case '*': tmp = SymOp::Asterisk; break;
    case '/': tmp = SymOp::Slash; break;
    case '\\': tmp = SymOp::Backslash; break;
    case '~': tmp = SymOp::Tilde; break;
    case '%': tmp = SymOp::Percent; break;
    case '#': tmp = SymOp::Hash; break;
    case '^': tmp = SymOp::Caret; break;
    case '\'': tmp = SymOp::Apostrophe; break;
    case '<': tmp = SymOp::Less; break;
    case '>': tmp = SymOp::Greater; break;
    case '!': tmp = SymOp::Exclamation; break;
    default: assert(false);
    }
    return static_cast<SymOp>(static_cast<int>(tmp) + offset);
}

size_t sym_op_size(const char* start)
{
    switch( start[0] ) {
    case '&':
    case '|':
        return is_symop_continuation(start[1]) ? 2 : 1;
    case '=':
        return 2;
    case '<':
    case '>':
    case '!':
        return (start[1] == '=') ? 2 : 1;
    default:
        return 1;
    }
}

bool is_identifier(const char* start)
{
    if( !is_word_start(*start) )
        return false;
    const auto w = read_word(start, 0);
    if( w.keyword || (*(start + w.end) != 0) )
        return false;
    return true;
}

bool expression_callbacks::ref(const char*, size_t, size_t, size_t) { return false; }
bool expression_callbacks::enum_ref(const char*, size_t, size_t, size_t, size_t) { return false; }
bool expression_callbacks::symop(const char*, size_t, size_t) { return false; }
bool expression_callbacks::true_lit(const char*, size_t) { return false; }
bool expression_callbacks::false_lit(const char*, size_t) { return false; }
bool expression_callbacks::num_lit(const char*, size_t, size_t) { return false; }
bool expression_callbacks::str_lit_start(const char*, size_t) { return false; }
bool expression_callbacks::str_lit_end(const char*, size_t) { return false; }
bool expression_callbacks::group_start(const char*, size_t) { return false; }
bool expression_callbacks::group_end(const char*, size_t) { return false; }
bool expression_callbacks::call_start(const char*, size_t, size_t, size_t) { return false; }
bool expression_callbacks::call_end(const char*, size_t) { return false; }
bool expression_callbacks::struct_start(const char*, size_t) { return false; }
bool expression_callbacks::struct_mem(const char*, size_t, size_t, size_t) { return false; }
bool expression_callbacks::struct_mem_value(const char*, size_t, size_t, size_t) { return false; }
bool expression_callbacks::struct_end(const char*, size_t) { return false; }
bool expression_callbacks::list_start(const char*, size_t) { return false; }
bool expression_callbacks::list_end(const char*, size_t) { return false; }
bool expression_callbacks::exp_mem_separator(const char*, size_t) { return false; }
void expression_callbacks::comment_start(const char*, size_t) {}
void expression_callbacks::error(const char*, ExpressionError, size_t, size_t) {}
bool statement_callbacks::proc_declaration(const char*, size_t, size_t, size_t, size_t) { return false; }
bool statement_callbacks::proc_declaration_param(const char*, size_t, size_t) { return false; }
bool statement_callbacks::proc_declaration_end(const char*, size_t) { return false; }
bool statement_callbacks::proc_alias(const char*, size_t, size_t, size_t, size_t, size_t, size_t) { return false; }
bool statement_callbacks::const_def(const char*, size_t, size_t, size_t) { return false; }
bool statement_callbacks::var_def(const char*, size_t, size_t, size_t, size_t) { return false; }
bool statement_callbacks::reassign(const char*, size_t, size_t, size_t, size_t) { return false; }
bool statement_callbacks::call_stm_start(const char*, size_t, size_t, size_t) { return false; }
bool statement_callbacks::call_stm_end(const char*, size_t) { return false; }
bool statement_callbacks::return_stm(const char*, size_t) { return false; }
bool statement_callbacks::return_value_stm(const char*, size_t) { return false; }
bool statement_callbacks::if_stm(const char*, size_t) { return false; }
bool statement_callbacks::else_if_stm(const char*, size_t, size_t) { return false; }
bool statement_callbacks::else_stm(const char*, size_t) { return false; }
bool statement_callbacks::switch_stm(const char*, size_t) { return false; }
bool statement_callbacks::case_stm(const char*, size_t) { return false; }
bool statement_callbacks::case_stm_label(const char*, size_t, size_t) { return false; }
bool statement_callbacks::while_stm(const char*, size_t) { return false; }
bool statement_callbacks::for_stm(const char*, size_t, size_t, size_t, size_t) { return false; }
bool statement_callbacks::break_stm(const char*, size_t) { return false; }
bool statement_callbacks::continue_stm(const char*, size_t) { return false; }
bool statement_callbacks::end_stm(const char*, size_t) { return false; }
bool statement_callbacks::section(const char*, size_t, size_t) { return false; }
bool statement_callbacks::param_label_separator(const char*, size_t) { return false; }
void statement_callbacks::newline(const char*, size_t) {}
void statement_callbacks::end_of_script(const char*, size_t) {}
void statement_callbacks::error(const char*, StatementError, size_t, size_t) {}

} // namespace
