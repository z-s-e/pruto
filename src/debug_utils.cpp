/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pruto/debug_utils.h"

#include <algorithm>

namespace pruto {

using string_list = std::vector<std::string>;

std::string type_stringifier::to_string(host_type_id host_type)
{
    return std::string("host(") + std::to_string(host_type) + ")";
}

std::string type_stringifier::to_string(const type_proc& proc) { return pruto::to_string(proc.id); }

std::string type_stringifier::operator()(const type_info& type)
{
    struct stringifier_visitor {
        std::string operator()(const type_host& t) { return s->to_string(t.id); }
        std::string operator()(const type_boolean&) { return "boolean"; }
        std::string operator()(const type_struct& t)
        {
            string_list l;
            for( const auto& m : t.members )
                l.push_back(m.name + ": " + (*s)(m.type));
            return "struct(" + join_strings(l) + ")";
        }
        std::string operator()(const type_enum& t)
        {
            return "enum(" + join_strings(t.members) + ")";
        }
        std::string operator()(const type_proc& t)
        {
            assert(t.id.is_proc());
            return s->to_string(t);
        }

        type_stringifier* s;
    };
    return std::visit(stringifier_visitor{this}, type);
}

std::string type_stringifier::list_to_string(const type_info* t, size_t count)
{
    std::string s;
    for( size_t i = 0; i < count; ++i )
        s += ((i != 0) ? ", " : "") + (*this)(t[i]);
    return s;
}

std::string data_stringifier::to_string(const virtual_machine::data_stack_entry*, host_type_id host_type)
{
    return std::string("(data of host type ") + std::to_string(host_type) + ")";
}

std::string data_stringifier::operator()(const virtual_machine::data_stack_entry* data,
                                         const type_info& type,
                                         code_generator::host_integration* host)
{
    struct stringifier_visitor {
        std::string operator()(const type_host& t) { return s->to_string(data, t.id); }
        std::string operator()(const type_boolean&) { return data->u == 1 ? "true" : "false"; }
        std::string operator()(const type_struct& t)
        {
            string_list l;
            size_t off = 0;
            for( const auto& m : t.members ) {
                l.push_back(m.name + ": " + (*s)(data + off, m.type, host));
                off += host->type_size(m.type);
            }
            return "{" + join_strings(l) + "}";
        }
        std::string operator()(const type_enum& t) { return std::string(":") + t.members[data->u]; }
        std::string operator()(const type_proc& t) { return pruto::to_string(t.id); }

        data_stringifier* s;
        const virtual_machine::data_stack_entry* data;
        code_generator::host_integration* host;
    };
    return std::visit(stringifier_visitor{this, data, host}, type);
}


struct active_local_statement_visitor {
    void pop()
    {
        while( l.back().id )
            l.pop_back();
        l.pop_back();
    }

    void operator()(const stm_data_def& stm) { l.push_back({stm.id, stm.name}); }
    void operator()(const stm_if&) { l.push_back({}); }
    void operator()(const stm_else_if&) { pop(); l.push_back({}); }
    void operator()(const stm_else&) { pop(); l.push_back({}); }
    void operator()(const stm_switch&) { l.push_back({}); }
    void operator()(const stm_case&) { pop(); l.push_back({}); }
    void operator()(const stm_while&) { l.push_back({}); }
    void operator()(const stm_for& stm) { l.push_back({}); l.push_back({stm.id, stm.name}); }
    void operator()(const stm_end&) { pop(); }

    template<typename T> void operator()(const T&) {}

    std::vector<active_local_data_entry> l;
};

std::vector<active_local_data_entry> ast_proc_statement_find_active_locals(const stm_proc_def& proc, uint32_t statement)
{
    active_local_statement_visitor v;
    if( statement >= proc.statements.size() )
        return {};
    for( size_t i = 0; i < proc.parameters.size(); ++i )
        v.l.push_back({ident::local_const(i), proc.parameters[i]});
    for( uint32_t i = 0; i < statement; ++i )
        std::visit(v, proc.statements[i]);
    v.l.erase(std::remove_if(v.l.begin(), v.l.end(), [](const active_local_data_entry& a) { return !a.id; }), v.l.end());
    return std::move(v.l);
}


static code_generator::instance_location backtrace_location_for_ip(const code_generator& gen, uint32_t ip)
{
    using OpCode = virtual_machine::instruction::OpCode;

    code_generator::instance_location l{{{}, uint32_t(gen.current_ast()->globals.size()), -1}, {}};
    if( ip <= virtual_machine::HostCallIP ) {
        assert(ip == virtual_machine::HostCallIP);
        return l;
    }

    const auto vm = gen.current_vm();
    const auto ast = gen.current_ast();

    int32_t calls_before_ip = -1;

    switch( vm->byte_code[ip].code ) {
    case OpCode::CallProc:
    case OpCode::CallHost:
    case OpCode::CallList:
        calls_before_ip = 0;
        break;
    default: ;
    }

    bool have_ast_pos = false;

    for( --ip; ip > virtual_machine::HostCallIP; --ip ) {
        const auto inst = vm->byte_code[ip];

        if( inst.code == OpCode::StartProc ) {
            if( inst.val != ident::max_index() ) {
                const auto& proc_i = gen.proc_instances()[inst.val];
                l.proc_id = proc_i.id;
                for( uint32_t param = 0; param < l.proc_id.proc_parameter_count(); ++param )
                    l.param_types.push_back(gen.type_list()[size_t(proc_i.local_types[param])]);
            }
            break;
        }

        switch( inst.code ) {
        case OpCode::CallProc:
        case OpCode::CallHost:
        case OpCode::CallList:
            if( !have_ast_pos && calls_before_ip >= 0 )
                ++calls_before_ip;
            break;
        case OpCode::UpdateAstPosition:
            if( !have_ast_pos )
                l.statement = inst.val;
            have_ast_pos = true;
            break;
        default: ;
        }
    }

    if( calls_before_ip >= 0 ) {
        const expression* e = nullptr;
        if( !l.proc_id )
            e = &std::get<stm_data_def>(ast->globals[l.statement]).e;
        else
            e = proc_statement_get_expression(ast_get_global_proc(*ast, l.proc_id).statements[l.statement]);
        assert(e != nullptr);
        for( size_t i = 0; i < e->size(); ++i ) {
            if( std::holds_alternative<exp_call>((*e)[i]) || std::holds_alternative<exp_list>((*e)[i]) ) {
                if( calls_before_ip == 0 ) {
                    l.expression_element = i;
                    break;
                }
                --calls_before_ip;
            }
        }
        assert(l.expression_element >= 0);
    }

    return l;
}

std::vector<code_generator::instance_location> current_backtrace(const code_generator& gen)
{
    assert(gen.current_ast() != nullptr && gen.current_vm() != nullptr);
    const auto vm = gen.current_vm();
    std::vector<code_generator::instance_location> bt;
    if( vm->call_stack.empty() )
        return bt;
    bt.push_back(backtrace_location_for_ip(gen, vm->ip));
    if( vm->call_stack.size() == 1 )
        return bt;
    for( auto it = vm->call_stack.rbegin(), end = vm->call_stack.rend() - 1; it != end; ++it ) {
        if( !std::holds_alternative<virtual_machine::call_stack_entry_call_data>(*it) )
            continue;
        bt.push_back(backtrace_location_for_ip(gen, std::get<virtual_machine::call_stack_entry_call_data>(*it).return_ip));
    }
    return bt;
}

struct expression_location_extract_visitor {
    template<typename T> expression_base operator()(const T& exp) { return {exp.line, exp.pos}; }
};
expression_base exp_element_script_location(const exp_element& element)
{
    return std::visit(expression_location_extract_visitor(), element);
}

struct statement_line_extract_visitor {
    template<typename T> uint32_t operator()(const T& stm) { return stm.line; }
};
uint32_t proc_statement_script_line(const proc_statement& stm)
{
    return std::visit(statement_line_extract_visitor(), stm);
}
uint32_t ast_global_script_line(const std::variant<stm_proc_def, stm_data_def>& global)
{
    return std::visit(statement_line_extract_visitor(), global);
}

uint32_t ast_find_statement_script_line(const script_section_ast& ast, const code_generator::ast_location& loc)
{
    if( !loc.proc_id )
        return ast_global_script_line(ast.globals[loc.statement]);
    return proc_statement_script_line(ast_get_global_proc(ast, loc.proc_id).statements[loc.statement]);
}

code_generator::ast_location ast_find_statement(const script_section_ast& ast, uint32_t line)
{
    const uint32_t N = ast.globals.size();
    const code_generator::ast_location no_match{{}, N, -1};

    uint32_t global = 0;
    for( ; global < N; ++global ) {
        const auto l = ast_global_script_line(ast.globals[global]);
        if( l == line )
            return {{}, global, -1};
        if( l > line )
            break;
    }
    if( global == 0 )
        return no_match;
    --global;

    if( std::holds_alternative<stm_proc_def>(ast.globals[global]) ) {
        const auto& def = std::get<stm_proc_def>(ast.globals[global]);
        for( uint32_t i = 0; i < def.statements.size(); ++i ) {
            if( proc_statement_script_line(def.statements[i]) == line )
                return {def.id, i, -1};
        }
    }
    return no_match;
}

const exp_element& ast_find_expression_element(const script_section_ast& ast, const code_generator::ast_location& loc)
{
    assert(loc.expression_element >= 0);
    if( !loc.proc_id ) {
        assert(loc.statement < ast.globals.size());
        const auto& data_def = std::get<stm_data_def>(ast.globals[loc.statement]);
        assert(size_t(loc.expression_element) < data_def.e.size());
        return data_def.e[size_t(loc.expression_element)];
    }

    const auto& proc_def = ast_get_global_proc(ast, loc.proc_id);
    assert(loc.statement < proc_def.statements.size());

    auto exp = proc_statement_get_expression(proc_def.statements[loc.statement]);
    assert(exp != nullptr && size_t(loc.expression_element) < exp->size());
    return (*exp)[size_t(loc.expression_element)];
}

const char* find_line(const char* text, size_t line)
{
    size_t i = 0;
    while( (i < line) && (*text != 0) ) {
        if( *text == '\n' )
            ++i;
        ++text;
    }
    return text;
}

std::string join_strings(const std::vector<std::string>& list)
{
    if( list.empty() )
        return {};
    auto s = list.front();
    for( auto it = list.begin() + 1, end = list.end(); it != end; ++it )
        s += ", " + *it;
    return s;
}

std::string strlit_encode_singleline(const char* text)
{
    std::string res;
    for( ; *text != 0; ++text ) {
        const auto c = *text;
        if( c == '\\' )
            res.append("\\\\");
        else if( c == '\n' )
            res.append("\\n");
        else if( c == '"' )
            res.append("\\\"");
        else
            res.push_back(c);
    }
    return res;
}

struct operator_id_find_visitor {
    bool operator()(const op_binary_and_left_unary& op) { return op.b.id == id || op.u.id == id; }
    template<typename T> bool operator()(const T& op) { return op.id == id; }
    ident id;
};

std::string host_id_find_name(ident id, const host_identifier_table& host_entries, const symop_table& operator_table)
{
    for( const auto& e : host_entries ) {
        if( e.second == id )
            return e.first;
    }
    for( const auto& op : operator_table ) {
        if( std::visit(operator_id_find_visitor{id}, op.second) )
            return to_string(op.first);
    }
    return {};
}


std::string to_string(ident id)
{
    std::string r;
    if( !id )
        return "invalid_id";
    switch( id.scope() ) {
    case ident::Local:
        r += "local_";
        break;
    case ident::Global:
        r += "global_";
        break;
    case ident::Host:
        r += "host_";
        break;
    }
    if( id.is_proc() )
        r += (id.proc_has_retval() ? "proc_ret" : "proc");
    else
        r += (id.data_is_var() ? "var" : "const");
    r += "(" + std::to_string(id.index());
    if( id.is_proc() )
        r += ", " + std::to_string(id.proc_parameter_count());
    return r + ")";
}


#define ES(name) case name: return #name

std::string to_string(ExpressionError e)
{
    switch( e ) {
    ES(ExpressionError::UnexpectedCharacter);
    ES(ExpressionError::UnexpectedKeyword);
    ES(ExpressionError::UnexpectedNonEnumColon);
    ES(ExpressionError::UnexpectedAssignSymbol);
    ES(ExpressionError::MissingOperand);
    ES(ExpressionError::MissingOperatorBetweenOperands);
    ES(ExpressionError::NestingLimitExceeded);
    }
    assert(false);
    return {};
}
std::string to_string(StatementError e)
{
    switch( e ) {
    ES(StatementError::UnexpectedCharacter);
    ES(StatementError::UnexpectedKeyword);
    ES(StatementError::KeywordOutsideProc);
    ES(StatementError::CallOutsideProc);
    ES(StatementError::ReassignOutsideProc);
    ES(StatementError::NestedProcDefinition);
    ES(StatementError::ElseOutsideConditional);
    ES(StatementError::UnexpectedWordAfterElse);
    ES(StatementError::BreakOutsideLoop);
    ES(StatementError::ContinueOutsideLoop);
    ES(StatementError::CaseOutsideSwitch);
    ES(StatementError::NeedCaseAfterSwitch);
    ES(StatementError::NeedInAfterForIdent);
    ES(StatementError::MissingSpaceAfterIf);
    ES(StatementError::MissingSpaceAfterSwitch);
    ES(StatementError::MissingSpaceAfterCase);
    ES(StatementError::MissingSpaceAfterWhile);
    ES(StatementError::MissingSpaceAfterForIn);
    ES(StatementError::MissingSpaceAfterReturn);
    ES(StatementError::SectionInsideProc);
    ES(StatementError::WhitespaceBeforeSection);
    }
    assert(false);
    return {};
}
std::string to_string(AstError e)
{
    switch( e ) {
    ES(AstError::UnknownName);
    ES(AstError::RecursiveRef);
    ES(AstError::InvalidRedefine);
    ES(AstError::ReassignNotVar);
    ES(AstError::UndefinedOperator);
    ES(AstError::LeftUnaryAfterOperand);
    ES(AstError::RightUnaryBeforeOperand);
    ES(AstError::MissingBinaryOperatorBetweenOperands);
    ES(AstError::BinaryOperatorMissingLeftOperand);
    ES(AstError::BinaryOperatorMissingRightOperand);
    ES(AstError::NameIsNotProc);
    ES(AstError::ProcInEnumRef);
    ES(AstError::ProcHasNoValue);
    ES(AstError::ProcHasValue);
    ES(AstError::ProcTooManyParameter);
    ES(AstError::ProcArgParameterCountMismatch);
    ES(AstError::ProcReturnValueMismatch);
    ES(AstError::RepeatedStructMemberName);
    ES(AstError::RepeatedCaseLabel);
    ES(AstError::BlockNestingLimitExceeded);
    }
    assert(false);
    return {};
}
std::string to_string(LiteralError e)
{
    switch( e ) {
    ES(LiteralError::HostDisabledNumLiterals);
    ES(LiteralError::HostDisabledStrLiterals);
    ES(LiteralError::HostRejectedNumLiteral);
    ES(LiteralError::HostRejectedStrLiteral);
    }
    assert(false);
    return {};
}
std::string to_string(TypeCheckError e)
{
    switch( e ) {
    ES(TypeCheckError::InvalidMember);
    ES(TypeCheckError::RefNotAStruct);
    ES(TypeCheckError::ListElementTypeMismatch);
    ES(TypeCheckError::ConditionalNotBoolean);
    ES(TypeCheckError::SwitchNotEnum);
    ES(TypeCheckError::MissingCaseOrElse);
    ES(TypeCheckError::ReassignTypeMismatch);
    ES(TypeCheckError::ReturnTypeMismatch);
    ES(TypeCheckError::HostRejetedProc);
    ES(TypeCheckError::HostRejetedList);
    ES(TypeCheckError::HostRejetedListOp);
    ES(TypeCheckError::HostRejetedIter);
    }
    assert(false);
    return {};
}
std::string to_string(code_generator::LimitError e)
{
    switch( e ) {
    ES(code_generator::LimitError::TypeExceedsSizeLimit);
    }
    assert(false);
    return {};
}

std::string to_string(SymOp s)
{
#define SYM(Op, Str) case Op: return Str
    switch( s ) {
    SYM(SymOp::And, "&");
    SYM(SymOp::Bar, "|");
    SYM(SymOp::Plus, "+");
    SYM(SymOp::Minus, "-");
    SYM(SymOp::Asterisk, "*");
    SYM(SymOp::Slash, "/");
    SYM(SymOp::Backslash, "\\");
    SYM(SymOp::Tilde, "~");
    SYM(SymOp::Percent, "%");
    SYM(SymOp::Hash, "#");
    SYM(SymOp::Caret, "^");
    SYM(SymOp::Apostrophe, "'");
    SYM(SymOp::Less, "<");
    SYM(SymOp::Greater, ">");
    SYM(SymOp::Exclamation, "!");
    SYM(SymOp::AndAnd, "&&");
    SYM(SymOp::AndBar, "&|");
    SYM(SymOp::AndPlus, "&+");
    SYM(SymOp::AndMinus, "&-");
    SYM(SymOp::AndAsterisk, "&*");
    SYM(SymOp::AndSlash, "&/");
    SYM(SymOp::AndBackslash, "&\\");
    SYM(SymOp::AndTilde, "&~");
    SYM(SymOp::AndPercent, "&%");
    SYM(SymOp::AndHash, "&#");
    SYM(SymOp::AndCaret, "&^");
    SYM(SymOp::AndApostrophe, "&'");
    SYM(SymOp::AndLess, "&<");
    SYM(SymOp::AndGreater, "&>");
    SYM(SymOp::AndExclamation, "&!");
    SYM(SymOp::BarAnd, "|&");
    SYM(SymOp::BarBar, "||");
    SYM(SymOp::BarPlus, "|+");
    SYM(SymOp::BarMinus, "|-");
    SYM(SymOp::BarAsterisk, "|*");
    SYM(SymOp::BarSlash, "|/");
    SYM(SymOp::BarBackslash, "|\\");
    SYM(SymOp::BarTilde, "|~");
    SYM(SymOp::BarPercent, "|%");
    SYM(SymOp::BarHash, "|#");
    SYM(SymOp::BarCaret, "|^");
    SYM(SymOp::BarApostrophe, "|'");
    SYM(SymOp::BarLess, "|<");
    SYM(SymOp::BarGreater, "|>");
    SYM(SymOp::BarExclamation, "|!");
    SYM(SymOp::LessEqual, "<=");
    SYM(SymOp::GreaterEqual, ">=");
    SYM(SymOp::NotEqual, "!=");
    SYM(SymOp::Equal, "==");
    }
#undef SYM
    assert(false);
    return {};
}
std::string to_string(virtual_machine::instruction::OpCode code)
{
    using OpCode = virtual_machine::instruction::OpCode;
    switch( code ) {
    ES(OpCode::Invalid);
    ES(OpCode::StartProc);
    ES(OpCode::CallProc);
    ES(OpCode::CallHost);
    ES(OpCode::PushIndex);
    ES(OpCode::Pop);
    ES(OpCode::Jump);
    ES(OpCode::JumpIfZeroPop);
    ES(OpCode::Return);
    ES(OpCode::PushCopy);
    ES(OpCode::PushCopyGlobal);
    ES(OpCode::PushCopyHost);
    ES(OpCode::PopMove);
    ES(OpCode::PopMoveGlobal);
    ES(OpCode::PopMoveHost);
    ES(OpCode::CallList);
    ES(OpCode::SizeArg);
    ES(OpCode::PushForLoop);
    ES(OpCode::AdvanceForLoopOrJump);
    ES(OpCode::PopForLoop);
    ES(OpCode::JumpTable);
    ES(OpCode::JumpTableEntry);
    ES(OpCode::IncrementGlobalDataInitCounter);
    ES(OpCode::UpdateAstPosition);
    ES(OpCode::MissingReturnError);
    }
    assert(false);
    return {};
}

#undef ES


std::string to_string(const expression& exp)
{
    struct expression_to_string_visitor {
        void operator()(const exp_bool_lit& e) { stack.emplace_back(e.val ? "true" : "false"); }
        void operator()(const exp_num_lit& e) { stack.emplace_back(e.val); }
        void operator()(const exp_string_lit& e)
        {
            stack.emplace_back(std::string("\"") + strlit_encode_singleline(e.val.data()) + "\"");
        }
        void operator()(const exp_ref& e)
        {
            std::string s = to_string(e.id);
            for( const auto& sub : e.sub ) {
                s.push_back('.');
                s.append(sub);
            }
            stack.emplace_back(std::move(s));
        }
        void operator()(const exp_enum& e)
        {
            auto s = std::string(":") + to_string(e.id);
            for( auto it = e.sub_mem.cbegin(), end = e.sub_mem.cend(); it != end; ++it )
            {
                s.push_back((it + 1) == end ? ':' : '.');
                s.append(*it);
            }
            stack.emplace_back(std::move(s));
        }
        void operator()(const exp_call& e)
        {
            string_list l;
            const auto arg_offset = stack.size() - e.id.proc_parameter_count();
            for( auto i = arg_offset; i < stack.size(); ++i )
                l.push_back(std::move(stack[i]));
            stack.resize(arg_offset);
            stack.emplace_back("call:" + to_string(e.id) + "(" + join_strings(l) + ")");
        }
        void operator()(const exp_list& e)
        {
            auto s = std::string("[");
            string_list l;
            const auto arg_offset = stack.size() - e.count;
            for( auto i = arg_offset; i < stack.size(); ++i )
                l.push_back(std::move(stack[i]));
            stack.resize(arg_offset);
            s += join_strings(l) + "]";
            if( e.is_operator )
                stack.back().append(std::move(s));
            else
                stack.emplace_back(std::move(s));
        }
        void operator()(const exp_struct& e)
        {
            string_list l;
            const auto arg_offset = stack.size() - e.members.size();
            for( size_t i = 0; i < e.members.size(); ++i )
                l.push_back(e.members[i] + ": " + stack[arg_offset + i]);
            stack.resize(arg_offset);
            stack.emplace_back("{" + join_strings(l) + "}");
        }

        std::vector<std::string> stack;
    } v;

    for( const auto& elm : exp )
        std::visit(v, elm);

    if( v.stack.size() != 1 )
        return "invalid expression";
    return v.stack.front();
}

std::string to_string(const stm_data_def& stm)
{
    return to_string(stm.id) + " " + stm.name + " = " + to_string(stm.e);
}

std::string to_string(const stm_reassign& stm)
{
    std::string s = to_string(stm.id);
    for( const auto& sub : stm.sub ) {
        s.push_back('.');
        s.append(sub);
    }
    return s + " := " + to_string(stm.e);
}

std::string to_string(const stm_call& stm) { return to_string(stm.e); }
std::string to_string(const stm_return&) { return "return"; }
std::string to_string(const stm_return_value& stm) { return std::string("return ") + to_string(stm.e); }
std::string to_string(const stm_break&) { return "break"; }
std::string to_string(const stm_continue&) { return "continue"; }
std::string to_string(const stm_if& stm) { return std::string("if ") + to_string(stm.e); }
std::string to_string(const stm_else_if& stm) { return std::string("else if ") + to_string(stm.e); }
std::string to_string(const stm_else&) { return "else"; }
std::string to_string(const stm_switch& stm) { return std::string("switch ") + to_string(stm.e); }

std::string to_string(const stm_case& stm)
{
    string_list l;
    for( const auto& c : stm.cases )
        l.push_back(":" + c);
    return "case [" + join_strings(l) + "]";
}

std::string to_string(const stm_while& stm) { return std::string("while ") + to_string(stm.e); }

std::string to_string(const stm_for& stm)
{
    return std::string("for ") + to_string(stm.id) + " " + stm.name + " in " + to_string(stm.e);
}

std::string to_string(const stm_end&) { return "end"; }

struct proc_statement_visitor {
    template<typename T>
    std::string operator()(const T& stm) { return to_string(stm); }
};

std::string to_string(const proc_statement& stm) { return std::visit(proc_statement_visitor(), stm); }

std::string to_string(const stm_proc_def& p)
{
    auto s = "proc " + to_string(p.id) + " " + p.name + "(" + join_strings(p.parameters) + ")\n";
    for( const auto& stm : p.statements )
        s += to_string(stm) + "\n";
    return s;
}

std::string to_string(const script_section_ast& section)
{
    std::string s;
    if( !section.name.empty() )
        s = std::string("$ ") + section.name + "\n";
    for( const auto& g : section.globals ) {
        if( g.index() == 0 )
            s += to_string(std::get<stm_proc_def>(g));
        else
            s += to_string(std::get<stm_data_def>(g)) + "\n";
    }
    return s;
}

std::string to_string(const ast_result_error& e)
{
    struct ast_error_string_visitor {
        std::string prefix() { return std::to_string(error.pos) + ":" + std::to_string(error.size) + ":"; }

        std::string operator()(AstError val) { return prefix() + to_string(val); }
        std::string operator()(StatementError val) { return prefix() + to_string(val); }
        std::string operator()(ExpressionError val) { return prefix() + to_string(val); }

        const ast_result_error& error;
    };
    return std::visit(ast_error_string_visitor{e}, e.val);
}

std::string to_string(virtual_machine::instruction inst)
{
    return "{" + to_string(inst.code) + ", " + std::to_string(inst.val) + "}";
}

std::string to_string(const std::vector<virtual_machine::instruction>& bytecode)
{
    std::string r("{\n");
    bool first = true;
    for( auto inst : bytecode ) {
        if( !first )
            r += ",\n";
        first = false;
        r += to_string(inst);
    }
    return r + (first ? "}" : "\n}");
}

std::string to_string(virtual_machine::step_result step)
{
    switch( step.result ) {
    case virtual_machine::step_result::Stop: return "Stop";
    case virtual_machine::step_result::Step: return "Step " + std::to_string(step.argument);
    case virtual_machine::step_result::Call: return "Call " + std::to_string(step.argument);
    case virtual_machine::step_result::Return: return "Return";
    case virtual_machine::step_result::WaitingForHost: return "WaitingForHost";
    case virtual_machine::step_result::ErrorHost: return "ErrorHost";
    case virtual_machine::step_result::ErrorMissingReturn: return "ErrorMissingReturn";
    }
    assert(false);
    return {};
}

} // namespace
