/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pruto/code_generator.h"

namespace pruto {

using stack_entry_count = virtual_machine::stack_entry_count;

static constexpr stack_entry_count InvalidTypeSize = code_generator::MaximumTypeSize + 1;

static stack_entry_count type_size_verified(code_generator::host_integration* h, const type_info& t)
{
    struct size_visitor {
        stack_entry_count operator()(const type_host& t)
        {
            assert(t.id != InvalidHostType);
            return host->type_size(t.id);
        }
        stack_entry_count operator()(const type_boolean&) { return 1; }
        stack_entry_count operator()(const type_struct& t)
        {
            stack_entry_count c = 0;
            for( const auto& m : t.members ) {
                const auto ms = std::min(type_size_verified(host, m.type), InvalidTypeSize);
                c = std::min(c + ms, InvalidTypeSize);
            }
            return c;
        }
        stack_entry_count operator()(const type_enum&) { return 1; }
        stack_entry_count operator()(const type_proc&) { return 0; }

        code_generator::host_integration* host;
    };
    return std::visit(size_visitor{h}, t);
}

stack_entry_count code_generator::host_integration::type_size(const type_info& t)
{
    if( !type_is_valid(t) )
        return InvalidTypeSize;
    return type_size_verified(this, t);
}

stack_entry_count code_generator::type_size(type_index index) const
{
    if( index == InvalidTypeIndex )
        return InvalidTypeSize;
    assert(index >= 0);
    return type_size_verified(host, types[size_t(index)]);
}


template<typename T>
size_t find_or_make_index(std::vector<T>* list, const T& val)
{
    const auto N = list->size();
    for( size_t i = 0; i < N; ++i ) {
        if( (*list)[i] == val )
            return i;
    }
    list->emplace_back(val);
    return N;
}

static code_generator::type_index find_or_make_type_index(std::vector<type_info>* types, const type_info& t)
{
    if( !type_is_valid(t) )
        return code_generator::InvalidTypeIndex;
    return code_generator::type_index(find_or_make_index(types, t));
}

static constexpr auto MaximumOffset = ident::max_index();

struct type_and_offset {
    code_generator::type_index type = code_generator::InvalidTypeIndex;
    stack_entry_count offset = 0;
};

struct expression_stack_tracker {
    void reset()
    {
        assert(data.size() <= 1);
        data.clear();
        next_offset = 0;
    }

    void push(code_generator::type_index type, stack_entry_count size)
    {
        assert(type >= 0 && size <= code_generator::MaximumTypeSize);
        assert(next_offset + size <= MaximumOffset);
        data.push_back({type, next_offset});
        next_offset += size;
    }

    void pop(size_t count)
    {
        assert(count <= data.size());
        if( count == 0 )
            return;
        const auto N = data.size() - count;
        next_offset = data[N].offset;
        data.resize(N);
    }

    code_generator::type_index type_index(size_t idx) const { return data[idx].type; }
    size_t element_count() const { return data.size(); }
    stack_entry_count size() const { return next_offset; }
    stack_entry_count top_size_sum(size_t count) const
    {
        assert(count <= data.size());
        return count == 0 ? 0 : next_offset - data[data.size() - count].offset;
    }

private:
    std::vector<type_and_offset> data;
    stack_entry_count next_offset = 0;
};

static code_generator::host_instance_index index_with_retval(code_generator::host_integration::instance i, type_info* retval)
{
    *retval = std::move(i.retval_type);
    return i.index;
}

struct code_generator::expression_visitor {
    using OpCode = virtual_machine::instruction::OpCode;

    type_and_offset type_and_offset_for_data(ident id) const
    {
        assert(id.is_data());
        type_and_offset r;
        switch( id.scope() ) {
        case ident::Local:
            r.type = (*local_types)[id.index()];
            r.offset = (*local_offsets)[id.index()];
            break;
        case ident::Global:
            for( const auto& g : gen->global_data_info ) {
                if( g.id == id ) {
                    r.type = g.type;
                    r.offset = g.offset;
                    break;
                }
            }
            break;
        case ident::Host:
            r.type = find_or_make_type_index(&gen->types, gen->host->host_data_type(id));
            break;
        }
        assert(r.type >= 0);
        return r;
    }

    const type_info& type_data(type_index idx) const { return gen->types[size_t(idx)]; }

    template<typename Error>
    bool set_error(Error e)
    {
        assert(!error);
        error.reset(new prepare_result_error);
        error->val = e;
        return false;
    }

    bool set_error_RefNotAStruct(const type_info& actual, size_t sub = 0)
    {
        set_error(TypeCheckError::RefNotAStruct);
        error->mismatch_actual = actual;
        error->sub_index = sub;
        return false;
    }

    bool set_error_InvalidMember(size_t sub, const type_info& expected)
    {
        set_error(TypeCheckError::InvalidMember);
        error->sub_index = sub;
        error->mismatch_expected = expected; // expected is either a struct or enum type with all the valid members
        return false;
    }

    bool set_error_TypeExceedsSizeLimit(const type_info& actual)
    {
        set_error(LimitError::TypeExceedsSizeLimit);
        error->mismatch_actual = actual;
        return false;
    }

    bool struct_sub_info(const type_info& info, const std::vector<std::string>& subs,
                         type_index* sub_type, stack_entry_count* offset)
    {
        assert(subs.size() > 0);
        if( !std::holds_alternative<type_struct>(info) )
            return set_error_RefNotAStruct(info);

        stack_entry_count c = 0;
        const type_struct* s = &std::get<type_struct>(info);
        for( auto it = subs.cbegin(), end = subs.cend(); it != end; ++it ) {
            bool found = false;
            for( const auto& mem : s->members ) {
                if( mem.name == *it ) {
                    if( it + 1 == end ) {
                        *sub_type = find_or_make_type_index(&gen->types, mem.type);
                        *offset = c;
                        return true;
                    }
                    if( !std::holds_alternative<type_struct>(mem.type) )
                        return set_error_RefNotAStruct(mem.type, 1 + size_t(it - subs.cbegin()));
                    s = &std::get<type_struct>(mem.type);
                    found = true;
                    break;
                } else {
                    c += gen->type_size(mem.type);
                    assert(c <= MaximumOffset); // normally c should even be below MaximumTypeSize, but we can
                                                // allow host types to be larger, as long as only small enough
                                                // parts of it are loaded onto the stack
                }
            }
            if( !found )
                return set_error_InvalidMember(1 + size_t(it - subs.cbegin()), *s);
        }
        assert(false);
        return false;
    }

    bool operator()(const exp_bool_lit& e)
    {
        expression_stack.push(BooleanTypeIndex, 1);
        code.emplace_back(OpCode::PushIndex, e.val ? 1 : 0);
        return true;
    }

    bool operator()(const exp_num_lit& e)
    {
        const auto host_num = gen->host_num_type;
        assert(host_num != code_generator::InvalidTypeIndex);
        const auto size = gen->type_size(host_num);
        expression_stack.push(host_num, size);
        code.emplace_back(OpCode::PushCopyGlobal, gen->num_lit_offset[e.val]);
        code.emplace_back(OpCode::SizeArg, size);
        return true;
    }

    bool operator()(const exp_string_lit& e)
    {
        const auto host_str = gen->host_str_type;
        assert(host_str != code_generator::InvalidTypeIndex);
        const auto size = gen->type_size(host_str);
        expression_stack.push(host_str, size);
        code.emplace_back(OpCode::PushCopyGlobal, gen->str_lit_offset[e.val]);
        code.emplace_back(OpCode::SizeArg, size);
        return true;
    }

    bool operator()(const exp_ref& e)
    {
        if( e.id.is_proc() ) {
            if( !e.sub.empty() )
                return set_error_RefNotAStruct(type_proc{e.id});
            expression_stack.push(find_or_make_type_index(&gen->types, type_proc{e.id}), 0);
            return true;
        }

        auto info = type_and_offset_for_data(e.id);

        if( !e.sub.empty() ) {
            stack_entry_count sub_offset;
            if( !struct_sub_info(type_data(info.type), e.sub, &info.type, &sub_offset) )
                return false;
            info.offset += sub_offset;
        }

        const auto size = gen->type_size(info.type);
        if( size > MaximumTypeSize )
            return set_error_TypeExceedsSizeLimit(type_data(info.type));
        expression_stack.push(info.type, size);

        if( size > 0 ) {
            switch( e.id.scope() ) {
            case ident::Local:
                code.emplace_back(OpCode::PushCopy, info.offset);
                break;
            case ident::Global:
                code.emplace_back(OpCode::PushCopyGlobal, info.offset);
                break;
            case ident::Host:
                code.emplace_back(OpCode::PushCopyHost,
                                  find_or_make_index(&gen->vm->host_data, {e.id, info.offset}));
                break;
            }
            code.emplace_back(OpCode::SizeArg, size);
        }
        return true;
    }

    bool operator()(const exp_enum& e)
    {
        if( e.id.is_proc() )
            return set_error_RefNotAStruct(type_proc{e.id});

        const type_struct* s = nullptr;
        {
            const type_info& info = type_data(type_and_offset_for_data(e.id).type);
            if( !std::holds_alternative<type_struct>(info) )
                return set_error_RefNotAStruct(info);
            s = &std::get<type_struct>(info);
        }

        for( auto it = e.sub_mem.cbegin(), end = e.sub_mem.cend() - 1; it != end; ++it ) {
            bool found = false;
            for( const auto& mem : s->members ) {
                if( mem.name == *it ) {
                    if( !std::holds_alternative<type_struct>(mem.type) )
                        return set_error_RefNotAStruct(mem.type, 1 + size_t(it - e.sub_mem.cbegin()));
                    s = &std::get<type_struct>(mem.type);
                    found = true;
                    break;
                }
            }
            if( !found )
                return set_error_InvalidMember(1 + size_t(it - e.sub_mem.cbegin()), *s);
        }

        const auto t = get_enum_and_index(*s, e.sub_mem.back());
        if( t.index == t.e.members.size() )
            return set_error_InvalidMember(e.sub_mem.size(), *s);
        expression_stack.push(find_or_make_type_index(&gen->types, t.e), 1);

        code.emplace_back(OpCode::PushIndex, t.index);
        return true;
    }

    bool operator()(const exp_call& e)
    {
        const auto N = e.id.proc_parameter_count();
        const auto M = expression_stack.element_count();
        assert(M >= N);

        std::vector<type_info> params;
        params.reserve(N);
        for( size_t i = M - N; i < M; ++i )
            params.push_back(type_data(expression_stack.type_index(i)));

        type_info retval;
        stack_entry_count retval_size = InvalidTypeSize;

        if( e.id.is_global() ) {
            auto c = gen->prepare_call(e.id, params.data());
            if( c.error ) {
                error = std::move(c.error);
                return false;
            }
            retval = std::move(c.retval_type);
            retval_size = e.id.proc_has_retval() ? gen->type_size(retval) : 0;
            code.emplace_back(OpCode::CallProc, gen->proc_instance_info[size_t(c.instance)].byte_code_offset);
        } else {
            virtual_machine::host_instance_info info;
            info.index = index_with_retval(gen->host->proc_instance(e.id, params.data()), &retval);
            if( info.index == virtual_machine::InvalidHostInstance )
                return set_error(TypeCheckError::HostRejetedProc);
            retval_size = e.id.proc_has_retval() ? gen->type_size(retval) : 0;
            if( retval_size > MaximumTypeSize )
                return set_error_TypeExceedsSizeLimit(retval);
            info.retval_size = retval_size;
            info.params_size = expression_stack.top_size_sum(N);
            code.emplace_back(OpCode::CallHost, find_or_make_index(&gen->vm->host_instances, info));
        }

        expression_stack.pop(N);
        if( e.id.proc_has_retval() ) {
            assert(type_is_valid(retval) && retval_size < InvalidTypeSize);
            expression_stack.push(find_or_make_type_index(&gen->types, retval), retval_size);
        }

        return true;
    }

    bool operator()(const exp_list& e)
    {
        const auto N = e.count;
        const auto M = expression_stack.element_count();
        assert(M >= (e.is_operator ? N + 1 : N));

        static const type_info empty_list_pseudo_element_type = type_struct{};
        const type_info* element_type = &empty_list_pseudo_element_type;
        if( N > 0 ) {
            auto t = expression_stack.type_index(M - N);
            element_type = &type_data(t);
            for( size_t i = M - N + 1; i < M; ++i ) {
                if( t != expression_stack.type_index(i) ) {
                    set_error(TypeCheckError::ListElementTypeMismatch);
                    error->mismatch_expected = *element_type;
                    error->mismatch_actual = type_data(expression_stack.type_index(i));
                    error->sub_index = i - (M - N);
                    return false;
                }
            }
        }

        type_info retval;
        virtual_machine::host_instance_info info;
        if( e.is_operator ) {
            const type_info& op_type = type_data(expression_stack.type_index(M - N - 1));
            info.params_size = expression_stack.top_size_sum(N + 1);
            info.index = index_with_retval(gen->host->list_operator_instance(op_type, *element_type, N), &retval);
        } else {
            info.params_size = expression_stack.top_size_sum(N);
            info.index = index_with_retval(gen->host->list_instance(*element_type, N), &retval);
        }
        if( info.index == virtual_machine::InvalidHostInstance )
            return set_error(e.is_operator ? TypeCheckError::HostRejetedListOp : TypeCheckError::HostRejetedList);
        assert(type_is_valid(retval));
        info.retval_size = gen->type_size(retval);
        if( info.retval_size > MaximumTypeSize )
            return set_error_TypeExceedsSizeLimit(retval);

        expression_stack.pop(e.is_operator ? N + 1 : N);
        expression_stack.push(find_or_make_type_index(&gen->types, retval), info.retval_size);

        code.emplace_back(OpCode::CallList, find_or_make_index(&gen->vm->host_instances, info));
        code.emplace_back(OpCode::SizeArg, N);
        return true;
    }

    bool operator()(const exp_struct& e)
    {
        const auto N = e.members.size();
        const auto M = expression_stack.element_count();
        assert(M >= N);

        type_struct s;
        s.members.reserve(N);
        for( size_t i = 0; i < N; ++i )
            s.members.push_back({e.members[i], type_data(expression_stack.type_index(M - N + i))});

        const auto size = expression_stack.top_size_sum(N);
        if( size > MaximumTypeSize )
            return set_error_TypeExceedsSizeLimit(s);

        expression_stack.pop(N);
        expression_stack.push(find_or_make_type_index(&gen->types, s), size);
        return true;
    }

    code_generator* gen = nullptr;
    const std::vector<type_index>* local_types = nullptr;
    const std::vector<stack_entry_count>* local_offsets = nullptr;
    expression_stack_tracker expression_stack;
    std::vector<virtual_machine::instruction> code;
    std::unique_ptr<prepare_result_error> error;
};

struct block_info_entry {
    enum class Type : uint8_t {
        BlockStackOffset,
        ForLoopBackAstLocation,
        LoopBackLocation,
        ElseJump,
        SwitchTableLocation,
        SwitchEnumType,
        ExitJump
    };
    Type type;
    uint32_t val : 24;

    block_info_entry(Type t, size_t v) : type(t), val(v) { assert(v <= MaximumOffset); }
};

struct code_generator::proc_statement_visitor {
    using OpCode = virtual_machine::instruction::OpCode;

    proc_statement_visitor(code_generator* codegen, size_t param_count, const type_info* params)
    {
        ev.gen = codegen;
        ev.local_types = &local_types;
        ev.local_offsets = &local_offsets;

        block_stack.reserve(16);
        block_stack.emplace_back(block_info_entry::Type::BlockStackOffset, current_offset);

        local_types.reserve(param_count);
        local_offsets.reserve(param_count);
        for( size_t i = 0; i < param_count; ++i ) {
            const auto& p = params[i];
            local_types.push_back(find_or_make_type_index(&codegen->types, p));
            local_offsets.push_back(current_offset);
            auto s = codegen->type_size(p);
            assert(s <= MaximumTypeSize);
            current_offset += s;
        }

        code().reserve(4);
        code().emplace_back(OpCode::StartProc, MaximumOffset - 1);
        code().emplace_back(OpCode::SizeArg, current_offset);
    }

    std::vector<virtual_machine::instruction>& code() { return ev.code; }
    const type_info& type_data(type_index idx) const { return ev.gen->types[size_t(idx)]; }

    bool process_expression(const expression& e)
    {
        ev.expression_stack.reset();
        for( size_t i = 0, end = e.size(); i != end; ++i ) {
            if( !std::visit(ev, e[i]) ) {
                error_expression_element = i;
                return false;
            }
        }
        assert(ev.expression_stack.element_count() <= 1);
        return true;
    }

    bool process_conditional(const expression& e)
    {
        if( !process_expression(e) )
            return false;

        if( ev.expression_stack.type_index(0) != BooleanTypeIndex ) {
            ev.set_error(TypeCheckError::ConditionalNotBoolean);
            ev.error->mismatch_actual = type_data(ev.expression_stack.type_index(0));
            return false;
        }
        return true;
    }

    stack_entry_count code_add_data_stack_reset(stack_entry_count offset)
    {
        assert(offset <= current_offset);
        const stack_entry_count count = current_offset - offset;
        if( count > 0 )
            code().emplace_back(OpCode::Pop, count);
        return offset;
    }

    bool operator()(const stm_data_def& stm)
    {
        if( !process_expression(stm.e) )
            return false;
        local_types.push_back(ev.expression_stack.type_index(0));
        local_offsets.push_back(current_offset);
        current_offset += ev.expression_stack.size();
        assert(current_offset <= MaximumOffset);
        return true;
    }

    bool operator()(const stm_reassign& stm)
    {
        auto info = ev.type_and_offset_for_data(stm.id);

        if( !stm.sub.empty() ) {
            stack_entry_count sub_offset;
            if( !ev.struct_sub_info(type_data(info.type), stm.sub, &info.type, &sub_offset) )
                return false;
            info.offset += sub_offset;
        }

        if( !process_expression(stm.e) )
            return false;

        if( ev.expression_stack.type_index(0) != info.type ) {
            ev.set_error(TypeCheckError::ReassignTypeMismatch);
            ev.error->mismatch_actual = type_data(ev.expression_stack.type_index(0));
            ev.error->mismatch_expected = type_data(info.type);
            return false;
        }

        const auto size = ev.expression_stack.size();
        if( size > 0 ) {
            switch( stm.id.scope() ) {
            case ident::Local:
                code().emplace_back(OpCode::PopMove, info.offset);
                break;
            case ident::Global:
                code().emplace_back(OpCode::PopMoveGlobal, info.offset);
                break;
            case ident::Host:
                code().emplace_back(OpCode::PopMoveHost,
                                  find_or_make_index(&ev.gen->vm->host_data, {stm.id, info.offset}));
                break;
            }
            code().emplace_back(OpCode::SizeArg, size);
        }
        return true;
    }

    bool operator()(const stm_call& stm)
    {
        return process_expression(stm.e);
    }

    bool operator()(const stm_return&)
    {
        if( for_loops > 0 )
            code().emplace_back(OpCode::PopForLoop, for_loops);
        code().emplace_back(OpCode::Return, 0);
        return true;
    }

    bool operator()(const stm_return_value& stm)
    {
        if( !process_expression(stm.e) )
            return false;

        if( retval_type == InvalidTypeIndex ) {
            retval_type = ev.expression_stack.type_index(0);
        } else if( retval_type != ev.expression_stack.type_index(0) ) {
            ev.set_error(TypeCheckError::ReturnTypeMismatch);
            ev.error->mismatch_actual = type_data(ev.expression_stack.type_index(0));
            ev.error->mismatch_expected = type_data(retval_type);
            return false;
        }

        if( for_loops > 0 )
            code().emplace_back(OpCode::PopForLoop, for_loops);
        code().emplace_back(OpCode::Return, ev.expression_stack.size());
        return true;
    }

    bool operator()(const stm_if& stm)
    {
        if( !process_conditional(stm.e) )
            return false;

        block_stack.emplace_back(block_info_entry::Type::ElseJump, code().size());
        code().emplace_back(OpCode::JumpIfZeroPop, 0);

        block_stack.emplace_back(block_info_entry::Type::BlockStackOffset, current_offset);
        return true;
    }

    bool operator()(const stm_else_if& stm)
    {
        auto it = block_stack.end() - 1;
        assert(it->type == block_info_entry::Type::BlockStackOffset);

        const auto tmp_ast_pos = code().back();
        assert(tmp_ast_pos.code == OpCode::UpdateAstPosition);
        code().pop_back(); // add previous block ending code before the ast update
        current_offset = code_add_data_stack_reset(it->val);

        assert((it - 1)->type == block_info_entry::Type::ElseJump);
        it = block_stack.insert(it - 1, {block_info_entry::Type::ExitJump, code().size()}) + 1;
        assert(it->type == block_info_entry::Type::ElseJump);
        code().emplace_back(OpCode::Jump, 0);

        assert(code()[it->val].code == OpCode::JumpIfZeroPop);
        code()[it->val] = {OpCode::JumpIfZeroPop, code().size()};
        code().push_back(tmp_ast_pos);

        if( !process_conditional(stm.e) )
            return false;

        *it = {block_info_entry::Type::ElseJump, code().size()};
        code().emplace_back(OpCode::JumpIfZeroPop, 0);

        return true;
    }

    bool operator()(const stm_else&)
    {
        auto it = block_stack.end() - 1;

        assert(it->type == block_info_entry::Type::BlockStackOffset);
        assert(code().back().code == OpCode::UpdateAstPosition);
        code().pop_back(); // "else" does not need an UpdateAstPosition instruction
        current_offset = code_add_data_stack_reset(it->val);

        const block_info_entry exit_jump(block_info_entry::Type::ExitJump, code().size());
        code().emplace_back(OpCode::Jump, 0);

        --it;
        if( it->type == block_info_entry::Type::ElseJump ) {
            assert(code()[it->val].code == OpCode::JumpIfZeroPop);
            code()[it->val] = {OpCode::JumpIfZeroPop, code().size()};
            *it = exit_jump;
        } else {
            assert(it->type == block_info_entry::Type::SwitchTableLocation);
            const auto else_dst = code().size();
            auto* table = &(code()[it->val]);
            assert(table->code == OpCode::JumpTable);
            const size_t count = table->val;
            ++table;
            for( size_t i = 0; i < count; ++i ) {
                assert(table[i].code == OpCode::JumpTableEntry);
                if( table[i].val == 0 )
                    table[i] = {OpCode::JumpTableEntry, else_dst};
            }
            assert((it - 1)->type == block_info_entry::Type::SwitchEnumType);
            *(it - 1) = exit_jump;
            block_stack.erase(it);
        }

        return true;
    }

    bool operator()(const stm_switch& stm)
    {
        if( !process_expression(stm.e) )
            return false;

        size_t mem_count = 0;
        {
            const type_index ti = ev.expression_stack.type_index(0);
            const auto& t = type_data(ti);
            if( !std::holds_alternative<type_enum>(t) ) {
                ev.set_error(TypeCheckError::SwitchNotEnum);
                ev.error->mismatch_actual = t;
                return false;
            }
            block_stack.emplace_back(block_info_entry::Type::SwitchEnumType, ti);
            mem_count = std::get<type_enum>(t).members.size();
        }
        assert(mem_count > 0);

        block_stack.emplace_back(block_info_entry::Type::SwitchTableLocation, code().size());
        code().emplace_back(OpCode::JumpTable, mem_count);
        for( size_t i = 0; i < mem_count; ++i )
            code().emplace_back(OpCode::JumpTableEntry, 0);
        return true;
    }

    bool operator()(const stm_case& stm)
    {
        assert(code().back().code == OpCode::UpdateAstPosition);
        code().pop_back(); // "case" does not need an UpdateAstPosition instruction

        virtual_machine::instruction* jump_table = nullptr;
        type_index enum_index = InvalidTypeIndex;
        {
            auto it = block_stack.end() - 1;
            if( it->type == block_info_entry::Type::BlockStackOffset ) {
                current_offset = code_add_data_stack_reset(it->val);

                assert((it - 1)->type == block_info_entry::Type::SwitchTableLocation);
                assert((it - 2)->type == block_info_entry::Type::SwitchEnumType);
                it = block_stack.insert(it - 2, {block_info_entry::Type::ExitJump, code().size()}) + 2;
                code().emplace_back(OpCode::Jump, 0);
            } else {
                assert(it->type == block_info_entry::Type::SwitchTableLocation);
                it = block_stack.insert(block_stack.end(),
                                       {block_info_entry::Type::BlockStackOffset, current_offset}) - 1;
            }
            assert(it->type == block_info_entry::Type::SwitchTableLocation);
            jump_table = &(code()[it->val + 1]);
            assert((it - 1)->type == block_info_entry::Type::SwitchEnumType);
            enum_index = (it - 1)->val;
        }

        const type_enum& e = std::get<type_enum>(type_data(enum_index));
        for( size_t c = 0; c < stm.cases.size(); ++c ) {
            bool found = false;
            for( size_t idx = 0; idx < e.members.size(); ++idx ) {
                if( e.members[idx] == stm.cases[c] ) {
                    jump_table[idx] = {OpCode::JumpTableEntry, code().size()};
                    found = true;
                    break;
                }
            }
            if( !found )
                return ev.set_error_InvalidMember(c, e);
        }

        return true;
    }

    bool operator()(const stm_while& stm)
    {
        const size_t loop_back = code().size() - 1;

        if( !process_conditional(stm.e) )
            return false;

        block_stack.emplace_back(block_info_entry::Type::ExitJump, code().size());
        code().emplace_back(OpCode::JumpIfZeroPop, 0);
        block_stack.emplace_back(block_info_entry::Type::LoopBackLocation, loop_back);
        block_stack.emplace_back(block_info_entry::Type::BlockStackOffset, current_offset);
        return true;
    }

    bool operator()(const stm_for& stm)
    {
        assert(code().back().code == OpCode::UpdateAstPosition);
        const uint32_t ast_location = code().back().val;

        if( !process_expression(stm.e) )
            return false;

        size_t element_size = 0;
        {
            auto codegen = ev.gen;
            type_info element_type;
            virtual_machine::host_instance_info info;
            info.index = index_with_retval(codegen->host->iterator_instance(type_data(ev.expression_stack.type_index(0))),
                                           &element_type);
            if( info.index == virtual_machine::InvalidHostInstance )
                return ev.set_error(TypeCheckError::HostRejetedIter);
            assert(type_is_valid(element_type));
            element_size = codegen->type_size(element_type);
            if( element_size > MaximumTypeSize )
                return ev.set_error_TypeExceedsSizeLimit(element_type);
            info.retval_size = element_size;
            info.params_size = ev.expression_stack.size();
            code().emplace_back(OpCode::PushForLoop, find_or_make_index(&codegen->vm->host_instances, info));

            local_types.push_back(find_or_make_type_index(&codegen->types, element_type));
            local_offsets.push_back(current_offset);
        }

        block_stack.emplace_back(block_info_entry::Type::LoopBackLocation, code().size());
        code().emplace_back(OpCode::AdvanceForLoopOrJump, 0);
        block_stack.emplace_back(block_info_entry::Type::ForLoopBackAstLocation, ast_location);
        block_stack.emplace_back(block_info_entry::Type::BlockStackOffset, current_offset);
        current_offset += element_size;
        ++for_loops;

        return true;
    }

    bool operator()(const stm_break&)
    {
        uint32_t loop_stack_offset = 0;

        auto it = block_stack.end() - 1;
        for( ; it->type != block_info_entry::Type::LoopBackLocation; --it ) {
            if( it->type == block_info_entry::Type::BlockStackOffset)
                loop_stack_offset = it->val;
        }
        code_add_data_stack_reset(loop_stack_offset);

        block_stack.insert(it, {block_info_entry::Type::ExitJump, code().size()});
        code().emplace_back(OpCode::Jump, 0);
        return true;
    }

    bool operator()(const stm_continue&)
    {
        uint32_t loop_stack_offset = 0;

        auto it = block_stack.end() - 1;
        for( ; it->type != block_info_entry::Type::LoopBackLocation
               && it->type != block_info_entry::Type::ForLoopBackAstLocation; --it ) {
            if( it->type == block_info_entry::Type::BlockStackOffset)
                loop_stack_offset = it->val;
        }

        code_add_data_stack_reset(loop_stack_offset);
        if( it->type == block_info_entry::Type::ForLoopBackAstLocation ) {
            code().emplace_back(OpCode::UpdateAstPosition, uint32_t(it->val));
            --it;
        }
        assert(it->type == block_info_entry::Type::LoopBackLocation);
        code().emplace_back(OpCode::Jump, uint32_t(it->val));
        return true;
    }

    bool operator()(const stm_end&)
    {
        if( block_stack.size() == 1 ) {
            if( retval_type == InvalidTypeIndex ) {
                code().emplace_back(OpCode::Return, 0);
            } else {
                code().emplace_back(OpCode::MissingReturnError, 0);
                local_types.push_back(retval_type);
            }
            return true;
        }

        assert(block_stack.back().type == block_info_entry::Type::BlockStackOffset);
        {
            const auto off = block_stack.back().val;
            assert(code().back().code == OpCode::UpdateAstPosition);
            code().pop_back(); // inner "end" do not need an UpdateAstPosition instruction
            current_offset = code_add_data_stack_reset(off);
            block_stack.pop_back();
        }

        bool for_loop_cleanup = false;
        switch( block_stack.back().type ) {
        case block_info_entry::Type::ForLoopBackAstLocation:
            code().emplace_back(OpCode::UpdateAstPosition, uint32_t(block_stack.back().val));
            block_stack.pop_back();
            assert(block_stack.back().type == block_info_entry::Type::LoopBackLocation);
            assert(code()[block_stack.back().val].code == OpCode::AdvanceForLoopOrJump);
            code()[block_stack.back().val] = {OpCode::AdvanceForLoopOrJump, code().size() + 1};
            for_loop_cleanup = true;
            --for_loops;
            [[fallthrough]];
        case block_info_entry::Type::LoopBackLocation:
            code().emplace_back(OpCode::Jump, size_t(block_stack.back().val));
            block_stack.pop_back();
            break;
        case block_info_entry::Type::ElseJump:
            assert(code()[block_stack.back().val].code == OpCode::JumpIfZeroPop);
            code()[block_stack.back().val] = {OpCode::JumpIfZeroPop, code().size()};
            block_stack.pop_back();
            break;
        case block_info_entry::Type::SwitchTableLocation:
            { // check for missing cases
                std::vector<std::string> missing_cases;

                const auto* table = &(code()[block_stack.back().val]);
                assert(table->code == OpCode::JumpTable);
                const size_t count = table->val;
                ++table;
                block_stack.pop_back();

                assert(block_stack.back().type == block_info_entry::Type::SwitchEnumType);
                for( size_t i = 0; i < count; ++i ) {
                    assert(table[i].code == OpCode::JumpTableEntry);
                    if( table[i].val == 0 )
                        missing_cases.push_back(std::get<type_enum>(type_data(block_stack.back().val)).members[i]);
                }
                block_stack.pop_back();

                if( !missing_cases.empty() ) {
                    ev.set_error(TypeCheckError::MissingCaseOrElse);
                    ev.error->missing_cases = std::move(missing_cases);
                    return false;
                }
            }
            break;
        case block_info_entry::Type::ExitJump:
            break;
        default: assert(false);
        }

        const auto exit_jmp = code().size();
        assert(exit_jmp <= MaximumOffset);
        while( block_stack.back().type == block_info_entry::Type::ExitJump ) {
            assert(code()[block_stack.back().val].code == OpCode::Jump || code()[block_stack.back().val].code == OpCode::JumpIfZeroPop);
            code()[block_stack.back().val].val = exit_jmp;
            block_stack.pop_back();
        }
        if( for_loop_cleanup )
            code().emplace_back(OpCode::PopForLoop, 1);
        return true;
    }

    std::unique_ptr<prepare_result_error> error(ident proc_id, uint32_t statement)
    {
        assert(ev.error);
        instance_location loc{{proc_id, statement, error_expression_element}, {}};
        if( proc_id ) {
            const size_t count = std::min<size_t>(proc_id.proc_parameter_count(), local_types.size());
            for( size_t i = 0; i < count; ++i )
                loc.param_types.push_back(type_data(local_types[i]));
        }
        ev.error->location.push_back(std::move(loc));
        return std::move(ev.error);
    }

    proc_instance_index commit_code(ident proc_id)
    {
        // an invalid proc_id is used for the global data init code
        auto codegen = ev.gen;
        auto& byte_code = codegen->vm->byte_code;
        const auto N = byte_code.size();
        const auto M = code().size();
        assert(N + M < MaximumOffset);

        const auto instance = proc_id ? codegen->proc_instance_info.size() : ident::max_index();
        assert(code().front().code == OpCode::StartProc);
        code().front().val = instance;

        if( proc_id ) {
            // relocate code
            for( size_t i = 3; i < M; ++i ) {
                switch( code()[i].code ) {
                case OpCode::Jump:
                case OpCode::JumpIfZeroPop:
                case OpCode::AdvanceForLoopOrJump:
                case OpCode::JumpTableEntry:
                    code()[i].val += N;
                    break;
                default: ;
                }
            }
            codegen->proc_instance_info.push_back({proc_id, virtual_machine::instruction_offset(N), std::move(local_types)});
        } else {
            // the global data init code does not need relocation as it cannot have jumps
            codegen->global_init = N;
        }

        byte_code.resize(N + M);
        std::memcpy(byte_code.data() + N, code().data(), M * sizeof(virtual_machine::instruction));

        return instance;
    }

    std::vector<type_index> local_types;
    std::vector<stack_entry_count> local_offsets;

    std::vector<block_info_entry> block_stack;
    stack_entry_count current_offset = 0;
    uint32_t for_loops = 0;
    type_index retval_type = InvalidTypeIndex;
    int32_t error_expression_element = -1;
    expression_visitor ev;
};


struct code_generator::expression_literal_extractor {
    bool operator()(const exp_num_lit& e)
    {
        if( gen->num_lit_offset.find(e.val) != gen->num_lit_offset.end() )
            return true;

        if( gen->host_num_type == InvalidTypeIndex )
            gen->host_num_type = find_or_make_type_index(&gen->types, gen->host->num_type());

        const size_t size = gen->type_size(gen->host_num_type);
        if( size > MaximumTypeSize ) {
            error.val = LiteralError::HostDisabledNumLiterals;
            return false;
        }

        const size_t off = gen->vm->data_stack.size();
        gen->num_lit_offset[e.val] = off;
        gen->vm->data_stack.resize(off + size);
        if( off + size > MaximumOffset || !gen->host->create_num(e.val, &gen->vm->data_stack[off]) ) {
            error.val = LiteralError::HostRejectedNumLiteral;
            return false;
        }
        return true;
    }

    bool operator()(const exp_string_lit& e)
    {
        if( gen->str_lit_offset.find(e.val) != gen->str_lit_offset.end() )
            return true;

        if( gen->host_str_type == InvalidTypeIndex )
            gen->host_str_type = find_or_make_type_index(&gen->types, gen->host->str_type());

        const size_t size = gen->type_size(gen->host_str_type);
        if( size > MaximumTypeSize ) {
            error.val = LiteralError::HostDisabledStrLiterals;
            return false;
        }

        const size_t off = gen->vm->data_stack.size();
        gen->str_lit_offset[e.val] = off;
        gen->vm->data_stack.resize(off + size);
        if( off + size > MaximumOffset || !gen->host->create_str(e.val, &gen->vm->data_stack[off]) ) {
            error.val = LiteralError::HostRejectedStrLiteral;
            return false;
        }
        return true;
    }

    template<typename T> bool operator()(const T&) { return true; }

    code_generator* gen = nullptr;
    literal_error error = {};
};

struct code_generator::proc_statement_literal_extractor {
    proc_statement_literal_extractor(code_generator* codegen) { ele.gen = codegen; }

    bool extract_expression(const expression& exp)
    {
        for( size_t i = 0, end = exp.size(); i < end; ++i ) {
            if( !std::visit(ele, exp[i]) ) {
                ele.error.expression_element = i;
                return false;
            }
        }
        return true;
    }

    bool operator()(const stm_data_def& stm) { return extract_expression(stm.e); }
    bool operator()(const proc_statement& stm)
    {
        if( const auto* e = proc_statement_get_expression(stm); e != nullptr )
            return extract_expression(*e);
        return true;
    }

    expression_literal_extractor ele;
};


code_generator::reset_result code_generator::reset(const script_section_ast* section, virtual_machine* machine, host_integration* integration)
{
    // create literals, type-check & create code to init globals

    ast = section;
    vm = machine;
    host = integration;

    proc_instance_info.clear();
    global_data_info.clear();
    types.clear();
    types.push_back(type_boolean());
    num_lit_offset.clear();
    str_lit_offset.clear();
    host_num_type = InvalidTypeIndex;
    host_str_type = InvalidTypeIndex;
    global_init = 0;

    if( ast == nullptr && vm == nullptr && host == nullptr )
        return {};
    assert(ast != nullptr && host != nullptr && vm != nullptr);

    vm->byte_code.resize(2);
    vm->data_stack.clear();
    vm->call_stack.clear();
    vm->host_instances.clear();
    vm->host_data.clear();
    vm->ip = virtual_machine::StopIP;
    vm->frame_offset = 0;
    vm->next_global_data_init = -1;

    using OpCode = virtual_machine::instruction::OpCode;

    proc_statement_visitor pv(this, 0, nullptr);
    proc_statement_literal_extractor literal_extractor(this);

    const auto& globals = ast->globals;
    for( size_t i = 0, end = globals.size(); i < end; ++i ) {
        const auto& global = globals[i];
        if( std::holds_alternative<stm_data_def>(global) ) {
            const auto& data = std::get<stm_data_def>(global);
            if( !literal_extractor(data) ) {
                literal_extractor.ele.error.statement = i;
                reset(nullptr, nullptr, nullptr);
                return {literal_extractor.ele.error};
            }

            pv.code().emplace_back(OpCode::UpdateAstPosition, i);
            if( !pv.process_expression(data.e) ) {
                reset(nullptr, nullptr, nullptr);
                return {pv.error({}, i)};
            }
            const uint32_t off = vm->data_stack.size();
            const auto size = pv.ev.expression_stack.size();
            if( size > 0 ) {
                vm->data_stack.resize(off + size);
                pv.code().emplace_back(OpCode::PopMoveGlobal, off);
                pv.code().emplace_back(OpCode::SizeArg, size);
            }
            pv.code().emplace_back(OpCode::IncrementGlobalDataInitCounter, 0);

            global_data_info.push_back({data.id, off, pv.ev.expression_stack.type_index(0)});
        } else {
            const auto& proc = std::get<stm_proc_def>(global);
            for( size_t j = 0, stm_end = proc.statements.size(); j < stm_end; ++j ) {
                if( !literal_extractor(proc.statements[j]) ) {
                    literal_extractor.ele.error.proc_id = proc.id;
                    literal_extractor.ele.error.statement = j;
                    reset(nullptr, nullptr, nullptr);
                    return {literal_extractor.ele.error};
                }
            }
        }
    }
    pv.code().emplace_back(OpCode::Return, 0);

    if( !global_data_info.empty() )
        pv.commit_code({});
    else
        vm->next_global_data_init = 0;
    return {};
}

code_generator::prepare_result code_generator::prepare_call(ident proc, const type_info* params)
{
    assert(ast != nullptr && host != nullptr && vm != nullptr);
    code_generator::prepare_result r;

    const auto& statements = ast_get_global_proc(*ast, proc).statements;

    proc_statement_visitor pv(this, proc.proc_parameter_count(), params);

    for( size_t i = 0; i < proc_instance_info.size(); ++i ) {
        const auto& inst = proc_instance_info[i];
        if( inst.id != proc )
            continue;
        bool same = true;
        for( size_t j = 0; j < proc.proc_parameter_count(); ++j ) {
            if( inst.local_types[j] != pv.local_types[j] ) {
                same = false;
                break;
            }
        }
        if( same ) {
            r.instance = i;
            if( proc.proc_has_retval() )
                r.retval_type = types[size_t(inst.local_types.back())];
            return r;
        }
    }

    for( size_t i = 0, end = statements.size(); i < end; ++i ) {
        pv.code().emplace_back(virtual_machine::instruction::OpCode::UpdateAstPosition, i);
        if( !std::visit(pv, statements[i]) ) {
            r.error = pv.error(proc, i);
            return r;
        }
    }

    r.instance = pv.commit_code(proc);
    if( proc.proc_has_retval() )
        r.retval_type = types[size_t(proc_instance_info[size_t(r.instance)].local_types.back())];
    return r;
}

bool code_generator::push_global_init()
{
    assert(ast != nullptr && host != nullptr && vm != nullptr);
    if( vm->next_global_data_init >= 0 )
        return false;
    vm->next_global_data_init = 0;
    vm->push_call(global_init, nullptr, nullptr);
    return true;
}

static stack_entry_count instance_retval_size(const code_generator::proc_instance& inst, const code_generator* gen)
{
    return inst.id.proc_has_retval() ? gen->type_size(inst.local_types.back()) : 0;
}

bool code_generator::push_call(proc_instance_index instance, const data_stack_entry* args, data_stack_entry* retval_storage)
{
    assert(ast != nullptr && host != nullptr && vm != nullptr);
    const auto& p = proc_instance_info[size_t(instance)];
    assert(instance_retval_size(p, this) == 0 || retval_storage != nullptr);

    const size_t uninit_global = size_t(std::max(vm->next_global_data_init, 0));
    if( uninit_global < global_data_info.size() ) {
        // check if all global data before the to be called procedure are initialized
        if( global_data_info[uninit_global].id.index() < p.id.index() )
            return false;
    }
    vm->push_call(p.byte_code_offset, args, retval_storage);
    return true;
}

code_generator::host_integration::instance code_generator::host_integration::proc_instance(ident, const type_info*) { return {}; }
code_generator::host_integration::instance code_generator::host_integration::list_instance(const type_info&, size_t) { return {}; }
code_generator::host_integration::instance code_generator::host_integration::list_operator_instance(const type_info&,
                                                                                                    const type_info&,
                                                                                                    size_t) { return {}; }
code_generator::host_integration::instance code_generator::host_integration::iterator_instance(const type_info&) { return {}; }
type_info code_generator::host_integration::num_type() { return {}; }
bool code_generator::host_integration::create_num(std::string_view, data_stack_entry*) { return false; }
type_info code_generator::host_integration::str_type() { return {}; }
bool code_generator::host_integration::create_str(std::string_view, data_stack_entry*) { return false; }
type_info code_generator::host_integration::host_data_type(ident) { return {}; }
code_generator::stack_entry_count code_generator::host_integration::type_size(host_type_id) { return InvalidTypeSize; }

} // namespace
