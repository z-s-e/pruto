/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pruto/ast.h"

namespace pruto {

static std::vector<std::string> split_ref_sub(const char* line, size_t ref_start, size_t ident_size, size_t ref_size)
{
    std::vector<std::string> r;
    if( ref_size > ident_size ) {
        line += ref_start;
        assert(line[ident_size] == '.');
        size_t next_sub = ident_size + 1;
        for( size_t i = next_sub + 1; i < ref_size - 1; ++i ) {
            if( line[i] == '.' ) {
                r.emplace_back(line + next_sub, i - next_sub);
                next_sub = i + 1;
            }
        }
        r.emplace_back(line + next_sub, ref_size - next_sub);
    }
    return r;
}

static std::string strlit_decode_singleline(const char* pos)
{
    std::string r;
    assert(*pos == '"');
    ++pos;
    while( *pos != '"' ) {
        if( *pos == '\\' ) {
            r.push_back((pos[1] == 'n') ? '\n' : pos[1]);
            pos += 2;
        } else {
            r.push_back(*pos);
            ++pos;
        }
    }
    return r;
}

static std::string strlit_decode_multiline(const char* pos)
{
    std::string r;
    assert(*pos == '"');
    ++pos;
    for( ; *pos != '>'; ++pos ) {
        if( *pos == '"' )
            return r;
    }
    while( true ) {
        assert(*pos == '>');
        ++pos;
        if( *pos != '\n' ) {
            ++pos;
            for( ; *pos != '\n'; ++pos )
                r.push_back(*pos);
        }
        ++pos;
        for( ; *pos != '>'; ++pos ) {
            if( *pos == '"' )
                return r;
        }
        r.push_back('\n');
    }
}

struct statement_expression_extract_visitor {
    const expression* operator()(const stm_data_def& stm) { return &stm.e; }
    const expression* operator()(const stm_reassign& stm) { return &stm.e; }
    const expression* operator()(const stm_call& stm) { return &stm.e; }
    const expression* operator()(const stm_return_value& stm) { return &stm.e; }
    const expression* operator()(const stm_if& stm) { return &stm.e; }
    const expression* operator()(const stm_else_if& stm) { return &stm.e; }
    const expression* operator()(const stm_switch& stm) { return &stm.e; }
    const expression* operator()(const stm_while& stm) { return &stm.e; }
    const expression* operator()(const stm_for& stm) { return &stm.e; }
    template<typename T> const expression* operator()(const T&) { return nullptr; }
};
const expression* proc_statement_get_expression(const proc_statement& stm)
{
    return std::visit(statement_expression_extract_visitor(), stm);
}

const stm_proc_def& ast_get_global_proc(const script_section_ast& ast, ident id)
{
    assert(id.is_proc() && id.is_global());
    assert(std::get<stm_proc_def>(ast.globals[id.index()]).id == id);
    return std::get<stm_proc_def>(ast.globals[id.index()]);
}

ident ast_find_global(const script_section_ast& ast, const char* name)
{
    struct find_global_visitor {
        ident operator()(const stm_proc_def& proc) { return (proc.name == name) ? proc.id : ident(); }
        ident operator()(const stm_data_def& data) { return (data.name == name) ? data.id : ident(); }
        const char* name;
    } v{name};
    for( const auto& g : ast.globals ) {
        if( auto id = std::visit(v, g); id )
            return id;
    }
    return {};
}


// operator stack

struct op_stack_left_unary { exp_call e; };
struct op_stack_binary { exp_call e; uint8_t precedence = 0; };
struct op_stack_group { uint32_t line = 0; uint32_t pos = 0; };
struct op_stack_call { exp_call e; size_t call_args = 0; };
struct op_stack_struct_single { exp_struct e; };

using op_stack_element = std::variant<op_stack_left_unary, op_stack_binary, op_stack_group, op_stack_call,
                                      exp_list, exp_struct, op_stack_struct_single>;


struct ast_parser_cb : public statement_callbacks {
    void set_error_common_data(size_t pos, size_t size)
    {
        assert(!error_ptr);
        error_ptr.reset(new ast_result_error);
        error_ptr->line = ast.end_line;
        error_ptr->line_byte_offset = ast.end_line_byte_offset;
        error_ptr->pos = pos;
        error_ptr->size = size;
        for( const auto& e : op_stack ) {
            if( std::holds_alternative<op_stack_group>(e) ) {
                const auto& group = std::get<op_stack_group>(e);
                error_ptr->exp_nesting.push_back({group.line, group.pos, ExpressionNesting::Group});
            } else if( std::holds_alternative<op_stack_call>(e) ) {
                const auto& call = std::get<op_stack_call>(e);
                error_ptr->exp_nesting.push_back({call.e.line, call.e.pos, ExpressionNesting::Call});
            } else if( std::holds_alternative<exp_list>(e) ) {
                const auto& list = std::get<exp_list>(e);
                error_ptr->exp_nesting.push_back({list.line, list.pos, ExpressionNesting::List});
            } else if( std::holds_alternative<exp_struct>(e) ) {
                const auto& s = std::get<exp_struct>(e);
                error_ptr->exp_nesting.push_back({s.line, s.pos, ExpressionNesting::Struct});
            }
        }
        done = true;
    }

    template<typename Error>
    bool set_error(Error e, size_t pos, size_t size)
    {
        set_error_common_data(pos, size);
        error_ptr->val = e;
        return false;
    }

    bool set_error_call(AstError e, size_t pos, size_t size, exp_call call)
    {
        set_error_common_data(pos, size);
        error_ptr->val = e;
        error_ptr->call = call;
        return false;
    }

    ident lookup(const std::string& name) const
    {
        for( auto block = blocks.rbegin(), first = blocks.rend() - 1; block != first; ++block ) {
            if( auto it = block->data.find(name); it != block->data.end() )
                return it->second;
        }
        return lookup_nonlocal(name);
    }

    ident lookup_nonlocal(const std::string& name) const
    {
        if( auto it = blocks.front().data.find(name); it != blocks.front().data.end() )
            return it->second;
        if( auto it = host_entries->find(name); it != host_entries->end() ) {
            assert(it->second.is_host());
            return it->second;
        }
        return {};
    }

    ident get_callable(const std::string& name, size_t pos)
    {
        auto id = lookup_nonlocal(name);
        if( !id )
            id = lookup(name); // for local proc aliases
        if( !id || !id.is_proc() ) {
            set_error(id ? AstError::NameIsNotProc : AstError::UnknownName, pos, name.size());
            return {};
        }
        if( current_proc && id == current_proc->id ) {
            set_error(AstError::RecursiveRef, pos, name.size());
            return {};
        }
        return id;
    }

    void add_local_name(const std::string& name, ident id)
    {
        assert(id.is_local());
        blocks.back().data[name] = id;
        ++local_def_count;
    }

    bool block_push(BlockNesting nesting, size_t pos, size_t size)
    {
        if( blocks.size() > BlockNestingLimit )
            return set_error(AstError::BlockNestingLimitExceeded, pos, size);
        blocks.emplace_back(nesting);
        return true;
    }

    bool block_next_branch(BlockNesting branch)
    {
        assert(blocks.size() > 1);
        blocks.pop_back();
        blocks.emplace_back(branch);
        return true;
    }

    template<typename Statement>
    Statement create_stm() const
    {
        Statement s;
        s.line = ast.end_line;
        return s;
    }
    template<typename Statement>
    Statement create_stm_with_name(const char* start, size_t len) const
    {
        auto s = create_stm<Statement>();
        s.name = std::string(start, len);
        return s;
    }

    template<typename Statement>
    void proc_add_exp_statement(Statement s)
    {
        current_exp = &std::get<Statement>(current_proc->statements.emplace_back(std::move(s))).e;
    }

    bool add_data_def_statement(const char* line, size_t ident_start, size_t ident_size, bool var)
    {
        auto s = create_stm_with_name<stm_data_def>(line + ident_start, ident_size);
        if( lookup(s.name) )
            return set_error(AstError::InvalidRedefine, ident_start, ident_size);
        if( blocks.back().nesting == BlockNesting::Global ) {
            assert(blocks.size() == 1);
            const auto idx = ast.globals.size();
            s.id = var ? ident::global_var(idx) : ident::global_const(idx);
            blocks.front().data[s.name] = s.id;
            current_exp = &std::get<stm_data_def>(ast.globals.emplace_back(std::move(s))).e;
        } else {
            s.id = var ? ident::local_var(local_def_count) : ident::local_const(local_def_count);
            add_local_name(s.name, s.id);
            proc_add_exp_statement(std::move(s));
        }
        return true;
    }

    bool return_check_retval(bool expected, size_t pos)
    {
        if( !current_proc_has_ret_stm ) {
            current_proc_has_ret_stm = true;
            current_proc_has_retval = expected;
        } else if( current_proc_has_retval != expected ) {
            return set_error(AstError::ProcReturnValueMismatch, pos, 6);
        }
        return true;
    }

    template<typename Exp>
    Exp create_exp(size_t pos) const
    {
        Exp e;
        e.line = ast.end_line;
        e.pos = pos;
        return e;
    }

    template<typename Op>
    void op_stack_transfer_exp()
    {
        current_exp->emplace_back(std::move(std::get<Op>(op_stack.back()).e));
        op_stack.pop_back();
    }

    bool exp_after_operand()
    {
        current_exp_state = ExpAfterOperand;
        while( op_stack.size() > 0 && std::holds_alternative<op_stack_left_unary>(op_stack.back()) )
            op_stack_transfer_exp<op_stack_left_unary>();
        return true;
    }

    bool exp_add_struct_member(const char* line, size_t start, size_t ident_size, size_t ref_size)
    {
        auto& e = std::get<exp_struct>(op_stack.back());
        auto name = std::string(line + start, ident_size);
        for( const auto& m : e.members ) {
            if( m == name )
                return set_error(AstError::RepeatedStructMemberName, start, ident_size);
        }
        e.members.emplace_back(std::move(name));

        auto sub = split_ref_sub(line, start, ident_size, ref_size);
        for( auto it = sub.begin(), end = sub.end(); it != end; ++it ) {
            auto s = create_exp<exp_struct>(start);
            s.members.emplace_back(std::move(*it));
            op_stack.emplace_back(op_stack_struct_single{std::move(s)});
        }
        return true;
    }

    bool finalize_subexpression(size_t pos)
    {
        if( current_exp_state != ExpAfterOperand ) {
            for( auto it = op_stack.crbegin(), end = op_stack.crend(); it != end; ++it ) {
                if( std::holds_alternative<op_stack_binary>(*it) )
                    return set_error_call(AstError::BinaryOperatorMissingRightOperand, pos, 1, std::get<op_stack_binary>(*it).e);
                assert(std::holds_alternative<op_stack_left_unary>(*it));
            }
            assert(false);
        }
        while( op_stack.size() > 0 && std::holds_alternative<op_stack_binary>(op_stack.back()) )
            op_stack_transfer_exp<op_stack_binary>();
        return true;
    }

    bool finalize_call(size_t pos, bool retval)
    {
        if( current_exp_state != ExpStartExpList && !finalize_subexpression(pos) )
            return false;

        auto& c = std::get<op_stack_call>(op_stack.back());
        if( current_exp_state != ExpStartExpList )
            ++c.call_args;
        if( c.call_args != c.e.id.proc_parameter_count() ) {
            set_error_call(AstError::ProcArgParameterCountMismatch, pos, 1, c.e);
            error_ptr->call_args = c.call_args;
            return false;
        }
        if( retval != c.e.id.proc_has_retval() )
            return set_error_call(retval ? AstError::ProcHasNoValue : AstError::ProcHasValue, pos, 1, c.e);

        op_stack_transfer_exp<op_stack_call>();
        return true;
    }

    void finalize_expression(size_t pos)
    {
        if( current_exp == nullptr || !finalize_subexpression(pos) )
            return;
        assert(op_stack.size() == 0);
        current_exp = nullptr;
        current_exp_state = ExpBeforeOperand;
    }

    // expression callbacks

    bool ref(const char* line, size_t start, size_t ident_size, size_t ref_size) override
    {
        if( current_exp_state == ExpAfterOperand )
            return set_error(AstError::MissingBinaryOperatorBetweenOperands, start, ref_size);
        auto e = create_exp<exp_ref>(start);
        e.id = lookup(std::string(line + start, ident_size));
        if( !e.id )
            return set_error(AstError::UnknownName, start, ident_size);
        if( current_proc && e.id == current_proc->id )
            return set_error(AstError::RecursiveRef, start, ident_size);
        e.sub = split_ref_sub(line, start, ident_size, ref_size);
        current_exp->emplace_back(std::move(e));
        return exp_after_operand();
    }
    bool symop(const char* line, size_t pos, size_t size) override
    {
        const auto it = operators->find(pruto::sym_op(line + pos, size));
        if( it == operators->end() )
            return set_error(AstError::UndefinedOperator, pos, size);

        op_binary binary;
        auto e = create_exp<exp_call>(pos);

        if( std::holds_alternative<op_binary>(it->second) ) {
            binary = std::get<op_binary>(it->second);
            e.id = binary.id;
            if( current_exp_state != ExpAfterOperand )
                return set_error_call(AstError::BinaryOperatorMissingLeftOperand, pos, size, e);
        } else if( std::holds_alternative<op_binary_and_left_unary>(it->second) ) {
            if( current_exp_state == ExpAfterOperand ) {
                binary = std::get<op_binary_and_left_unary>(it->second).b;
                e.id = binary.id;
            } else {
                e.id = std::get<op_binary_and_left_unary>(it->second).u.id;
                assert(e.id.is_host() && e.id.is_proc() && e.id.proc_parameter_count() == 1 && e.id.proc_has_retval());
                op_stack.emplace_back(op_stack_left_unary{e});
                return true;
            }
        } else if( std::holds_alternative<op_left_unary>(it->second) ) {
            if( current_exp_state == ExpAfterOperand )
                return set_error(AstError::LeftUnaryAfterOperand, pos, size);
            e.id = std::get<op_left_unary>(it->second).id;
            assert(e.id.is_host() && e.id.is_proc() && e.id.proc_parameter_count() == 1 && e.id.proc_has_retval());
            op_stack.emplace_back(op_stack_left_unary{e});
            return true;
        } else {
            assert(std::holds_alternative<op_right_unary>(it->second));
            if( current_exp_state != ExpAfterOperand )
                return set_error(AstError::RightUnaryBeforeOperand, pos, size);
            e.id = std::get<op_right_unary>(it->second).id;
            assert(e.id.is_host() && e.id.is_proc() && e.id.proc_parameter_count() == 1 && e.id.proc_has_retval());
            current_exp->emplace_back(e);
            return true;
        }

        assert(e.id.is_host() && e.id.is_proc() && e.id.proc_parameter_count() == 2 && e.id.proc_has_retval());
        assert(current_exp_state == ExpAfterOperand);
        while( op_stack.size() > 0 && std::holds_alternative<op_stack_binary>(op_stack.back()) ) {
            const auto& other = std::get<op_stack_binary>(op_stack.back());
            if( other.precedence <= binary.precedence )
                op_stack_transfer_exp<op_stack_binary>();
            else
                break;
        }
        op_stack.emplace_back(op_stack_binary{e, binary.precedence});
        current_exp_state = ExpBeforeOperand;
        return true;
    }
    bool true_lit(const char*, size_t pos) override
    {
        if( current_exp_state == ExpAfterOperand )
            return set_error(AstError::MissingBinaryOperatorBetweenOperands, pos, 4);
        auto e = create_exp<exp_bool_lit>(pos);
        e.val = true;
        current_exp->emplace_back(std::move(e));
        return exp_after_operand();
    }
    bool false_lit(const char*, size_t pos) override
    {
        if( current_exp_state == ExpAfterOperand )
            return set_error(AstError::MissingBinaryOperatorBetweenOperands, pos, 5);
        current_exp->emplace_back(create_exp<exp_bool_lit>(pos));
        return exp_after_operand();
    }
    bool num_lit(const char* line, size_t pos, size_t size) override
    {
        if( current_exp_state == ExpAfterOperand )
            return set_error(AstError::MissingBinaryOperatorBetweenOperands, pos, size);
        auto e = create_exp<exp_num_lit>(pos);
        e.val = std::string(line + pos, size);
        current_exp->emplace_back(std::move(e));
        return exp_after_operand();
    }
    bool str_lit_start(const char* line, size_t pos) override
    {
        if( current_exp_state == ExpAfterOperand )
            return set_error(AstError::MissingBinaryOperatorBetweenOperands, pos, 1);
        current_exp->emplace_back(create_exp<exp_string_lit>(pos));
        current_strlit_start = line + pos;
        return true;
    }
    bool str_lit_end(const char* line, size_t) override
    {
        const bool is_multiline = (current_strlit_start < line);
        std::get<exp_string_lit>(current_exp->back()).val = is_multiline ? strlit_decode_multiline(current_strlit_start)
                                                                         : strlit_decode_singleline(current_strlit_start);
        current_strlit_start = nullptr;
        return exp_after_operand();
    }
    bool enum_ref(const char* line, size_t ref_start, size_t ident_size, size_t ref_size, size_t mem_size) override
    {
        if( current_exp_state == ExpAfterOperand )
            return set_error(AstError::MissingBinaryOperatorBetweenOperands, ref_start, ref_size + mem_size + 1);
        auto e = create_exp<exp_enum>(ref_start);
        e.id = lookup(std::string(line + ref_start, ident_size));
        if( !e.id  )
            return set_error(AstError::UnknownName, ref_start, ident_size);
        if( e.id.is_proc() )
            return set_error(AstError::ProcInEnumRef, ref_start, ident_size);
        e.sub_mem = split_ref_sub(line, ref_start, ident_size, ref_size);
        e.sub_mem.emplace_back(line + ref_start + ref_size + 1, mem_size);
        current_exp->emplace_back(std::move(e));
        return exp_after_operand();
    }
    bool group_start(const char*, size_t pos) override
    {
        if( current_exp_state == ExpAfterOperand )
            return set_error(AstError::MissingBinaryOperatorBetweenOperands, pos, 1);
        op_stack.emplace_back(op_stack_group{ ast.end_line, uint32_t(pos) });
        current_exp_state = ExpBeforeOperand;
        return true;
    }
    bool group_end(const char*, size_t pos) override
    {
        if( !finalize_subexpression(pos) )
            return false;
        assert(std::holds_alternative<op_stack_group>(op_stack.back()));
        op_stack.pop_back();
        return exp_after_operand();
    }
    bool call_start(const char* line, size_t ident_start, size_t ident_size, size_t) override
    {
        if( current_exp_state == ExpAfterOperand )
            return set_error(AstError::MissingBinaryOperatorBetweenOperands, ident_start, ident_size);
        auto e = create_exp<exp_call>(ident_start);
        e.id = get_callable(std::string(line + ident_start, ident_size), ident_start);
        if( !e.id )
            return false;
        op_stack.emplace_back(op_stack_call{std::move(e)});
        current_exp_state = ExpStartExpList;
        return true;
    }
    bool call_end(const char*, size_t pos) override
    {
        if( !finalize_call(pos, true) )
            return false;
        return exp_after_operand();
    }
    bool struct_start(const char*, size_t pos) override
    {
        if( current_exp_state == ExpAfterOperand )
            return set_error(AstError::MissingBinaryOperatorBetweenOperands, pos, 1);
        op_stack.emplace_back(create_exp<exp_struct>(pos));
        current_exp_state = ExpStartExpList;
        return true;
    }
    bool struct_mem(const char* line, size_t start, size_t ident_size, size_t ref_size) override
    {
        if( !exp_add_struct_member(line, start, ident_size, ref_size) )
            return false;
        current_exp->emplace_back(create_exp<exp_struct>(start));
        current_exp_state = ExpAfterOperand;
        return true;
    }
    bool struct_mem_value(const char* line, size_t start, size_t ident_size, size_t ref_size) override
    {
        if( !exp_add_struct_member(line, start, ident_size, ref_size) )
            return false;
        current_exp_state = ExpBeforeOperand;
        return true;
    }
    bool struct_end(const char*, size_t pos) override
    {
        if( current_exp_state != ExpStartExpList && !finalize_subexpression(pos) )
            return false;

        while( std::holds_alternative<op_stack_struct_single>(op_stack.back()) )
            op_stack_transfer_exp<op_stack_struct_single>();

        current_exp->emplace_back(std::move(std::get<exp_struct>(op_stack.back())));
        op_stack.pop_back();
        return exp_after_operand();
    }
    bool list_start(const char*, size_t pos) override
    {
        op_stack.emplace_back(create_exp<exp_list>(pos));
        std::get<exp_list>(op_stack.back()).is_operator = (current_exp_state == ExpAfterOperand);
        current_exp_state = ExpStartExpList;
        return true;
    }
    bool list_end(const char*, size_t pos) override
    {
        if( current_exp_state != ExpStartExpList && !finalize_subexpression(pos) )
            return false;

        auto& e = std::get<exp_list>(op_stack.back());
        if( current_exp_state != ExpStartExpList )
            ++e.count;
        current_exp->emplace_back(e);
        op_stack.pop_back();
        return exp_after_operand();
    }
    bool exp_mem_separator(const char*, size_t pos) override
    {
        if( !finalize_subexpression(pos) )
            return false;

        if( std::holds_alternative<op_stack_call>(op_stack.back()) ) {
            ++std::get<op_stack_call>(op_stack.back()).call_args;
        } else if( std::holds_alternative<exp_list>(op_stack.back()) ) {
            ++std::get<exp_list>(op_stack.back()).count;
        } else {
            while( std::holds_alternative<op_stack_struct_single>(op_stack.back()) )
                op_stack_transfer_exp<op_stack_struct_single>();
            assert(std::holds_alternative<exp_struct>(op_stack.back()));
        }

        current_exp_state = ExpBeforeOperand;
        return true;
    }
    const char* inner_newline(const char* line, size_t pos) override
    {
        ++ast.end_line;
        ast.end_line_byte_offset += pos + 1;
        return line + pos + 1;
    }
    void error(const char*, ExpressionError e, size_t pos, size_t size) override { set_error(e, pos, size); }

    // statement callbacks

    bool proc_declaration(const char* line, size_t, size_t ident_start, size_t ident_size, size_t) override
    {
        auto p = create_stm_with_name<stm_proc_def>(line + ident_start, ident_size);
        if( lookup_nonlocal(p.name) )
            return set_error(AstError::InvalidRedefine, ident_start, ident_size);

        // give the proc a temporary id, so that the name is reserved in the block
        // data - id will be updated according to arg count and return value later
        p.id = ident::global_proc(ast.globals.size(), 0);
        blocks.front().data[p.name] = p.id;
        blocks.emplace_back(BlockNesting::Proc);

        assert(current_proc == nullptr);
        current_proc = &std::get<stm_proc_def>(ast.globals.emplace_back(std::move(p)));
        assert(current_proc_has_ret_stm == false);
        assert(current_proc_has_retval == false);
        assert(local_def_count == 0);
        return true;
    }
    bool proc_declaration_param(const char* line, size_t start, size_t size) override
    {
        if( current_proc->parameters.size() == ident::proc_max_parameters() )
            return set_error(AstError::ProcTooManyParameter, start, size);
        std::string name(line + start, size);
        if( lookup(name) )
            return set_error(AstError::InvalidRedefine, start, size);
        assert(blocks.back().data.size() == current_proc->parameters.size());
        assert(local_def_count == current_proc->parameters.size());
        add_local_name(name, ident::local_const(local_def_count));
        current_proc->parameters.emplace_back(std::move(name));
        return true;
    }
    bool proc_declaration_end(const char*, size_t) override { return true; }
    bool proc_alias(const char* line, size_t, size_t ident_start, size_t ident_size,
                    size_t, size_t alias_start, size_t alias_size) override
    {
        std::string name(line + ident_start, ident_size);
        if( lookup(name) )
            return set_error(AstError::InvalidRedefine, ident_start, ident_size);
        const auto alias_id = get_callable(std::string(line + alias_start, alias_size), alias_start);
        if( !alias_id )
            return false;
        blocks.back().data[std::move(name)] = alias_id;
        return true;
    }
    bool const_def(const char* line, size_t ident_start, size_t ident_size, size_t) override
    {
        return add_data_def_statement(line, ident_start, ident_size, false);
    }
    bool var_def(const char* line, size_t, size_t ident_start, size_t ident_size, size_t) override
    {
        return add_data_def_statement(line, ident_start, ident_size, true);
    }
    bool reassign(const char* line, size_t ref_start, size_t ident_size, size_t ref_size, size_t) override
    {
        const auto id = lookup(std::string(line + ref_start, ident_size));
        if( !id || !id.is_data() || !id.data_is_var() )
            return set_error(id ? AstError::ReassignNotVar : AstError::UnknownName, ref_start, ident_size);
        auto s = create_stm<pruto::stm_reassign>();
        s.id = id;
        s.sub = split_ref_sub(line, ref_start, ident_size, ref_size);
        proc_add_exp_statement(std::move(s));
        return true;
    }
    bool call_stm_start(const char* line, size_t ident_start, size_t ident_size, size_t pos) override
    {
        proc_add_exp_statement(create_stm<pruto::stm_call>());
        assert(current_exp_state == ExpBeforeOperand);
        return call_start(line, ident_start, ident_size, pos);
    }
    bool call_stm_end(const char*, size_t pos) override
    {
        if( !finalize_call(pos, false) )
            return false;
        assert(op_stack.size() == 0);
        current_exp = nullptr;
        current_exp_state = ExpBeforeOperand;
        return true;
    }
    bool return_stm(const char*, size_t pos) override
    {
        current_proc->statements.emplace_back(create_stm<pruto::stm_return>());
        return return_check_retval(false, pos);
    }
    bool return_value_stm(const char*, size_t pos) override
    {
        proc_add_exp_statement(create_stm<pruto::stm_return_value>());
        return return_check_retval(true, pos);
    }
    bool if_stm(const char*, size_t pos) override
    {
        proc_add_exp_statement(create_stm<pruto::stm_if>());
        return block_push(BlockNesting::If, pos, 2);
    }
    bool else_if_stm(const char*, size_t, size_t) override
    {
        proc_add_exp_statement(create_stm<pruto::stm_else_if>());
        assert(blocks.back().nesting == BlockNesting::If);
        return block_next_branch(BlockNesting::If);
    }
    bool else_stm(const char*, size_t) override
    {
        current_proc->statements.emplace_back(create_stm<pruto::stm_else>());
        assert(blocks.back().nesting == pruto::BlockNesting::If || blocks.back().nesting == BlockNesting::SwitchCase);
        if( blocks.back().nesting == BlockNesting::SwitchCase )
            switch_cases.pop_back();
        return block_next_branch(BlockNesting::Else);
    }
    bool switch_stm(const char*, size_t pos) override
    {
        proc_add_exp_statement(create_stm<pruto::stm_switch>());
        switch_cases.emplace_back();
        return block_push(BlockNesting::Switch, pos, 6);
    }
    bool case_stm(const char*, size_t) override
    {
        current_proc->statements.emplace_back(create_stm<pruto::stm_case>());
        assert(blocks.back().nesting == BlockNesting::Switch || blocks.back().nesting == BlockNesting::SwitchCase);
        return block_next_branch(BlockNesting::SwitchCase);
    }
    bool case_stm_label(const char* line, size_t label_start, size_t label_size) override
    {
        std::string l(line + label_start, label_size);
        for( const auto& c : switch_cases.back() ) {
            if( c == l )
                return set_error(AstError::RepeatedCaseLabel, label_start, label_size);
        }
        switch_cases.back().emplace_back(l);
        std::get<pruto::stm_case>(current_proc->statements.back()).cases.emplace_back(std::move(l));
        return true;
    }
    bool while_stm(const char*, size_t pos) override
    {
        proc_add_exp_statement(create_stm<pruto::stm_while>());
        ++loop_count;
        return block_push(BlockNesting::Loop, pos, 5);
    }
    bool for_stm(const char* line, size_t pos, size_t ident_start, size_t ident_size, size_t) override
    {
        auto s = create_stm_with_name<pruto::stm_for>(line + ident_start, ident_size);
        if( lookup(s.name) )
            return set_error(AstError::InvalidRedefine, ident_start, ident_size);

        if( !block_push(BlockNesting::Loop, pos, 3) )
            return false;
        s.id = ident::local_const(local_def_count);
        add_local_name(s.name, s.id);
        proc_add_exp_statement(std::move(s));
        ++loop_count;
        return true;
    }
    bool break_stm(const char*, size_t) override
    {
        current_proc->statements.emplace_back(create_stm<pruto::stm_break>());
        return true;
    }
    bool continue_stm(const char*, size_t) override
    {
        current_proc->statements.emplace_back(create_stm<pruto::stm_continue>());
        return true;
    }
    bool end_stm(const char*, size_t) override
    {
        current_proc->statements.emplace_back(create_stm<pruto::stm_end>());
        switch( blocks.back().nesting ) {
        case BlockNesting::Proc:
            // update proc id
            current_proc->id = current_proc_has_retval ? ident::global_proc_ret(current_proc->id.index(), current_proc->parameters.size())
                                                       : ident::global_proc(current_proc->id.index(), current_proc->parameters.size());
            blocks.front().data[current_proc->name] = current_proc->id;

            current_proc = nullptr;
            current_proc_has_ret_stm = false;
            current_proc_has_retval = false;
            local_def_count = 0;
            assert(loop_count == 0);
            break;
        case BlockNesting::Loop:
            --loop_count;
            break;
        case BlockNesting::SwitchCase:
            switch_cases.pop_back();
            break;
        default:;
        }
        blocks.pop_back();
        return true;
    }
    bool section(const char* line, size_t ident_start, size_t ident_size) override
    {
        if( ast.name.size() > 0 || ast.globals.size() > 0 ) {
            done = true;
            return false;
        }
        ast.name = std::string(line + ident_start, ident_size);
        return true;
    }
    bool param_label_separator(const char*, size_t) override { return true; }
    void newline(const char*, size_t pos) override
    {
        finalize_expression(pos);
        ++ast.end_line;
        ast.end_line_byte_offset += pos + 1;
    }
    void end_of_script(const char* line, size_t pos) override
    {
        newline(line, pos);
        done = true;
    }
    void error(const char*, StatementError e, size_t pos, size_t size) override { set_error(e, pos, size); }


    struct block_info {
        explicit block_info(BlockNesting n) : nesting(n) {}

        MapType<std::string, ident> data;
        BlockNesting nesting;
    };

    enum ExpressionState {
        ExpBeforeOperand,
        ExpAfterOperand,
        ExpStartExpList
    };


    const host_identifier_table* host_entries = nullptr;
    const symop_table* operators = nullptr;

    script_section_ast ast;
    ast_result_error_ptr error_ptr;

    stm_proc_def* current_proc = nullptr;
    expression* current_exp = nullptr;
    const char* current_strlit_start = nullptr;

    std::vector<block_info> blocks = {block_info(BlockNesting::Global)};
    std::vector<std::vector<std::string>> switch_cases;
    std::vector<op_stack_element> op_stack;
    size_t loop_count = 0;
    size_t local_def_count = 0;

    ExpressionState current_exp_state = ExpBeforeOperand;
    bool current_proc_has_ret_stm = false;
    bool current_proc_has_retval = false;
    bool done = false;
};

ast_result section_ast(const char* script, const host_identifier_table& host_entries, const symop_table& operator_table,
                       const script_section_ast& previous_section)
{
    ast_parser_cb cb;
    cb.host_entries = &host_entries;
    cb.operators = &operator_table;
    cb.ast.start_line = previous_section.end_line;
    cb.ast.start_line_byte_offset = previous_section.end_line_byte_offset;
    cb.ast.end_line = cb.ast.start_line;
    cb.ast.end_line_byte_offset = cb.ast.start_line_byte_offset;
    while( !cb.done ) {
        parse_statement(script + cb.ast.end_line_byte_offset, cb.blocks.back().nesting,
                        cb.loop_count > 0 ? InsideLoop::Yes : InsideLoop::No, &cb);
    }
    return {std::move(cb.error_ptr), std::move(cb.ast)};
}

} // namespace
