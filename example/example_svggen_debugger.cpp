/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "example_svggen.h"

#include <pruto/error_messages.h>

#include <charconv>
#include <cstdio>
#include <iostream>

struct command {
    enum Value { Run, Step, Backtrace, Locals, Globals, Quit };
    Value val;
    int32_t arg = -1;
};

static void print_help()
{
    std::cout << R"(List of commands:
  'r' : run to the end
  'r <line-number>' : run until the given line number is reached
  's' : do a single execution step
  't' : print backtrace
  'l' : print locals
  'g' : print globals
  'q' : quit
)" << std::endl;
}

static void unrecognized_command(const std::string& cmd)
{
    std::cout << "Unrecognized command: '" << cmd << "'" << std::endl;
    print_help();
}

static command read_command()
{
    while( true ) {
        std::cout << "> ";
        std::cout.flush();

        std::string line;
        std::getline(std::cin, line);

        if( line.empty() ) {
            continue;
        } else if( line.size() > 1 ) {
            if( line[0] == 'r' && line[1] == ' ' && line.size() > 2 ) {
                int32_t arg;
                auto end = line.data() + line.size();
                auto r = std::from_chars(line.data() + 2, end, arg);
                if( r.ec == std::errc() && r.ptr == end )
                    return {command::Run, arg};
            }
            unrecognized_command(line);
            continue;
        }

        switch( line[0] ) {
        case 'r': return {command::Run};
        case 's': return {command::Step};
        case 't': return {command::Backtrace};
        case 'l': return {command::Locals};
        case 'g': return {command::Globals};
        case 'q': return {command::Quit};
        case 'h':
            print_help();
            break;
        default:
            unrecognized_command(line);
        }
    }
}

struct svggen_data_stringifier final : public pruto::data_stringifier {
    std::string to_string(const pruto::virtual_machine::data_stack_entry* data, pruto::host_type_id host_type) override
    {
        assert(host_type == 0);
        double d;
        std::memcpy(&d, data, sizeof(d));
        char buf[32] = {};
        std::snprintf(buf, 32, "%g", d);
        return buf;
    }
};
static svggen_data_stringifier ds;


struct execution_location {
    pruto::code_generator::proc_instance_index proc = pruto::code_generator::InvalidProcInstance;
    uint32_t statement = 0;
};

static pruto::ident current_proc(pruto::code_generator::proc_instance_index proc, pruto::code_generator* gen)
{
    if( proc == pruto::code_generator::InvalidProcInstance )
        return {};
    assert(proc >= 0);
    return gen->proc_instances()[size_t(proc)].id;
}


int main(int argc, char* argv[])
{
    svggen_data d(argc, argv);
    bool main_scheduled = false;

    std::cout << "Start debugging script file " << argv[1] << std::endl;

    std::vector<execution_location> execution_stack;
    if( !d.vm.call_stack.empty() )
        execution_stack.push_back({});

    while( true ) {
        const auto cmd = read_command();

        bool single_step = false;
        pruto::code_generator::ast_location break_loc{{}, uint32_t(d.ast.globals.size()), -1};

        switch( cmd.val ) {
        case command::Run:
            if( cmd.arg >= 1 ) {
                break_loc = pruto::ast_find_statement(d.ast, uint32_t(cmd.arg - 1));
                if( !break_loc.proc_id && break_loc.statement >= d.ast.globals.size() ) {
                    std::cout << "Error: Invalid statement line" << std::endl;
                    continue;
                }
                if( break_loc.proc_id ) {
                    const auto& statements = pruto::ast_get_global_proc(d.ast, break_loc.proc_id).statements;
                    const auto& stm = statements[break_loc.statement];
                    if( std::holds_alternative<pruto::stm_else>(stm) || std::holds_alternative<pruto::stm_case>(stm)
                        || (break_loc.statement < statements.size() - 1 && std::holds_alternative<pruto::stm_end>(stm)) ) {
                        std::cout << "Error: Statement at given line has no corresponding code" << std::endl;
                        continue;
                    }
                } else if( std::holds_alternative<pruto::stm_proc_def>(d.ast.globals[break_loc.statement]) ) {
                    std::cout << "Error: Proc definition has no corresponding code" << std::endl;
                    continue;
                }
            }
            break;
        case command::Step:
            single_step = true;
            break;
        case command::Backtrace:
            std::cout << d.backtrace() << std::endl;
            continue;
        case command::Locals:
            if( execution_stack.empty() || execution_stack.back().proc < 0 ) {
                std::cout << "Error: Not inside procedure" << std::endl;
            } else {
                const auto& proc_instance = d.gen.proc_instances()[size_t(execution_stack.back().proc)];
                const auto& proc = pruto::ast_get_global_proc(d.ast, proc_instance.id);
                const auto locals = pruto::ast_proc_statement_find_active_locals(proc, execution_stack.back().statement);

                size_t offset = d.vm.frame_offset;
                for( const auto& local : locals ) {
                    const auto& type = d.gen.type_list()[size_t(proc_instance.local_types[local.id.index()])];
                    std::cout << "  " << local.name
                              << ": " << ds(d.vm.data_stack.data() + offset, type, &d.host)
                              << std::endl;
                    offset += d.gen.type_size(type);
                }
            }
            continue;
        case command::Globals:
            for( int32_t i = 0; i < d.vm.next_global_data_init; ++i ) {
                const auto& entry = d.gen.global_data()[size_t(i)];
                std::cout << "  " << std::get<pruto::stm_data_def>(d.ast.globals[entry.id.index()]).name
                          << ": " << ds(d.vm.data_stack.data() + entry.offset, d.gen.type_list()[size_t(entry.type)], &d.host)
                          << std::endl;
            }
            continue;
        case command::Quit:
            std::exit(EXIT_SUCCESS);
        }

        while( true ) {
            const auto step = d.vm.step(&d.host);
            switch( step.result ) {
            case pruto::virtual_machine::step_result::Stop:
                if( !main_scheduled ) {
                    d.gen.push_call(d.main_instance, nullptr, nullptr);
                    main_scheduled = true;
                    execution_stack.push_back({d.main_instance, 0});
                } else {
                    return EXIT_SUCCESS;
                }
                break;
            case pruto::virtual_machine::step_result::Step:
                execution_stack.back().statement = step.argument;
                break;
            case pruto::virtual_machine::step_result::Call:
                execution_stack.push_back({pruto::code_generator::proc_instance_index(step.argument), 0});
                break;
            case pruto::virtual_machine::step_result::Return:
                execution_stack.pop_back();
                break;
            case pruto::virtual_machine::step_result::WaitingForHost:
                d.fail_waiting();
            case pruto::virtual_machine::step_result::ErrorHost:
                d.fail_host();
            case pruto::virtual_machine::step_result::ErrorMissingReturn:
                d.fail_missing_return();
            }

            if( single_step || (current_proc(execution_stack.back().proc, &d.gen) == break_loc.proc_id
                                && execution_stack.back().statement == break_loc.statement) )
                break;
        }
    }
}
