/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "example_svggen.h"

#include <pruto/error_messages.h>

int main(int argc, char* argv[])
{
    svggen_data d(argc, argv);
    bool main_scheduled = false;

    while( true ) {
        switch( d.vm.step(&d.host).result ) {
        case pruto::virtual_machine::step_result::Stop:
            if( !main_scheduled ) {
                d.gen.push_call(d.main_instance, nullptr, nullptr);
                main_scheduled = true;
            } else {
                return EXIT_SUCCESS;
            }
            break;
        case pruto::virtual_machine::step_result::Step:
        case pruto::virtual_machine::step_result::Call:
        case pruto::virtual_machine::step_result::Return:
            break;
        case pruto::virtual_machine::step_result::WaitingForHost:
            d.fail_waiting();
        case pruto::virtual_machine::step_result::ErrorHost:
            d.fail_host();
        case pruto::virtual_machine::step_result::ErrorMissingReturn:
            d.fail_missing_return();
        }
    }
}
