/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "example_svggen.h"

#include <pruto/error_messages.h>

#include <algorithm>
#include <charconv>
#include <cmath>
#include <cstdlib>
#include <iostream>

#ifdef __MINGW32__
#include <cstdio>
#endif

using namespace pruto;

static void wait_for_user()
{
#ifdef _WIN32
    std::cout << "\nPress enter to quit" << std::endl;
    std::cin.ignore();
#endif
}

[[ noreturn ]] static void exit_fail(const std::string& msg)
{
    std::cerr << "Error: " << msg << std::endl;
    wait_for_user();
    std::exit(EXIT_FAILURE);
}

static void append_double(std::string& s, double d)
{
    std::array<char, 32> buf = {};
#ifdef __MINGW32__
    std::snprintf(buf.data(), 32, "%.16g", d);
    s += buf.data();
#else
    auto r = std::to_chars(buf.data(), buf.data() + buf.size(), d);
    if( r.ec != std::errc() )
        assert(false);
    s.append(buf.data(), r.ptr);
#endif
}

static void append_matrix(std::string& s, const double* d)
{
    append_double(s, *d);
    for( size_t i = 1; i < 6; ++i ) {
        s.append(", ");
        append_double(s, d[i]);
    }
}

static void append_color_value(std::string& s, double d)
{
    const int val = d < 0 ? 0 : (d >= 1 ? 255 : std::lround(255 * d));
    char buf[2] = {'?', '?'};
    std::to_chars(buf, buf + 2, val, 16);
    if( val <= 15 )
        s.push_back('0');
    s.push_back(buf[0]);
    if( val > 15 )
        s.push_back(buf[1]);
}

static void append_color(std::string& s, const double* d)
{
    s.push_back('#');
    append_color_value(s, d[0]);
    append_color_value(s, d[1]);
    append_color_value(s, d[2]);
}

static void write_file(const char* path, const std::string& data)
{
    using namespace std;
    FILE* f = fopen(path, "w+");
    if( f == nullptr )
        exit_fail("Cannot open path " + string(path));
    if( (data.size() > 0 && fwrite(data.data(), data.size(), 1, f) != 1) || (fclose(f) != 0) )
        exit_fail("IO error for path " + string(path));
}

static const type_info boolean_type() { return {type_boolean{}}; }
static const type_info number_type() { return {pruto::type_host{0}}; }
static const type_info point_type() { return {type_struct{ {{"x", number_type()}, {"y", number_type()}} }}; }
static const type_info matrix_type() {
    return {type_struct{ {{"a", number_type()}, {"b", number_type()},
                          {"c", number_type()}, {"d", number_type()},
                          {"e", number_type()}, {"f", number_type()}} }};
}
static const type_info rgba_type() {
    return {type_struct{ {{"r", number_type()}, {"g", number_type()},
                          {"b", number_type()}, {"a", number_type()}} }};
}
static const type_info arc_selection_struct_type() {
    const type_info empty_struct{type_struct{}};
    return {type_struct{ {{"small_cw", empty_struct}, {"small_ccw", empty_struct},
                          {"large_cw", empty_struct}, {"large_ccw", empty_struct}} }};
}
static const type_info arc_selection_enum_type() {
    return {type_enum{ {{"small_cw"}, {"small_ccw"}, {"large_cw"}, {"large_ccw"}} }};
}

static std::array<double, 6> matrix_mul(const double* lhs, const double* rhs)
{
    // 0 2 4   0 2 4
    // 1 3 5 * 1 3 5
    return { lhs[0] * rhs[0] + lhs[2] * rhs[1], lhs[1] * rhs[0] + lhs[3] * rhs[1],
             lhs[0] * rhs[2] + lhs[2] * rhs[3], lhs[1] * rhs[2] + lhs[3] * rhs[3],
             lhs[0] * rhs[4] + lhs[2] * rhs[5] + lhs[4], lhs[1] * rhs[4] + lhs[3] * rhs[5] + lhs[5]};
}

static std::array<double, 2> matrix_point_mul(const double* m, const double* p)
{
    return { m[0] * p[0] + m[2] * p[1] + m[4], m[1] * p[0] + m[3] * p[1] + m[5] };
}

struct svggen_type_stringifier final : public type_stringifier {
    std::string to_string(host_type_id host_type) override
    {
        assert(host_type == 0);
        return "number";
    }
};
static svggen_type_stringifier ts;

template<size_t Size>
bool svggen::check_result(const std::array<double, Size>& r, virtual_machine::data_stack_entry* retval)
{
    for( auto d : r ) {
        if( !std::isinf(d) && !std::isnan(d))
            continue;
        error = "Computation resulted in invalid value (" + std::to_string(d) + ")";
        return false;
    }
    virtual_machine::write_array(retval, r);
    return true;
}

symop_table svggen::symops()
{
    return {
        {SymOp::Asterisk, binary_operator(ProcOpMultiply, 1)},
        {SymOp::Slash, binary_operator(ProcOpDivide, 1)},
        {SymOp::Plus, binary_operator(ProcOpPlus, 2)},
        {SymOp::Minus, op_binary_and_left_unary{ binary_operator(ProcOpMinus, 2), left_unary_operator(ProcOpMinusUnary) }},
        {SymOp::And, binary_operator(ProOpAnd, 3)},
        {SymOp::Bar, binary_operator(ProcOpOr, 4)},
        {SymOp::Exclamation, left_unary_operator(ProcOpNot)},
        {SymOp::Equal, binary_operator(ProcOpEq, 5)},
        {SymOp::NotEqual, binary_operator(ProcOpNeq, 5)},
        {SymOp::Less, binary_operator(ProcOpLess, 5)},
        {SymOp::Greater, binary_operator(ProcOpGreater, 5)},
        {SymOp::LessEqual, binary_operator(ProcOpLessEq, 5)},
        {SymOp::GreaterEqual, binary_operator(ProcOpGreaterEq, 5)}
    };
}

host_identifier_table svggen::host_identifier()
{
    return {
        {"pi", ident::host_const(ConstPi)},
        {"arc_selection", ident::host_const(ConstArcSelection)},
        {"abs", ident::host_proc_ret(ProcAbs, 1)},
        {"pow", ident::host_proc_ret(ProcPow, 2)},
        {"ln", ident::host_proc_ret(ProcLn, 1)},
        {"log10", ident::host_proc_ret(ProcLog10, 1)},
        {"sqrt", ident::host_proc_ret(ProcSqrt, 1)},
        {"sin", ident::host_proc_ret(ProcSin, 1)},
        {"cos", ident::host_proc_ret(ProcCos, 1)},
        {"tan", ident::host_proc_ret(ProcTan, 1)},
        {"position", ident::host_proc_ret(ProcPosition, 0)},
        {"new_path", ident::host_proc(ProcNewPath, 4)},
        {"move", ident::host_proc(ProcMove, 1)},
        {"line", ident::host_proc(ProcLine, 1)},
        {"quad", ident::host_proc(ProcQuad, 2)},
        {"cubic", ident::host_proc(ProcCubic, 3)},
        {"arc", ident::host_proc(ProcArc, 5)},
        {"close", ident::host_proc(ProcClose, 0)}
    };
}

code_generator::host_integration::instance svggen::proc_instance(ident id, const type_info* params)
{
    auto it = procs.find(id.index());
    assert(it != procs.end());

    for( const auto& inst : it->second ) {
        bool found = true;
        for( size_t i = 0; i < id.proc_parameter_count(); ++i ) {
            if( params[i] != inst.params[i] ) {
                found = false;
                break;
            }
        }
        if( found )
            return inst.i;
    }

    error = "Unsupported parameter types for '" + host_id_find_name(id, host_identifier(), symops());
    error += "'\n  given types: " + ts.list_to_string(params, id.proc_parameter_count());
    for( const auto& inst : it->second )
        error += "\n  allowed types: " + ts.list_to_string(inst.params.data(), id.proc_parameter_count());

    return {};
}

type_info svggen::num_type() { return number_type(); }
bool svggen::create_num(std::string_view literal, code_generator::data_stack_entry* retval)
{
    double d;
#ifdef __MINGW32__
    d = std::strtod(literal.data(), nullptr);
#else
    auto r = std::from_chars(literal.data(), literal.data() + literal.size(), d, std::chars_format::fixed);
    if( r.ec != std::errc() )
        return false;
#endif
    std::memcpy(retval, &d, sizeof(d));
    return true;
}

type_info svggen::host_data_type(pruto::ident id)
{
    if( id == ident::host_const(ConstPi) )
        return number_type();
    if( id == ident::host_const(ConstArcSelection) )
        return arc_selection_struct_type();
    assert(false);
    return {};
}

code_generator::stack_entry_count svggen::type_size(host_type_id type_id)
{
    return type_id == 0 ? virtual_machine::entries_for_type<double>()
                        : code_generator::host_integration::type_size(type_id);
}

template<typename Op>
std::array<double, 1> unary_op(const virtual_machine::data_stack_entry* args, Op op)
{
    auto in = virtual_machine::read_array<double, 1>(args);
    return {op(in[0])};
}

template<typename Op>
std::array<double, 1> binary_op(const virtual_machine::data_stack_entry* args, Op op)
{
    auto in = virtual_machine::read_array<double, 2>(args);
    return {op(in[0], in[1])};
}

template<typename Op>
bool binary_rel_op(const virtual_machine::data_stack_entry* args, virtual_machine::data_stack_entry* retval, Op op)
{
    auto in = virtual_machine::read_array<double, 2>(args);
    retval->u = op(in[0], in[1]);
    return true;
}

bool svggen::call_proc(virtual_machine::host_instance_index index, const virtual_machine::data_stack_entry* args,
                       virtual_machine::data_stack_entry* retval)
{
    switch( index ) {
    case InstNumMultiply:
        return check_result(binary_op(args, [](double a, double b) { return a * b; }), retval);
    case InstNumPointMultiply:
        {
            const auto in = virtual_machine::read_array<double, 3>(args);
            const std::array<double, 2> out = { in[0] * in[1], in[0] * in[2] };
            return check_result(out, retval);
        }
    case InstPointNumMultiply:
        {
            const auto in = virtual_machine::read_array<double, 3>(args);
            const std::array<double, 2> out = { in[0] * in[2], in[1] * in[2] };
            return check_result(out, retval);
        }
    case InstMatMultiply:
        {
            const auto in = virtual_machine::read_array<double, 12>(args);
            return check_result(matrix_mul(in.data(), in.data() + 6), retval);
        }
    case InstMatPointMultiply:
        {
            const auto in = virtual_machine::read_array<double, 8>(args);
            return check_result(matrix_point_mul(in.data(), in.data() + 6), retval);
        }
    case InstNumDivide:
        return check_result(binary_op(args, [](double a, double b) { return a / b; }), retval);
    case InstNumPlus:
        return check_result(binary_op(args, [](double a, double b) { return a + b; }), retval);
    case InstPointPlus:
        {
            const auto in = virtual_machine::read_array<double, 4>(args);
            const std::array<double, 2> out = { in[0] + in[2], in[1] + in[3] };
            return check_result(out, retval);
        }
    case InstNumMinus:
        return check_result(binary_op(args, [](double a, double b) { return a - b; }), retval);
    case InstNumMinusUnary:
        return check_result(unary_op(args, [](double a) { return -a; }), retval);
    case InstPointMinus:
        {
            const auto in = virtual_machine::read_array<double, 4>(args);
            const std::array<double, 2> out = { in[0] - in[2], in[1] - in[3] };
            return check_result(out, retval);
        }
    case InstPointMinusUnary:
        {
            const auto in = virtual_machine::read_array<double, 2>(args);
            const std::array<double, 2> out = { -in[0], -in[1] };
            return check_result(out, retval);
        }
    case InstBoolAnd:
        retval->u = (args[0].u != 0) && (args[1].u != 0);
        return true;
    case InstBoolOr:
        retval->u = (args[0].u != 0) || (args[1].u != 0);
        return true;
    case InstBoolNot:
        retval->u = (args[0].u == 0);
        return true;
    case InstNumEq:
        return binary_rel_op(args, retval, [](double a, double b) { return a == b; });
    case InstBoolEq:
        retval->u = (args[0].u == args[1].u);
        return true;
    case InstNumNeq:
        return binary_rel_op(args, retval, [](double a, double b) { return a != b; });
    case InstBoolNeq:
        retval->u = (args[0].u != args[1].u);
        return true;
    case InstNumLess:
        return binary_rel_op(args, retval, [](double a, double b) { return a < b; });
    case InstNumGreater:
        return binary_rel_op(args, retval, [](double a, double b) { return a > b; });
    case InstNumLessEq:
        return binary_rel_op(args, retval, [](double a, double b) { return a <= b; });
    case InstNumGreaterEq:
        return binary_rel_op(args, retval, [](double a, double b) { return a >= b; });
    case InstAbs:
        return check_result(unary_op(args, [](double a) { return std::abs(a); }), retval);
    case InstPow:
        return check_result(binary_op(args, [](double a, double b) { return std::pow(a, b); }), retval);
    case InstLn:
        return check_result(unary_op(args, [](double a) { return std::log(a); }), retval);
    case InstLog10:
        return check_result(unary_op(args, [](double a) { return std::log10(a); }), retval);
    case InstSqrt:
        return check_result(unary_op(args, [](double a) { return std::sqrt(a); }), retval);
    case InstSin:
        return check_result(unary_op(args, [](double a) { return std::sin(a); }), retval);
    case InstCos:
        return check_result(unary_op(args, [](double a) { return std::cos(a); }), retval);
    case InstTan:
        return check_result(unary_op(args, [](double a) { return std::tan(a); }), retval);
    case InstPosition:
        virtual_machine::write_array(retval, pos);
        return true;
    case InstNewPath:
        return new_path(args);
    case InstMove:
        return append_points('M', args, 1);
    case InstLine:
        return append_points('L', args, 1);
    case InstQuad:
        return append_points('Q', args, 2);
    case InstCubic:
        return append_points('C', args, 3);
    case InstArc:
        return append_arc(args);
    case InstClose:
        return append_points('Z', nullptr, 0);
    }
    return false;
}

bool svggen::load_host_data(ident id, virtual_machine::stack_entry_count offset,
                            virtual_machine::stack_entry_count count, virtual_machine::data_stack_entry* dst)
{
    assert(id == ident::host_const(ConstPi) && offset == 0 && count == virtual_machine::entries_for_type<double>());
    virtual_machine::write_array<double, 1>(dst, { M_PI });
    return true;
}

svggen::svggen()
{
    data = "<g><path d=\"M 0,0\n";

    procs[ProcOpMultiply] = {
        { {InstNumMultiply, number_type()}, {number_type(), number_type()} },
        { {InstNumPointMultiply, point_type()}, {number_type(), point_type()} },
        { {InstPointNumMultiply, point_type()}, {point_type(), number_type()} },
        { {InstMatMultiply, matrix_type()}, {matrix_type(), matrix_type()} },
        { {InstMatPointMultiply, point_type()}, {matrix_type(), point_type()} }
    };
    procs[ProcOpDivide] = { { {InstNumDivide, number_type()}, {number_type(), number_type()} } };
    procs[ProcOpPlus] = {
        { {InstNumPlus, number_type()}, {number_type(), number_type()} },
        { {InstPointPlus, point_type()}, {point_type(), point_type()} }
    };
    procs[ProcOpMinus] = {
        { {InstNumMinus, number_type()}, {number_type(), number_type()} },
        { {InstPointMinus, point_type()}, {point_type(), point_type()} }
    };
    procs[ProcOpMinusUnary] = {
        { {InstNumMinusUnary, number_type()}, {number_type()} },
        { {InstPointMinusUnary, point_type()}, {point_type()} }
    };
    procs[ProOpAnd] = { { {InstBoolAnd, boolean_type()}, {boolean_type(), boolean_type()} } };
    procs[ProcOpOr] = { { {InstBoolOr, boolean_type()}, {boolean_type(), boolean_type()} } };
    procs[ProcOpNot] = { { {InstBoolNot, boolean_type()}, {boolean_type()} } };
    procs[ProcOpEq] = {
        { {InstNumEq, boolean_type()}, {number_type(), number_type()} },
        { {InstBoolEq, boolean_type()}, {boolean_type(), boolean_type()} }
    };
    procs[ProcOpNeq] = {
        { {InstNumNeq, boolean_type()}, {number_type(), number_type()} },
        { {InstBoolNeq, boolean_type()}, {boolean_type(), boolean_type()} }
    };
    procs[ProcOpLess] = { { {InstNumLess, boolean_type()}, {number_type(), number_type()} } };
    procs[ProcOpGreater] = { { {InstNumGreater, boolean_type()}, {number_type(), number_type()} } };
    procs[ProcOpLessEq] = { { {InstNumLessEq, boolean_type()}, {number_type(), number_type()} } };
    procs[ProcOpGreaterEq] = { { {InstNumGreaterEq, boolean_type()}, {number_type(), number_type()} } };
    procs[ProcAbs] = { { {InstAbs, number_type()}, {number_type()} } };
    procs[ProcPow] = { { {InstPow, number_type()}, {number_type(), number_type()} } };
    procs[ProcLn] = { { {InstLn, number_type()}, {number_type()} } };
    procs[ProcLog10] = { { {InstLog10, number_type()}, {number_type()} } };
    procs[ProcSqrt] = { { {InstSqrt, number_type()}, {number_type()} } };
    procs[ProcSin] = { { {InstSin, number_type()}, {number_type()} } };
    procs[ProcCos] = { { {InstCos, number_type()}, {number_type()} } };
    procs[ProcTan] = { { {InstTan, number_type()}, {number_type()} } };
    procs[ProcPosition] = { { {InstPosition, point_type()}, {} } };
    procs[ProcNewPath] = {
        { {InstNewPath, {}}, {rgba_type(), rgba_type(), number_type(), matrix_type()} }
    };
    procs[ProcMove] = { { {InstMove, {}}, {point_type()} } };
    procs[ProcLine] = { { {InstLine, {}}, {point_type()} } };
    procs[ProcQuad] = { { {InstQuad, {}}, {point_type(), point_type()} } };
    procs[ProcCubic] = { { {InstCubic, {}}, {point_type(), point_type(), point_type()} } };
    procs[ProcArc] = {
        { {InstArc, {}}, {number_type(), number_type(), number_type(), arc_selection_enum_type(), point_type()} }
    };
    procs[ProcClose] = { { {InstClose, {}}, {} } };
}

svggen::~svggen()
{
    const auto x_extend = ext[1] - ext[0];
    const auto y_extend = ext[3] - ext[2];
    if( x_extend == 0 || y_extend == 0 ) {
        std::cout << "No output generated" << std::endl;
        wait_for_user();
        return;
    }

    std::string prefix = R"SVG(<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width=")SVG";
    {
        double w = 1000;
        double h = 1000;
        if( x_extend > y_extend )
            h = (h / x_extend) * y_extend;
        else
            w = (w / y_extend) * x_extend;
        const double scale = w / x_extend;

        const std::array<double, 6> flip_matrix = {1, 0, 0, -1, 0, h};
        const std::array<double, 6> scale_matrix = {scale, 0, 0, scale, -ext[0] * scale, -ext[2] * scale};

        append_double(prefix, w);
        prefix.append("\" height=\"");
        append_double(prefix, h);
        prefix.append(R"SVG(" version="1.1" xmlns="http://www.w3.org/2000/svg">
<g fill-rule="evenodd" transform="matrix()SVG");
        append_matrix(prefix, matrix_mul(flip_matrix.data(), scale_matrix.data()).data());
        prefix.append(")\">\n");
    }

    write_file("output.svg", prefix + data + "\" /></g>\n</g>\n</svg>");
    std::cout << "output.svg generated" << std::endl;
    wait_for_user();
}

void svggen::push_point(double x, double y)
{
    data.push_back(' ');
    append_double(data, x);
    data.push_back(',');
    append_double(data, y);
}

void svggen::extend(const std::array<double, 2>& p)
{
    pos = p;
    const auto q = matrix_point_mul(mat.data(), p.data());
    ext = { std::min(ext[0], q[0]), std::max(ext[1], q[0]), std::min(ext[2], q[1]), std::max(ext[3], q[1]) };
    push_point(p[0], p[1]);
}

bool svggen::element_added()
{
    data.push_back('\n');
    return true;
}

bool svggen::new_path(const virtual_machine::data_stack_entry* args)
{
    const auto in = virtual_machine::read_array<double, 4 + 4 + 1 + 6>(args);
    pos = {0, 0};
    mat = {in[9], in[10], in[11], in[12], in[13], in[14]};
    data.append("\" /></g>\n<g fill=\"");
    if( in[3] <= 0 ) {
        data.append("none");
    } else {
        append_color(data, in.data());
        data.append("\" fill-opacity=\"");
        append_double(data, in[3]);
    }
    data.append("\" stroke=\"");
    if( in[7] <= 0 ) {
        data.append("none");
    } else {
        append_color(data, in.data() + 4);
        data.append("\" stroke-opacity=\"");
        append_double(data, in[7]);
    }
    data.append("\" stroke-width=\"");
    append_double(data, in[8]);
    data.append("\" transform=\"matrix(");
    append_matrix(data, in.data() + 9);
    data.append(")\"><path d=\"M 0,0\n");
    return true;
}

bool svggen::append_points(char prefix, const virtual_machine::data_stack_entry* args, size_t count)
{
    data.push_back(' ');
    data.push_back(prefix);
    for( size_t i = 0; i < count; ++i )
        extend(virtual_machine::read_array<double, 2>(args + (i * 2 * virtual_machine::entries_for_type<double>())));
    return element_added();
}

bool svggen::append_arc(const virtual_machine::data_stack_entry* args)
{
    const auto entries_for_double = virtual_machine::entries_for_type<double>();
    const auto ellipse = virtual_machine::read_array<double, 3>(args);
    const unsigned arc_selection = args[3 * entries_for_double].u;
    const auto p = virtual_machine::read_array<double, 2>(args + (3 * entries_for_double + 1));

    data.push_back(' ');
    data.push_back('A');
    push_point(ellipse[0], ellipse[1]);
    data.push_back(' ');
    append_double(data, (180 / M_PI) * ellipse[2]);
    data.push_back(' ');

    data.push_back(arc_selection > 1 ? '1' : '0');
    data.push_back(',');
    data.push_back(arc_selection & 1 ? '1' : '0');

    // would be nice to extend the bounding box (ext) so that the arc is guaranteed to be fully inside,
    // but seems complicated to compute...
    extend(p);
    return element_added();
}

static std::string read_file(const char* path)
{
    using namespace std;
    string dst;
    FILE* f = fopen(path, "rb");
    long size;
    if( f == nullptr )
        exit_fail("Cannot open path " + string(path));
    if( fseek(f, 0L, SEEK_END) < 0 )
        goto ioerror;
    size = ftell(f);
    if( size < 0 )
        goto ioerror;
    dst.resize(size_t(size));
    if( dst.size() > 1024 * 1024 * 1024 )
        exit_fail("File " + string(path) + " is too large");
    fseek(f, 0L, SEEK_SET);
    if( dst.size() > 0 && fread(dst.data(), dst.size(), 1, f) != 1 )
        goto ioerror;
    fclose(f);
    return dst;
ioerror:
    exit_fail("IO error for path " + string(path));
}

svggen_data::svggen_data(int argc, char* argv[])
{
    if( argc != 2 )
        exit_fail("A script file must be the only argument");

    script = read_file(argv[1]);
    script.erase(std::remove(script.begin(), script.end(), '\r'), script.end());

    {
        auto result = section_ast(script.data(), svggen::host_identifier(), svggen::symops());
        if( result.error )
            exit_fail(error_message(result, script.data()));
        if( !result.section.name.empty() )
            exit_fail("Unknown section: " + result.section.name);
        ast = std::move(result.section);
    }

    if( auto result = gen.reset(&ast, &vm, &host); !std::holds_alternative<std::monostate>(result) )
        exit_fail(error_message(result, ast, script.data(), host.error_msg(), &ts));

    {
        const auto main_id = ast_find_global(ast, "main");
        if( !main_id )
            exit_fail("Missing main procedure");

        if( !main_id.is_proc() || main_id.proc_has_retval() || main_id.proc_parameter_count() != 0 )
            exit_fail("'main' must be a procedure without parameter and return value");

        auto prepare_main = gen.prepare_call(main_id, nullptr);
        if( prepare_main.error )
            exit_fail(error_message(prepare_main, ast, script.data(), host.error_msg(), &ts));

        main_instance = prepare_main.instance;
    }

    gen.push_global_init();
}

std::string svggen_data::backtrace()
{
    return pruto::current_backtrace_string(gen, &ts);
}

void svggen_data::fail_waiting()
{
    exit_fail("Bug (WaitingForHost)\n" + backtrace());
}

void svggen_data::fail_host()
{
    exit_fail(host.error_msg("Unhandled host error") + std::string("\n") + backtrace());
}

void svggen_data::fail_missing_return()
{
    exit_fail("A procedure required to return a value reached 'end' without doing so\n" + backtrace());
}
