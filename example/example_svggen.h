/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PRUTO_EXAMPLE_SVGGEN_H
#define PRUTO_EXAMPLE_SVGGEN_H

#include <pruto/code_generator.h>


class svggen : public pruto::code_generator::host_integration, public pruto::virtual_machine::host_integration {
public:
    enum Entries {
        ProcOpMultiply, ProcOpDivide,
        ProcOpPlus, ProcOpMinus, ProcOpMinusUnary,
        ProOpAnd, ProcOpOr, ProcOpNot,
        ProcOpEq, ProcOpNeq, ProcOpLess, ProcOpGreater, ProcOpLessEq, ProcOpGreaterEq,
        ConstPi, ConstArcSelection,
        ProcAbs, ProcPow, ProcLn, ProcLog10, ProcSqrt, ProcSin, ProcCos, ProcTan,
        ProcPosition, ProcNewPath, ProcMove, ProcLine, ProcQuad, ProcCubic, ProcArc, ProcClose
    };

    enum StaticInstances {
        InstNumMultiply, InstNumPointMultiply, InstPointNumMultiply, InstMatMultiply, InstMatPointMultiply,
        InstNumDivide,
        InstNumPlus, InstPointPlus, InstNumMinus, InstPointMinus, InstNumMinusUnary, InstPointMinusUnary,
        InstBoolAnd, InstBoolOr, InstBoolNot,
        InstNumEq, InstBoolEq, InstNumNeq, InstBoolNeq,
        InstNumLess, InstNumGreater, InstNumLessEq, InstNumGreaterEq,
        InstPi, InstArcSelection,
        InstAbs, InstPow, InstLn, InstLog10, InstSqrt, InstSin, InstCos, InstTan,
        InstPosition, InstNewPath, InstMove, InstLine, InstQuad, InstCubic, InstArc, InstClose
    };

    static pruto::symop_table symops();
    static pruto::host_identifier_table host_identifier();

    instance proc_instance(pruto::ident id, const pruto::type_info* params) override;

    pruto::type_info num_type() override;
    bool create_num(std::string_view literal, pruto::code_generator::data_stack_entry* retval) override;
    pruto::type_info host_data_type(pruto::ident id) override;
    pruto::code_generator::stack_entry_count type_size(pruto::host_type_id type_id) override;

    bool call_proc(pruto::virtual_machine::host_instance_index index, const pruto::virtual_machine::data_stack_entry* args,
                   pruto::virtual_machine::data_stack_entry* retval) override;

    bool load_host_data(pruto::ident id, pruto::virtual_machine::stack_entry_count offset,
                        pruto::virtual_machine::stack_entry_count count, pruto::virtual_machine::data_stack_entry* dst) override;

    const char* error_msg(const char* fallback = nullptr) { return error.empty() ? fallback : error.data(); }

    svggen();
    ~svggen();

private:
    struct instance_info {
        instance i;
        std::vector<pruto::type_info> params;
    };

    template<size_t Size> bool check_result(const std::array<double, Size>& r, pruto::virtual_machine::data_stack_entry* retval);
    void push_point(double x, double y);
    void extend(const std::array<double, 2>& p);
    bool element_added();
    bool new_path(const pruto::virtual_machine::data_stack_entry* args);
    bool append_points(char prefix, const pruto::virtual_machine::data_stack_entry* args, size_t count);
    bool append_arc(const pruto::virtual_machine::data_stack_entry* args);

    std::unordered_map<uint32_t, std::vector<instance_info>> procs;
    std::string error;
    std::string data;
    std::array<double, 2> pos = {0, 0};
    std::array<double, 6> mat = {1, 0, 0, 1, 0, 0};
    std::array<double, 4> ext = {0, 0, 0, 0};
};

struct svggen_data {
    svggen_data(int argc, char* argv[]);
    svggen_data(const svggen_data&) = delete;

    std::string backtrace();

    [[ noreturn ]] void fail_waiting();
    [[ noreturn ]] void fail_host();
    [[ noreturn ]] void fail_missing_return();

    std::string script;
    pruto::script_section_ast ast;
    svggen host;
    pruto::virtual_machine vm;
    pruto::code_generator gen;
    pruto::code_generator::proc_instance_index main_instance;
};

#endif // PRUTO_EXAMPLE_SVGGEN_H
