# Pruto - a tiny "embedding-required language"

- No-dependency C++17 library that includes a parser, AST generator, virtual machine and code generator
- [100-line syntax showcase](docs/syntax-showcase.pruto) of the language
- Example host integration that implements a mini-DSL on top for drawing SVG shapes:

  ![(Output of example/example_svggen_scripts/logo.pruto)](misc/logo.png "Output of example/example_svggen_scripts/logo.pruto")


## Abstract

Pruto is a minimalist programming language - or rather a "language skeleton" (or kind of a DLS creation framework? "embedding-required language"?); while the general syntax is fully fixed, many details of how the syntax is interpreted exactly must be provided by the host system that integrates the interpreter. This includes what type numbers, strings and lists actually are as well as what operators are defined. There is also no standard library, thus it is obvious that without host provided types, operators and functions you can't really do anything with it by itself. With that in mind, for simplicity I'll still refer to it as a language though.

Its intended use case (besides my personal learning experience creating a language by myself) is for situations where you need to embed some light scripting capability into a system, but still want to retain tight control over what can be done from inside it.


## Overview

Traditional embeddable scripting languages hard-code many choices (such as, what type is a number? A float, a double, some bigint/rational or fixed point? etc) that you either have to live with when you use them, or tough luck. Here, the idea is to delegate as much design decisions to the embedding host as possible. This goes as far as Pruto not knowing about the heap or reference types; all things are just values on the stack, all memory allocations are strictly coming only from host provided functions, if any. Thus the host can decide (and implement on top) the memory management strategy (none, manual, gc etc.) - of course within the practical limits of what you can reasonably do with the basic interpreter provided by the library. And with the restriction of not allowing recursive procedure calls in the language, an upper bound on the required stack space should be relatively easy to compute before execution starts, giving fairly strong static guarantees.

Regarding syntax and semantics, a full (only 64 lines long) [formal syntax definition](docs/grammar.txt) is provided. Semantically I consider it closest to C, as there is only data and procedures, so no objects or any other higher level concepts. In other words, it is rather traditional, you shouldn't expect huge innovations, but I do hope you'll find a few curious spins on various details.

Typing is static, but implicit, so one could see procedure definitions kind of like C++ function templates (in all parameters) that get instantiated with concrete types when used.

Implementation wise, the API is designed to be modular, e.g. the parser or AST should be usable not only by the reference interpreter, but any other alternate interpreter/transpiler as well as tooling around the language. The reference interpreter should provide very clear, detailed error descriptions, and make it easy to implement a debugger on top. Generally I avoid any hidden magic, everything is explicit and inspectable, and I strive to keep all APIs as simple and obvious as possible. Some more [notes on implementation details](docs/implementation-notes.md) discuss a few specifics.

The language supports quite a few one- or two-character operators, with some restrictions: binary operators can only be left-associative (their precedence can be freely configured though), and the same symbol may not be used for both a binary and a right-unary operator together (binary and left-unary are allowed, so that "minus" can be properly supported).

For even more information on the language, consult the [language detail notes](docs/language-details.md).


## A note about issue reporting

I very much like to get this as bug free as possible, so reporting errors is appreciated. However, feature requests are most likely not accepted, as minimalism is an explicit goal. I deem the language itself essentially feature complete, and this base library should stay lean, too. At least for now I don't plan to add a lot more to it, any additional tooling/helper/alternate interpreter should go into separate libs/repos.
