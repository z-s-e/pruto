# Parser

The parser is a fairly standard recursive descent one I believe. I do not use an extra traditional "lexer layer", a few internal helper functions suffice for that.

There is one noteworthy consideration regarding the API style; for things like parsers (or, more generally, things that can be modeled as an "event stream") one has the choice between a callback API (where one passes callbacks to the parser that get called on various "events") and a "pull-event"-style API (where the parser is called repeatedly and returns the events as return values). In my personal judgement one should usually prefer the "pull-event"-style, but in the case of a recursive descent parser it is very convenient and efficient to use the stack as temporary storage between "parsing events".

I decided on a compromise solution - single statements (and containing expressions) are parsed with callbacks, but parsing a script (section) is driven from the outside by repeatedly parsing single statements, and a small amount of state needs to be remembered and passed in by the outside. The parser itself does zero heap allocations.

Additionally, the parser only requires that each line is a continuous character buffer, with the `inner_newline`-callback the user can point the parser to a different buffer for continuing the next line. All this is also in the hopes that it will make the parser reusable for other tooling (incremental parsing for editors etc).


# AST

Creating an AST for a script section is done by a single function call to `section_ast(...)`. Contrary to the parser this function requires the whole script to be in the `script` argument as a continuous buffer. As usual, the AST is a simplified, more easily machine-readable representation of the code, and a few errors are detected during construction. Expressions are turned into reverse Polish notation lists via Operator-Precedence Parsing, and identifiers and operators are converted to 32-bit ids.


# VM

Since I want a type-check-phase before execution begins, it just makes too much sense to use that to also generate some bytecode that encodes all the information needed at runtime, instead of re-compute it on the fly again (if I were to use a straight-from-AST-interpretation), thus I chose to implement what I think is called a "hybrid-stack" virtual machine. This should also help separate concerns, as the machine execution code becomes much simpler, and of course improve runtime performance, though that is more of a nice side effect, performance is not the primary goal for this reference implementation.

To keep the VM simple, for the data stack there is no way to declare memory alignments of the different types; all data is layed out consecutively on the stack without any alignment padding. To at least have some basic alignment without too much internal fragmentation, instead of being a raw byte array a data stack element is 4 bytes large. For the host integration this means that for types with 32-bit-or-less alignment requirements, those can be stored and accessed in the stack directly, for stricter alignments the stack data content must be memcopied out into appropriately aligned temporary buffers first. Also note that the code generator imposes a size limit of how many data stack elements a type may span (`MaximumTypeSize`).

The general layout of the data stack is as follows: On initialization all literals are created and space for global data is reserved. Then, when a procedure is called, the arguments are added. During procedure execution local data is added or removed according to the instructions. On top are the temporaries of the currently executed expression. In other words, a "procedure stack frame" has the layout `[<args> | <locals> | <temporaries>]`, and the overall stack `[<globals-and-literals> | <procedure-stack-frames>]`.

Procedure call information is stored an a separate call stack, making generating stack traces significantly easier. There is only one complication; for-loops require storage of a reference to a host iterator, and for that the options were using the data stack, the call stack or create another new stack. Mixing them into the call stack seems to be the most sensible solution - one can easily assert that all for-loops are properly cleaned up before returning from a procedure, it does not complicate the data stack layout, and it avoids the hassle/overhead of managing yet another stack.

Regarding the instructions, I noticed most require at least one accompanying value; a call or jump instruction need a destination, a push needs a source etc. For this reference implementation limiting those values to 24 bit (like the AST id indexes) should be sufficient, and allows pretty good packing of instructions (8 bit opcode + 24 bit value). The individual opcodes (and what their respective value mean) are documented in code comments.

Whenever a host integration callback is called, the instruction pointer is set to a special value to prevent the callback to step-advance the VM further, as obviously the VM can't advance without the callback returning the necessary information. However, it is possible/allowed that the callback pushes a new procedure call onto the VM, but then the callback *must* make sure to make the VM fully execute it before returning, so that the instruction pointer is back to where it was before the callback call.

The host integration callbacks can indicate an error to the VM by returning false, interrupting further execution. It is safe (from the VM's point of view) to retry the operation again, if it makes sense for the host.


# Code generator

The code generator type checks and instantiates VM byte code. As mentioned, simplicity and correctness are more important than performance here, so the AST is transformed into code in the most straight forward manner, without applying any kind of optimizations. But even the most straight forward way can be tricky at times, as is evident by the substantial amount of code needed.

One has to first initialize the code generator by calling the `reset(...)` method, which will instantiate all literals on the VM's data stack, as well as reserve space on the stack, type-check, and generate the code needed to initialize all global data. Before a procedure can be called, it must be instantiated (i.e. type-checked and code-generated) with concrete types, which is done via the `prepare_call(...)` method. Finally, those proc instantiations (resp. the global data init code) can be pushed to the VM (i.e. scheduled for execution) with the `push_call(...)` (resp. `push_global_init()`) methods. Again to keep things simple, there is the rule that one may only push a call to a proc instance to the VM when all globals declared before the corresponding proc have been initialized.

