# Control flow statements

Pruto has two branching and two looping constructs. For branching there is the traditional "if-else if-else" as well as a "switch" on an enum expression. It is required that a switch-block either contains cases for all possible enum values, or has an "else"-branch. Looping is done either with a while- or a for-loop. The for-loop is special in that it is only available if the host provides the needed iterator callbacks, as the language itself does not know about any list-like types - keep in mind that list expressions are also only available when the host can convert them into valid types.


# Procedures

## Return values and calling procedures

Procedures can be categorized into ones that return values and ones that don't. A procedure that contains at least one return-statement with a following expression is not allowed to have a return-statement without one, and that procedure is considered to return values. But since there is no reachability analysis, it is possible and valid to write value-returning procedures where the procedure's end-statement is reachable. There is also no detection of "dead code", so any such code must otherwise be valid as if it were possible to reach it. If the interpreter reaches an end-statement of a value-returning procedure, it will report a "missing-return" runtime error (with "runtime" meaning VM step execution here). This is in fact the only language-internal runtime error, all other runtime errors come from the host integration, if any.

Value-returning procedures may only be called inside expressions, value-less ones only as statements.


## No recursion

The restriction of not allowing recursive procedure calls is certainly a very severe one, I acknowledge that and struggled with this decision quite a bit. But besides the general goal of simplicity of Pruto, there is the real advantage of being easily able to determine an upper bound for the required stack space for a given script. This can allow Pruto to be used in very memory constrained systems, such as transpilation into GPU shader, which I actually might want to try in the future.

Closely related to this is the question of whether one can "store" procedures in variables. If it would be possible to call such stored procedures, that would obviously circumvent the recursion-restriction. On the other hand, it would be very useful to be able to pass procedures from the script to the host as a callback resp. configuration mechanism, without having to use kludges such as passing the procedure name as a string literal.

In the spirit of Pruto's design goals, I chose the following compromise. While Pruto allows you to "store" (there is actually nothing stored at runtime, the procedure id is stored in the type system) procedures in variables or constants, you cannot call them, reassign them or do anything else besides passing them around, with the only useful use case of eventually passing them to the host.

Additionally, Pruto provides a way to (statically) alias a procedure to another name. Contrary to procedure definitions, which are always global, an alias can be created locally and thus be valid only in the local scope (see next section). For example, if you need to call a procedure many times that has a long name, you can create a local short name alias to reduce the amount of typing.


# Scoping

Names have the usual lexical scope; constants, variables and procedure aliases are valid from their point of definition until the end of their current block (or the end of the script section when in the global scope). It is an error to re-define a valid name (i.e. shadowing is not allowed). Contrary to C, each "case" (and "else") in a switch-statement is the start of a block scope.


# Character encoding

Pruto scripts must be encoded in ASCII. The only exceptions are string literals and comments, those may also contain non-ASCII bytes. The interpreter will pass non-ASCII bytes in string literals unmodified to the host integration. A null byte is used as the end of script marker, and thus must occur at the actual end, and not anywhere else.
