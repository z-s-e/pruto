/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PRUTO_VIRTUAL_MACHINE_H
#define PRUTO_VIRTUAL_MACHINE_H

#include <pruto/ast.h>

#include <array>
#include <cstring>

namespace pruto {

    struct virtual_machine {

        // Main data and host integration definitions:

        union data_stack_entry {
            uint32_t u;
            int32_t i;
            char c[4];
        };
        using stack_entry_count = uint32_t;

        using host_instance_index = int32_t;
        static constexpr host_instance_index InvalidHostInstance = -1;

        class host_integration {
        public:
            struct for_iterator {
                virtual bool advance(data_stack_entry* element_storage) = 0;
                virtual void release() = 0;
            };

            virtual bool call_proc(host_instance_index index, const data_stack_entry* args, data_stack_entry* retval);
            virtual bool call_list(host_instance_index index, const data_stack_entry* args, size_t count, data_stack_entry* retval);
            virtual for_iterator* create_iterator(host_instance_index index, const data_stack_entry* arg);

            virtual bool store_host_data(ident id, const data_stack_entry* val, stack_entry_count offset, stack_entry_count count);
            virtual bool load_host_data(ident id, stack_entry_count offset, stack_entry_count count, data_stack_entry* dst);
        };

        struct step_result {
            enum Result {
                Stop, // The vm is idle, the call stack empty.
                Step, // The vm has executed a step. Argument: ast position of the next step
                Call, // The vm has entered a procedure call. Argument: procedure instance index
                Return, // The vm exited a procedure call.
                WaitingForHost, // The vm is waiting for a host callback to return.
                ErrorHost, // A host callback indicated an error.
                ErrorMissingReturn // The vm reached the end of a procedure that must return a value.
            };
            Result result;
            uint32_t argument;
        };


        // Main method:

        step_result step(host_integration* host);


        // Helper methods for data_stack_entry handling:

        static constexpr stack_entry_count entries_for_size(size_t size)
        {
            return (size + sizeof(data_stack_entry) - 1) / sizeof(data_stack_entry);
        }

        template<typename T>
        static constexpr stack_entry_count entries_for_type() { return entries_for_size(sizeof(T)); }

        template<typename T, size_t Size>
        static std::array<T, Size> read_array(const virtual_machine::data_stack_entry* src)
        {
            std::array<T, Size> r;
            for( size_t i = 0; i < Size; ++i )
                std::memcpy(&r[i], src + i * entries_for_type<T>(), sizeof(T));
            return r;
        }

        template<typename T, size_t Size>
        static void write_array(virtual_machine::data_stack_entry* dst, const std::array<T, Size>& data)
        {
            for( size_t i = 0; i < Size; ++i )
                std::memcpy(dst + i * entries_for_type<T>(), &data[i], sizeof(T));
        }


        // !! "Private:" !! -------------------------------------------------------------------------------------------
        // The following are the internal workings and usually do not need to be inspected/interacted with.
        // However, for testing and some advanced usages I'll keep these things still public.

        struct instruction {
            enum class OpCode : uint8_t {
                // The first 2 instructions of byte_code must be invalid instructions and have special handling,
                // otherwise the code generator should not generate any further invalid instructions; the first invalid
                // byte_code instruction signals the stop state (unless the call stack is not empty, which would be
                // an error instead), the second is used to represent waiting for a host call to finish
                Invalid,

                // Pseudo instruction that marks the start of a procedure; val is the proc_instance_index, or
                // ident::max_index() for the global data init procedure;
                // a SizeArg must follow that indicates the args size for frame offset calculation
                StartProc,

                // Do a call; val contains the byte code offset which must contain a StartProc pseudo instruction
                CallProc,

                // Do a host call; val contains an internal index into a datastructure which stores for every
                // host proc instance the index, params size, retval size (host_instances)
                CallHost,

                // Push val onto the data stack (for booleans use 0 resp. 1 for false/true, for enum values their index)
                PushIndex,

                // Pop elements from the data stack; val contains the amount
                Pop,

                // Do a jump; val contains the target byte code offset
                Jump,

                // Do a jump if the top data stack element is a zero value; val contains the target byte code offset;
                // the top element is always popped
                JumpIfZeroPop,

                // Returns the current proc; val contains the retval size
                Return,

                // Push a copy of a local, global, or host value identified by val onto the data stack
                PushCopy,       // val is a frame offset relative value
                PushCopyGlobal, // val is absolute (not frame_offset relative); also used for literals (which are converted to pseudo-globals)
                PushCopyHost,   // val is an internal index into a datastructure storing the host data id and offset (host_data)
                // Copy (and pop) the top of the stack to a local, global, or host var (val same as the respective Push* opcodes)
                PopMove,
                PopMoveGlobal,
                PopMoveHost,

                // Create a list or apply a list operator; val is internal index into host proc datastructure (host_instances)
                CallList,

                // Pseudo instruction; must follow any StartProc, PushCopy*, PopMove*, CallList;
                // val contains the list- or stack element-count
                SizeArg,

                // Create a new iterator instance; val is internal index into host proc datastructure (host_instances)
                PushForLoop,

                // Advance the current iterator instace; if no more elements, do a jump instead (val is target)
                AdvanceForLoopOrJump,

                // Release iterator instances; val is the number of instances to pop
                PopForLoop,

                // Use the current top of data stack as offset into the following jump table (current top gets popped);
                // val is the size of the jump table
                JumpTable,

                // Pseudo instruction that must follow a JumpTable instruction; val is jump target
                JumpTableEntry,

                // Mark the next global data element as initialized; val must be 0
                IncrementGlobalDataInitCounter,

                // Set the ast position of the following instruction(s) to the content of val, to allow associating
                // the byte code to the originating ast and the interpreter to recognise step boundaries
                UpdateAstPosition,

                // For procedures with a return value, this is the last instruction that will abort execution when encountered; val must be 0
                MissingReturnError
            };
            uint32_t val : 24;
            OpCode code;

            instruction() : val(0), code(OpCode::Invalid) {}
            instruction(OpCode c, size_t v) : val(v), code(c) { assert(v <= (uint32_t(1) << 24) - 1); }
        };

        using instruction_offset = uint32_t;

        static constexpr instruction_offset StopIP = 0;
        static constexpr instruction_offset HostCallIP = 1;

        struct call_stack_entry_call_data {
            instruction_offset return_ip;
            stack_entry_count frame_offset;
            data_stack_entry* retval_storage; // if set, store retval there instead of stack (for host-initiated calls)
        };
        struct call_stack_entry_for_loop_data { // the call stack contains besides the calls also the necessary data for for-loop iteration
            stack_entry_count element_size;
            host_integration::for_iterator* iterator;
        };
        using call_stack_entry = std::variant<call_stack_entry_call_data, call_stack_entry_for_loop_data>;

        struct host_instance_info {
            host_instance_index index = InvalidHostInstance;
            stack_entry_count params_size = 0;
            stack_entry_count retval_size = 0;
        }; // Note: this does not store whether the instance is a proc, list, or iterator

        struct host_data_info {
            ident id;
            stack_entry_count offset = 0;
        };

        void push_call(instruction_offset loc, const data_stack_entry* args, data_stack_entry* retval_storage);

        std::vector<instruction> byte_code;
        std::vector<data_stack_entry> data_stack;
        std::vector<call_stack_entry> call_stack;

        std::vector<host_instance_info> host_instances;
        std::vector<host_data_info> host_data;

        instruction_offset ip = StopIP;
        stack_entry_count frame_offset = 0;
        int32_t next_global_data_init = -1;
    };

    bool operator ==(const virtual_machine::host_instance_info& l, const virtual_machine::host_instance_info& r);
    bool operator ==(const virtual_machine::host_data_info& l, const virtual_machine::host_data_info& r);

} // namespace

#endif // PRUTO_VIRTUAL_MACHINE_H
