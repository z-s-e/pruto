/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PRUTO_DEBUG_UTILS_H
#define PRUTO_DEBUG_UTILS_H

#include <pruto/code_generator.h>

namespace pruto {

    struct type_stringifier {
        virtual std::string to_string(host_type_id host_type);
        virtual std::string to_string(const type_proc& proc);
        std::string operator()(const type_info& type);

        std::string list_to_string(const type_info* t, size_t count);
    };

    struct data_stringifier {
        virtual std::string to_string(const virtual_machine::data_stack_entry* data, host_type_id host_type);
        std::string operator()(const virtual_machine::data_stack_entry* data, const type_info& type,
                               code_generator::host_integration* host);
    };

    struct active_local_data_entry {
        ident id;
        std::string_view name;
    };
    std::vector<active_local_data_entry> ast_proc_statement_find_active_locals(const stm_proc_def& proc, uint32_t statement);

    std::vector<code_generator::instance_location> current_backtrace(const code_generator& gen);

    expression_base exp_element_script_location(const exp_element& element);
    uint32_t proc_statement_script_line(const proc_statement& stm);
    uint32_t ast_global_script_line(const std::variant<stm_proc_def, stm_data_def>& global);
    uint32_t ast_find_statement_script_line(const script_section_ast& ast, const code_generator::ast_location& loc);
    code_generator::ast_location ast_find_statement(const script_section_ast& ast, uint32_t line);
    const exp_element& ast_find_expression_element(const script_section_ast& ast, const code_generator::ast_location& loc);

    const char* find_line(const char* text, size_t line);
    std::string join_strings(const std::vector<std::string>& list);
    std::string strlit_encode_singleline(const char* text);
    std::string host_id_find_name(ident id, const host_identifier_table& host_entries, const symop_table& operator_table);

    std::string to_string(ident id);

    std::string to_string(ExpressionError e);
    std::string to_string(StatementError e);
    std::string to_string(AstError e);
    std::string to_string(LiteralError e);
    std::string to_string(TypeCheckError e);
    std::string to_string(code_generator::LimitError e);

    std::string to_string(SymOp s);
    std::string to_string(virtual_machine::instruction::OpCode code);

    std::string to_string(const expression& exp);
    std::string to_string(const stm_data_def& stm);
    std::string to_string(const stm_reassign& stm);
    std::string to_string(const stm_call& stm);
    std::string to_string(const stm_return& stm);
    std::string to_string(const stm_return_value& stm);
    std::string to_string(const stm_break& stm);
    std::string to_string(const stm_continue& stm);
    std::string to_string(const stm_if& stm);
    std::string to_string(const stm_else_if& stm);
    std::string to_string(const stm_else& stm);
    std::string to_string(const stm_switch& stm);
    std::string to_string(const stm_case& stm);
    std::string to_string(const stm_while& stm);
    std::string to_string(const stm_for& stm);
    std::string to_string(const stm_end& stm);
    std::string to_string(const proc_statement& stm);
    std::string to_string(const stm_proc_def& p);
    std::string to_string(const script_section_ast& section);
    std::string to_string(const ast_result_error& e);

    std::string to_string(virtual_machine::instruction inst);
    std::string to_string(const std::vector<virtual_machine::instruction>& bytecode);
    std::string to_string(virtual_machine::step_result step);


    inline bool operator ==(const exp_bool_lit& lhs, const exp_bool_lit& rhs)
    {
        return lhs.line == rhs.line && lhs.pos == rhs.pos && lhs.val == rhs.val;
    }
    inline bool operator ==(const exp_num_lit& lhs, const exp_num_lit& rhs)
    {
        return lhs.line == rhs.line && lhs.pos == rhs.pos && lhs.val == rhs.val;
    }
    inline bool operator ==(const exp_string_lit& lhs, const exp_string_lit& rhs)
    {
        return lhs.line == rhs.line && lhs.pos == rhs.pos && lhs.val == rhs.val;
    }
    inline bool operator ==(const exp_ref& lhs, const exp_ref& rhs)
    {
        return lhs.line == rhs.line && lhs.pos == rhs.pos && lhs.id == rhs.id && lhs.sub == rhs.sub;
    }
    inline bool operator ==(const exp_enum& lhs, const exp_enum& rhs)
    {
        return lhs.line == rhs.line && lhs.pos == rhs.pos && lhs.id == rhs.id && lhs.sub_mem == rhs.sub_mem;
    }
    inline bool operator ==(const exp_call& lhs, const exp_call& rhs)
    {
        return lhs.line == rhs.line && lhs.pos == rhs.pos && lhs.id == rhs.id;
    }
    inline bool operator ==(const exp_list& lhs, const exp_list& rhs)
    {
        return lhs.line == rhs.line && lhs.pos == rhs.pos && lhs.count == rhs.count && lhs.is_operator == rhs.is_operator;
    }
    inline bool operator ==(const exp_struct& lhs, const exp_struct& rhs)
    {
        return lhs.line == rhs.line && lhs.pos == rhs.pos && lhs.members == rhs.members;
    }

    inline bool operator ==(const stm_data_def& lhs, const stm_data_def& rhs)
    {
        return lhs.line == rhs.line && lhs.name == rhs.name && lhs.id == rhs.id && lhs.e == rhs.e;
    }
    inline bool operator ==(const stm_reassign& lhs, const stm_reassign& rhs)
    {
        return lhs.line == rhs.line && lhs.id == rhs.id && lhs.sub == rhs.sub && lhs.e == rhs.e;
    }
    inline bool operator ==(const stm_call& lhs, const stm_call& rhs)
    {
        return lhs.line == rhs.line && lhs.e == rhs.e;
    }
    inline bool operator ==(const stm_return& lhs, const stm_return& rhs)
    {
        return lhs.line == rhs.line;
    }
    inline bool operator ==(const stm_return_value& lhs, const stm_return_value& rhs)
    {
        return lhs.line == rhs.line && lhs.e == rhs.e;
    }
    inline bool operator ==(const stm_break& lhs, const stm_break& rhs)
    {
        return lhs.line == rhs.line;
    }
    inline bool operator ==(const stm_continue& lhs, const stm_continue& rhs)
    {
        return lhs.line == rhs.line;
    }
    inline bool operator ==(const stm_if& lhs, const stm_if& rhs)
    {
        return lhs.line == rhs.line && lhs.e == rhs.e;
    }
    inline bool operator ==(const stm_else_if& lhs, const stm_else_if& rhs)
    {
        return lhs.line == rhs.line && lhs.e == rhs.e;
    }
    inline bool operator ==(const stm_else& lhs, const stm_else& rhs)
    {
        return lhs.line == rhs.line;
    }
    inline bool operator ==(const stm_switch& lhs, const stm_switch& rhs)
    {
        return lhs.line == rhs.line && lhs.e == rhs.e;
    }
    inline bool operator ==(const stm_case& lhs, const stm_case& rhs)
    {
        return lhs.line == rhs.line && lhs.cases == rhs.cases;
    }
    inline bool operator ==(const stm_while& lhs, const stm_while& rhs)
    {
        return lhs.line == rhs.line && lhs.e == rhs.e;
    }
    inline bool operator ==(const stm_for& lhs, const stm_for& rhs)
    {
        return lhs.line == rhs.line && lhs.id == rhs.id && lhs.name == rhs.name && lhs.e == rhs.e;
    }
    inline bool operator ==(const stm_end& lhs, const stm_end& rhs)
    {
        return lhs.line == rhs.line;
    }
    inline bool operator ==(const stm_proc_def& lhs, const stm_proc_def& rhs)
    {
        return lhs.line == rhs.line && lhs.id == rhs.id && lhs.name == rhs.name
                && lhs.parameters == rhs.parameters && lhs.statements == rhs.statements;
    }

    inline bool operator ==(const virtual_machine::instruction& l, const virtual_machine::instruction& r)
    {
        return std::memcmp(&l, &r, sizeof(l)) == 0;
    }
    inline bool operator ==(const virtual_machine::data_stack_entry& l, const virtual_machine::data_stack_entry& r)
    {
        return std::memcmp(&l, &r, sizeof(l)) == 0;
    }
    inline bool operator ==(const virtual_machine::step_result& l, const virtual_machine::step_result& r)
    {
        return std::memcmp(&l, &r, sizeof(l)) == 0;
    }

} // namespace

#endif // PRUTO_DEBUG_UTILS_H
