/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PRUTO_TYPE_SYSTEM_H
#define PRUTO_TYPE_SYSTEM_H

#include <pruto/ast.h>

namespace pruto {

    using host_type_id = int32_t;
    constexpr host_type_id InvalidHostType = -1;

    struct type_host {
        host_type_id id = InvalidHostType;
    };
    struct type_boolean {
    };
    struct type_struct {
        struct member;
        std::vector<member> members;
    };
    struct type_enum {
        std::vector<std::string> members;
    };
    struct type_proc {
        ident id;
    };

    using type_info = std::variant<type_host, type_boolean, type_struct, type_enum, type_proc>;

    struct type_struct::member {
        std::string name;
        type_info type;
    };

    inline bool operator ==(const type_host& l, const type_host& r) { return l.id == r.id; }
    inline bool operator !=(const type_host& l, const type_host& r) { return !(l == r); }
    inline bool operator ==(const type_boolean&, const type_boolean&) { return true; }
    inline bool operator !=(const type_boolean&, const type_boolean&) { return false; }
    bool operator ==(const type_struct& l, const type_struct& r);
    inline bool operator !=(const type_struct& l, const type_struct& r) { return !(l == r); }
    inline bool operator ==(const type_enum& l, const type_enum& r) { return l.members == r.members; }
    inline bool operator !=(const type_enum& l, const type_enum& r) { return !(l == r); }
    inline bool operator ==(const type_proc& l, const type_proc& r) { return l.id == r.id; }
    inline bool operator !=(const type_proc& l, const type_proc& r) { return !(l == r); }

    bool type_is_valid(const type_info& t);


    enum class LiteralError {
        HostDisabledNumLiterals, // the AST contains num literals, which the host does not support
        HostDisabledStrLiterals, // the AST contains str literals, which the host does not support
        HostRejectedNumLiteral, // a specific instance of a num literal was rejected
        HostRejectedStrLiteral // a specific instance of a str literal was rejected
    };

    enum class TypeCheckError {
        InvalidMember,
        RefNotAStruct,
        ListElementTypeMismatch,
        ConditionalNotBoolean,
        SwitchNotEnum,
        MissingCaseOrElse,
        ReassignTypeMismatch,
        ReturnTypeMismatch,
        HostRejetedProc,
        HostRejetedList,
        HostRejetedListOp,
        HostRejetedIter
    };


    struct enum_and_index {
        type_enum e;
        size_t index;
    };
    enum_and_index get_enum_and_index(const type_struct& s, const std::string& member);

} // namespace

#endif // PRUTO_TYPE_SYSTEM_H
