/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PRUTO_AST_H
#define PRUTO_AST_H

#include <pruto/parser.h>

#include <cassert>
#include <memory>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

namespace pruto {

    static constexpr int BlockNestingLimit = 10; // like ExpressionNestingLimit in the parser, pretty arbitrary choice

    template<typename Key, typename T>
    using MapType = std::unordered_map<Key, T>;

    enum class AstError {
        UnknownName,
        RecursiveRef,
        InvalidRedefine,
        ReassignNotVar,
        UndefinedOperator,
        LeftUnaryAfterOperand,
        RightUnaryBeforeOperand,
        MissingBinaryOperatorBetweenOperands,
        BinaryOperatorMissingLeftOperand,
        BinaryOperatorMissingRightOperand,
        NameIsNotProc,
        ProcInEnumRef,
        ProcHasNoValue,
        ProcHasValue,
        ProcTooManyParameter,
        ProcArgParameterCountMismatch,
        ProcReturnValueMismatch,
        RepeatedStructMemberName,
        RepeatedCaseLabel,
        BlockNestingLimitExceeded
    };

    class ident {
    public:
        using value_type = uint32_t;

        enum Scope {
            Local = 1,
            Global = 2,
            Host = 3
        };

        ident() = default;

        static ident local_const(value_type index) { return data(index, Local, false); }
        static ident local_var(value_type index) { return data(index, Local, true); }
        static ident global_const(value_type index) { return data(index, Global, false); }
        static ident global_var(value_type index) { return data(index, Global, true); }
        static ident host_const(value_type index) { return data(index, Host, false); }
        static ident host_var(value_type index) { return data(index, Host, true); }
        static ident global_proc(value_type index, uint8_t params) { return proc(index, Global, params, false); }
        static ident global_proc_ret(value_type index, uint8_t params) { return proc(index, Global, params, true); }
        static ident host_proc(value_type index, uint8_t params) { return proc(index, Host, params, false); }
        static ident host_proc_ret(value_type index, uint8_t params) { return proc(index, Host, params, true); }

        explicit operator bool() const { return value != 0; }

        Scope scope() const { return Scope((value & (BitVarOrRet - 1)) / BitScopeStart); }

        bool is_local() const { return scope() == Local; }
        bool is_global() const { return scope() == Global; }
        bool is_host() const { return scope() == Host; }
        bool is_proc() const { return value & BitProc; }
        bool is_data() const { return !is_proc() && value != 0; }
        bool data_is_var() const { assert(is_data()); return value & BitVarOrRet; }
        bool proc_has_retval() const { assert(is_proc()); return value & BitVarOrRet; }
        value_type index() const { return value & (BitParamStart - 1); }

        value_type proc_parameter_count() const
        {
            assert(is_proc());
            return (value & (BitScopeStart - 1)) / BitParamStart;
        }

        static constexpr value_type max_index() { return BitParamStart - 1; }

        static constexpr value_type proc_max_parameters()
        {
            value_type res = 1;
            value_type tmp = BitParamStart;
            while( tmp < BitScopeStart ) {
                res *= 2;
                tmp <<= 1;
            }
            return res - 1;
        }

        value_type data() const { return value; }

    private:
        ident(value_type idx, Scope s, value_type flags)
            : value(idx | flags | (value_type(s) * BitScopeStart))
        {
            assert(idx <= max_index());
            assert(s >= Local && s <= Host);
            assert(s != Local || (flags & BitProc) == 0);
        }

        static ident data(value_type idx, Scope s, bool var)
        {
            return ident(idx, s, var ? value_type(BitVarOrRet) : 0);
        }

        static ident proc(value_type idx, Scope s, uint8_t params, bool ret)
        {
            assert(params <= proc_max_parameters());
            value_type flags = value_type(params) * BitParamStart;
            if( ret )
                flags |= BitVarOrRet;
            return ident(idx, s, BitProc | flags);
        }

        enum Bit {
            BitParamStart = value_type(1) << 24,
            BitScopeStart = value_type(1) << 28,
            BitVarOrRet =   value_type(1) << 30,
            BitProc =       value_type(1) << 31
        };

        value_type value = 0;
    };
    inline bool operator ==(ident l, ident r) { return l.data() == r.data(); }
    inline bool operator !=(ident l, ident r) { return l.data() != r.data(); }


    // Expressions:

    enum class ExpressionNesting { Group, Call, List, Struct };

    struct expression_base {
        uint32_t line = 0;
        uint32_t pos = 0;
    };

    struct exp_bool_lit final : public expression_base { bool val = false; };
    struct exp_num_lit final : public expression_base { std::string val; };
    struct exp_string_lit final : public expression_base { std::string val; };
    struct exp_ref final : public expression_base {
        ident id;
        std::vector<std::string> sub;
    };
    struct exp_enum final : public expression_base {
        ident id;
        std::vector<std::string> sub_mem;
    };

    struct exp_call final : public expression_base { ident id; };
    struct exp_list final : public expression_base {
        size_t count = 0;
        bool is_operator = false;
    };
    struct exp_struct final : public expression_base { std::vector<std::string> members; };

    using exp_element = std::variant<exp_bool_lit, exp_num_lit, exp_string_lit, exp_ref, exp_enum,
                                     exp_call, exp_list, exp_struct>;

    // The elements are sorted in execution order (stack machine semantics, reverse Polish notation)
    using expression = std::vector<exp_element>;


    // Statements:

    struct statement_base { uint32_t line = 0; };
    struct expression_statement : public statement_base { expression e; };
    struct id_expression_statement : public statement_base {
        ident id;
        expression e;
    };

    struct stm_data_def final : public id_expression_statement { std::string name; };
    struct stm_reassign final : public id_expression_statement { std::vector<std::string> sub; };

    struct stm_call final : public expression_statement {};
    struct stm_return final : public statement_base {};
    struct stm_return_value final : public expression_statement {};

    struct stm_if final : public expression_statement {};
    struct stm_else_if final : public expression_statement {};
    struct stm_else final : public statement_base {};
    struct stm_switch final : public expression_statement {};
    struct stm_case final : public statement_base { std::vector<std::string> cases; };
    struct stm_while final : public expression_statement {};
    struct stm_for final : public id_expression_statement { std::string name; };

    struct stm_break final : public statement_base {};
    struct stm_continue final : public statement_base {};

    struct stm_end final : public statement_base {};

    using proc_statement = std::variant<stm_data_def, stm_reassign, stm_call,
                                        stm_return, stm_return_value,
                                        stm_if, stm_else_if, stm_else, stm_switch, stm_case,
                                        stm_while, stm_for, stm_break, stm_continue, stm_end>;

    const expression* proc_statement_get_expression(const proc_statement& stm);

    struct stm_proc_def final : public statement_base {
        ident id;
        std::string name;
        std::vector<std::string> parameters;
        std::vector<proc_statement> statements;
    };


    // Section AST:

    struct script_section_ast {
        std::string name;
        std::vector<std::variant<stm_proc_def, stm_data_def>> globals;

        uint32_t start_line = 0;
        uint32_t start_line_byte_offset = 0;
        uint32_t end_line = 0;
        uint32_t end_line_byte_offset = 0;
    };

    const stm_proc_def& ast_get_global_proc(const script_section_ast& ast, ident id);
    ident ast_find_global(const script_section_ast& ast, const char* name);


    // Symop table:

    struct op_left_unary{ ident id; };
    struct op_right_unary{ ident id; };
    struct op_binary {
        ident id;
        uint8_t precedence = 0;
    };
    struct op_binary_and_left_unary {
        op_binary b;
        op_left_unary u;
    };

    inline op_binary binary_operator(ident::value_type index, uint8_t precedence) { return {ident::host_proc_ret(index, 2), precedence}; }
    inline op_left_unary left_unary_operator(ident::value_type index) { return {ident::host_proc_ret(index, 1)}; }
    inline op_right_unary right_unary_operator(ident::value_type index) { return {ident::host_proc_ret(index, 1)}; }

    using symop_table = MapType<SymOp, std::variant<op_left_unary, op_right_unary, op_binary, op_binary_and_left_unary>>;


    // Main API for AST generation:

    using host_identifier_table = MapType<std::string, ident>;

    struct ast_result_error {
        struct expression_nesting_info {
            uint32_t line;
            uint32_t pos;
            ExpressionNesting type;
        };

        std::variant<AstError, StatementError, ExpressionError> val;
        uint32_t line = 0;
        uint32_t line_byte_offset = 0;
        uint32_t pos = 0;
        uint32_t size = 0;
        exp_call call; // used for BinaryOperatorMissingLeft/RightOperand, ProcHasNoValue, ProcHasValue, ProcArgParameterCountMismatch
        ident::value_type call_args = 0; // used for ProcArgParameterCountMismatch
        std::vector<expression_nesting_info> exp_nesting;
    };
    using ast_result_error_ptr = std::unique_ptr<ast_result_error>;

    struct ast_result {
        ast_result_error_ptr error;
        script_section_ast section;
    };
    ast_result section_ast(const char* script, const host_identifier_table& host_entries, const symop_table& operator_table,
                           const script_section_ast& previous_section = {});

} // namespace

#endif // PRUTO_AST_H
