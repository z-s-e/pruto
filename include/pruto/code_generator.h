/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PRUTO_CODE_GENERATOR_H
#define PRUTO_CODE_GENERATOR_H

#include <pruto/type_system.h>
#include <pruto/virtual_machine.h>

#include <string_view>

namespace pruto {

    class code_generator {
    public:

        // Main data and host integration definitions:

        using data_stack_entry = virtual_machine::data_stack_entry;
        using stack_entry_count = virtual_machine::stack_entry_count;

        static constexpr stack_entry_count MaximumTypeSize = 1024;

        using host_instance_index = virtual_machine::host_instance_index;

        class host_integration {
        public:
            struct instance {
                host_instance_index index = virtual_machine::InvalidHostInstance;
                type_info retval_type;
            };
            virtual instance proc_instance(ident id, const type_info* params);
            virtual instance list_instance(const type_info& element_type, size_t count);
            virtual instance list_operator_instance(const type_info& operand, const type_info& element_type, size_t count);
            virtual instance iterator_instance(const type_info& param);

            virtual type_info num_type();
            virtual bool create_num(std::string_view literal, data_stack_entry* retval);

            virtual type_info str_type();
            virtual bool create_str(std::string_view literal, data_stack_entry* retval);

            virtual type_info host_data_type(ident id);

            virtual stack_entry_count type_size(host_type_id type_id);
            stack_entry_count type_size(const type_info& t);
        };

        using proc_instance_index = int32_t;
        static constexpr proc_instance_index InvalidProcInstance = -1;

        enum class LimitError {
            TypeExceedsSizeLimit
        };

        struct ast_location {
            ident proc_id; // an invalid proc_id is used for the global data init code (and 'statement' is then the global data index)
            uint32_t statement;
            int32_t expression_element; // -1 for error outside expression
        };

        struct instance_location final : public ast_location {
            std::vector<type_info> param_types;
        };

        struct prepare_result_error {
            std::variant<TypeCheckError, LimitError> val;

            // sub_index is used to more precisely describe the error location:
            // - InvalidMember in a ref or enum expression: index (counting from 1) of the sub ref
            // - InvalidMember in a case list: index of the case list
            // - RefNotAStruct: index (counting from 1) of the sub ref; 0 when the referenced ident itself is not a struct
            // - ListElementTypeMismatch: list element index of the mismatch
            size_t sub_index = 0;
            type_info mismatch_expected; // used for InvalidMember, ListElementTypeMismatch, ReassignTypeMismatch, ReturnTypeMismatch
            type_info mismatch_actual; // as above; additionally used for RefNotAStruct, ConditionalNotBoolean, SwitchNotEnum, TypeExceedsSizeLimit
            std::vector<std::string> missing_cases; // list of not handled cases in a switch statement
            std::vector<instance_location> location;
        };
        using prepare_result_error_ptr = std::unique_ptr<prepare_result_error>;

        struct literal_error final : public ast_location {
            LiteralError val;
        };

        using reset_result = std::variant<std::monostate, literal_error, prepare_result_error_ptr>;

        struct prepare_result {
            prepare_result_error_ptr error;

            proc_instance_index instance = InvalidProcInstance;
            type_info retval_type;
        };


        // Main methods:

        reset_result reset(const script_section_ast* section, virtual_machine* machine, host_integration* integration);
        prepare_result prepare_call(ident proc, const type_info* params);

        bool push_global_init(); // returns false if global init was already scheduled or there are no globals to initialize
        bool push_call(proc_instance_index instance, const data_stack_entry* args, data_stack_entry* retval_storage);
            // returns false if not all global data before the procedure definition has been initialized


        // Introspection API (for e.g. building a debugger)

        using type_index = int32_t;
        static constexpr type_index InvalidTypeIndex = -1;
        static constexpr type_index BooleanTypeIndex = 0;

        struct proc_instance {
            ident id;
            virtual_machine::instruction_offset byte_code_offset = 0;
            std::vector<type_index> local_types; // Includes parameters at front and retval at back (if any)
        };

        struct global_data_entry {
            ident id;
            stack_entry_count offset = 0;
            type_index type = InvalidTypeIndex;
        };

        stack_entry_count type_size(type_index index) const;
        stack_entry_count type_size(const type_info& t) const { return host->type_size(t); }
        const std::vector<type_info>& type_list() const { return types; }
        const std::vector<global_data_entry>& global_data() const { return global_data_info; }
        const std::vector<proc_instance>& proc_instances() const { return proc_instance_info; }
        const script_section_ast* current_ast() const { return ast; }
        virtual_machine* current_vm() const { return vm; }

    private:
        struct expression_visitor;
        struct proc_statement_visitor;
        struct expression_literal_extractor;
        struct proc_statement_literal_extractor;

        const script_section_ast* ast = nullptr;
        virtual_machine* vm = nullptr;
        host_integration* host = nullptr;
        std::vector<proc_instance> proc_instance_info;
        std::vector<global_data_entry> global_data_info;
        std::vector<type_info> types;
        MapType<std::string_view, stack_entry_count> num_lit_offset;
        MapType<std::string_view, stack_entry_count> str_lit_offset;
        type_index host_num_type = InvalidTypeIndex;
        type_index host_str_type = InvalidTypeIndex;
        virtual_machine::instruction_offset global_init = 0;
    };

} // namespace

#endif // PRUTO_CODE_GENERATOR_H
