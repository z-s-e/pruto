/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PRUTO_ERROR_MESSAGES_H
#define PRUTO_ERROR_MESSAGES_H

#include <pruto/debug_utils.h>

namespace pruto {

    std::string error_message(const ast_result& r, const char* script);
    std::string error_message(const code_generator::prepare_result& r, const script_section_ast& ast, const char* script,
                              const char* host_reject_message = nullptr, type_stringifier* type_string = nullptr);
    std::string error_message(const code_generator::reset_result& r, const script_section_ast& ast, const char* script,
                              const char* host_reject_message = nullptr, type_stringifier* type_string = nullptr);

    std::string current_backtrace_string(const code_generator& gen, type_stringifier* type_string = nullptr);

    struct error_script_location {
        uint32_t line;
        uint32_t bytes_before_line;
        uint32_t pos;
    };

    error_script_location error_find_script_location(TypeCheckError e, size_t sub_index,
                                                     const code_generator::ast_location& loc,
                                                     const script_section_ast& ast, const char* script);
    error_script_location error_find_script_location(const code_generator::literal_error& e,
                                                     const script_section_ast& ast, const char* script);
    error_script_location error_find_script_location(code_generator::LimitError e,
                                                     const code_generator::ast_location& loc,
                                                     const script_section_ast& ast, const char* script);

    error_script_location error_find_case_label(error_script_location case_statement_loc, size_t index, const char* script);

} // namespace

#endif // PRUTO_ERROR_MESSAGES_H
