/* Copyright 2022 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef PRUTO_PARSER_H
#define PRUTO_PARSER_H

#include <stddef.h>

namespace pruto {

    static constexpr int ExpressionNestingLimit = 10; // pretty arbitrary choice - sane limit for readable expressions

    enum class BlockNesting {
        Global,
        Proc,
        If,
        Switch,
        SwitchCase,
        Else,
        Loop
    };

    enum class InsideLoop {
        No, Yes
    };

    enum class ExpressionError {
        UnexpectedCharacter, // includes newline and eos
        UnexpectedKeyword,
        UnexpectedNonEnumColon, UnexpectedAssignSymbol,
        MissingOperand, MissingOperatorBetweenOperands,
        NestingLimitExceeded
    };

    enum class StatementError {
        UnexpectedCharacter,
        UnexpectedKeyword,
        KeywordOutsideProc, CallOutsideProc, ReassignOutsideProc,
        NestedProcDefinition,
        ElseOutsideConditional, UnexpectedWordAfterElse,
        BreakOutsideLoop, ContinueOutsideLoop,
        CaseOutsideSwitch, NeedCaseAfterSwitch,
        NeedInAfterForIdent,
        MissingSpaceAfterIf, MissingSpaceAfterSwitch, MissingSpaceAfterCase,
        MissingSpaceAfterWhile, MissingSpaceAfterForIn, MissingSpaceAfterReturn,
        SectionInsideProc, WhitespaceBeforeSection
    };

    class expression_callbacks {
    public:
        virtual bool ref(const char* line, size_t start, size_t ident_size, size_t ref_size);
        virtual bool enum_ref(const char* line, size_t ref_start, size_t ident_size, size_t ref_size, size_t mem_size);

        virtual bool symop(const char* line, size_t pos, size_t size);

        virtual bool true_lit(const char* line, size_t pos);
        virtual bool false_lit(const char* line, size_t pos);
        virtual bool num_lit(const char* line, size_t pos, size_t size);
        virtual bool str_lit_start(const char* line, size_t pos);
        virtual bool str_lit_end(const char* line, size_t pos);

        virtual bool group_start(const char* line, size_t pos);
        virtual bool group_end(const char* line, size_t pos);
        virtual bool call_start(const char* line, size_t ident_start, size_t ident_size, size_t pos);
        virtual bool call_end(const char* line, size_t pos);
        virtual bool struct_start(const char* line, size_t pos);
        virtual bool struct_mem(const char* line, size_t start, size_t ident_size, size_t ref_size);
        virtual bool struct_mem_value(const char* line, size_t start, size_t ident_size, size_t ref_size);
        virtual bool struct_end(const char* line, size_t pos);
        virtual bool list_start(const char* line, size_t pos);
        virtual bool list_end(const char* line, size_t pos);
        virtual bool exp_mem_separator(const char* line, size_t pos);

        virtual void comment_start(const char* line, size_t pos);
        virtual const char* inner_newline(const char* line, size_t pos) = 0;

        virtual void error(const char* line, ExpressionError e, size_t pos, size_t size = 1);
    };

    class statement_callbacks : public expression_callbacks {
    public:
        virtual bool proc_declaration(const char* line, size_t proc_pos, size_t ident_start, size_t ident_size, size_t params_start);
        virtual bool proc_declaration_param(const char* line, size_t start, size_t size);
        virtual bool proc_declaration_end(const char* line, size_t pos);

        virtual bool proc_alias(const char* line, size_t proc_pos, size_t ident_start, size_t ident_size,
                                size_t sym_pos, size_t alias_start, size_t alias_size);

        virtual bool const_def(const char* line, size_t ident_start, size_t ident_size, size_t sym_pos);
        virtual bool var_def(const char* line, size_t var_pos, size_t ident_start, size_t ident_size, size_t sym_pos);

        virtual bool reassign(const char* line, size_t ref_start, size_t ident_size, size_t ref_size, size_t sym_pos);

        virtual bool call_stm_start(const char* line, size_t ident_start, size_t ident_size, size_t pos);
        virtual bool call_stm_end(const char* line, size_t pos);

        virtual bool return_stm(const char* line, size_t pos);
        virtual bool return_value_stm(const char* line, size_t pos);

        virtual bool if_stm(const char* line, size_t pos);
        virtual bool else_if_stm(const char* line, size_t else_pos, size_t if_pos);
        virtual bool else_stm(const char* line, size_t pos);
        virtual bool switch_stm(const char* line, size_t pos);
        virtual bool case_stm(const char* line, size_t pos);
        virtual bool case_stm_label(const char* line, size_t label_start, size_t label_size);
        virtual bool while_stm(const char* line, size_t pos);
        virtual bool for_stm(const char* line, size_t pos, size_t ident_start, size_t ident_size, size_t in_pos);

        virtual bool break_stm(const char* line, size_t pos);
        virtual bool continue_stm(const char* line, size_t pos);

        virtual bool end_stm(const char* line, size_t pos);

        virtual bool section(const char* line, size_t ident_start, size_t ident_size);

        virtual bool param_label_separator(const char* line, size_t pos);

        virtual void newline(const char* line, size_t pos);
        virtual void end_of_script(const char* line, size_t pos);

        virtual void error(const char* line, StatementError e, size_t pos, size_t size = 1);
    };


    // The main API of the parser are just these two functions:

    struct expression_parse_result {
        const char* end_line = nullptr;
        size_t end_pos = 0;
    };
    expression_parse_result parse_expression(const char* line, size_t start, expression_callbacks* cb);

    void parse_statement(const char* line, BlockNesting current_nesting, InsideLoop loop, statement_callbacks* cb);


    // Additional parsing utilities:

    enum class SymOp {
        And,
        Bar,
        Plus,
        Minus,
        Asterisk,
        Slash,
        Backslash,
        Tilde,
        Percent,
        Hash,
        Caret,
        Apostrophe,
        Less,
        Greater,
        Exclamation,
        AndAnd,
        AndBar,
        AndPlus,
        AndMinus,
        AndAsterisk,
        AndSlash,
        AndBackslash,
        AndTilde,
        AndPercent,
        AndHash,
        AndCaret,
        AndApostrophe,
        AndLess,
        AndGreater,
        AndExclamation,
        BarAnd,
        BarBar,
        BarPlus,
        BarMinus,
        BarAsterisk,
        BarSlash,
        BarBackslash,
        BarTilde,
        BarPercent,
        BarHash,
        BarCaret,
        BarApostrophe,
        BarLess,
        BarGreater,
        BarExclamation,
        LessEqual,
        GreaterEqual,
        NotEqual,
        Equal
    };
    constexpr unsigned SymOpCount = static_cast<unsigned>(SymOp::Equal) + 1;

    SymOp sym_op(const char* start, size_t size);
    size_t sym_op_size(const char* start);

    constexpr bool is_alpha(char c) { return (c >= 0x41 && c <= 0x5A) || (c >= 0x61 && c <= 0x7A); }
    constexpr bool is_digit(char c) { return c >= 0x30 && c <= 0x39; }
    constexpr bool is_numlit_continuation(char c) { return is_digit(c) || c == '_' || c == '.'; }
    constexpr bool is_alnum(char c) { return is_alpha(c) || is_digit(c); }
    constexpr bool is_word_start(char c) { return is_alpha(c) || c == '_'; }
    constexpr bool is_word_continuation(char c) { return is_alnum(c) || c == '_'; }
    constexpr bool is_ws(char c) { return c == ' ' || c == '\t'; }

    bool is_identifier(const char* start);

} // namespace

#endif // PRUTO_PARSER_H
